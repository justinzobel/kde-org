-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

KDE Security Advisory: Konqueror International Domain Name Spoofing
Original Release Date: 20050316
URL: http://www.kde.org/info/security/advisory-20050316-2.txt

0. References
        http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2005-0237
        http://bugs.kde.org/show_bug.cgi?id=98788
        http://lists.netsys.com/pipermail/full-disclosure/2005-February/031459.html 
        http://lists.netsys.com/pipermail/full-disclosure/2005-February/031460.html 
        http://www.shmoo.com/idn 
        http://www.shmoo.com/idn/homograph.txt 
        http://xforce.iss.net/xforce/xfdb/19236
        http://secunia.com/advisories/14162/

1. Systems affected:

        All KDE versions in the KDE 3.2.x and KDE 3.3.x series.


2. Overview:

        Since version 3.2 KDE and it's webbrowser Konqueror have support
        for International Domain Names (IDN). Unfortunately this has
        made KDE vulnerable to a phishing technique known as a 
        Homograph attack.

        IDN allows a website to use a wide range of international characters
        in its domain name. Unfortunately some of these characters have a
        strong resemblance to other characters, so called homographs. This
        makes it possible for a website to use a domain name that is
        technically different from another well known domain name, but has
        no or very little visual differences.

        This lack of visual difference can be abused by attackers to
        trick users into visiting malicious websites that resemble
        a well known and trusted website in order to obtain personal
        information such as credit card details.

        The Common Vulnerabilities and Exposures project (cve.mitre.org)
        has assigned the name CAN-2005-0237 to this issue.

        For KDE 3.4 KDE and the Konqueror webbrowser have adopted a
        whitelist of domains for which IDN is safe to use because the
        registrar for these domains has implemented anti-homographic
        character policies or otherwise limited the available set of
        characters to prevent spoofing.

      
3. Impact:

        Users can be tricked into visiting a malicious website that
        resembles a well known and trusted website without getting any
        visual indication that this website differs from the one the
        user was expecting to visit.


4. Solution:

        Upgrade to KDE 3.4.

        For older versions of KDE Source code patches have been made
        available which fix these vulnerabilities. Contact your OS vendor /
        binary package provider for information about how to obtain updated
        binary packages.


5. Patch:

        A patch for KDE 3.2.x is available from
        ftp://ftp.kde.org/pub/kde/security_patches

        611bad3cb9ae46ac35b907c7321da7aa  post-3.2.3-kdelibs-idn.patch

        A patch for KDE 3.3.x is available from
        ftp://ftp.kde.org/pub/kde/security_patches

        b92182b7734e4ff145a08d9755448ec7  post-3.3.2-kdelibs-idn-2.patch

6. Time line and credits:

        07/02/2005 Issue raised by Eric Johanson on full-disclosure
        03/03/2005 Patches applied to KDE CVS.
        04/03/2005 Vendors notified
        16/03/2005 KDE Security Advisory released.
        16/03/2005 KDE Security Advisory patch updated.
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.0 (GNU/Linux)

iD8DBQFCOEvFN4pvrENfboIRAoCgAKCNO+hEBqYGBBF0naUNrVQ0RritKQCfUyLp
TVxqiY9Nb933I3jIP4UdLTk=
=PFy8
-----END PGP SIGNATURE-----
