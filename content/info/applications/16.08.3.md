---
title: "KDE Applications 16.08.3 Info Page"
announcement: /announcements/announce-applications-16.08.3
layout: applications
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
