---
aliases:
- ../plasma2tp
custom_about: true
custom_contact: true
title: Plasma 2 Technology Preview and Plasma Media Center 1.2.0
date: "2013-12-20"
description: Plasma 2 Technology Preview and Plasma Media Center 1.2.0
---

December 20, 2013. The KDE Community is proud to announce two milestones of the Plasma Workspaces.

- <strong><a href="http://dot.kde.org/2013/12/20/plasma-2-technology-preview">Plasma 2 Technology Preview</a></strong> -- A first glance at the evolution of the Plasma Workspaces
- <strong><a href="http://dot.kde.org/2013/12/20/plasma-media-center-12-released-time-christmas">Plasma Media Center 1.2.0</a></strong> -- Many new features and improvements in Plasma's Media Center user experience.

## <a href="http://dot.kde.org/2013/12/20/plasma-2-technology-preview"><img src="/announcements/plasma/2-tp/plasma2tp-desktop-with-stuff.png" class="app-icon" alt="Plasma 2 Technology Preview"/> Plasma 2 Technology Preview</a>

KDE's Plasma Team presents a first glance at the evolution of the Plasma Workspaces. <strong>Plasma 2 Technology Preview</strong> demonstrates the current development status. The Plasma 2 user interfaces are built using QML and run on top of a fully hardware accelerated graphics stack using Qt5, QtQuick 2 and an OpenGL(-ES) scenegraph. Plasma 2's converged workspace shell allows you to run and switch between user interfaces for different form factors, and makes the workspace adaptable to the given target device. The workspace demonstrated in this technology preview is Plasma Desktop, an incremental evolution to known desktop and laptop paradigms. The user experience aims at keeping existing workflows intact, while providing incremental visual and interactive improvements. <a href="http://dot.kde.org/2013/12/20/plasma-2-technology-preview">More info...</a>

## <a href="http://dot.kde.org/2013/12/20/plasma-media-center-12-released-time-christmas"><img src="/announcements/plasma/2-tp/plasma-mediacenter.png" class="app-icon" alt="Plasma Media Center 1.2.0"/> Plasma Media Center 1.2.0</a>

The KDE community has a Christmas gift for you! We are happy to announce the release of KDE's <strong>Plasma Media Center 1.2.0</strong> - your first stop for media and entertainment created by the Elves at KDE. We have designed it to provide an easy and comfortable way to watch your videos, browse your photo collection and listen to your music, all in one place. New in Plasma Media Center 1.2.0 is improved navigation in music mode, fetching of album covers, picture previews while browsing folders, support for multiple playlist, improved key bindings and new artwork. <a href="http://dot.kde.org/2013/12/20/plasma-media-center-12-released-time-christmas">More info...</a>

## Spread the Word and See What's Happening: Tag as &quot;KDE&quot;

KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for these releases of KDE software.
