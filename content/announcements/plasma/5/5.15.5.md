---
aliases:
- ../../plasma-5.15.5
changelog: 5.15.4-5.15.5
date: 2019-05-07
layout: plasma
figure:
  src: /announcements/plasma/5/5.15.0/plasma-5.15-apps.png
asBugfix: true
---

- KWin Emoji Support: Fix captions with non-BMP characters. <a href="https://commits.kde.org/kwin/57440d1d6b490cdad51266977d0269a08918b82f">Commit.</a> Fixes bug <a href="https://bugs.kde.org/376813">#376813</a>. Phabricator Code review <a href="https://phabricator.kde.org/D19052">D19052</a>
- Breeze theme: Fix build with Qt 4. <a href="https://commits.kde.org/breeze/386d3b8ed1e0595c9fc6e21643ff748402171429">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D20201">D20201</a>
- [weather] Fix default visibility unit for non-metric locales. <a href="https://commits.kde.org/kdeplasma-addons/c06e11928b4c70fe3d124ec247ef8b08bd441a86">Commit.</a>
- System Settings: Fix invisible monochrome icons in Icon View tooltips too. <a href="https://commits.kde.org/systemsettings/8b2e8d7eb61a82b61df40489eb09ce297a21c3eb">Commit.</a> Fixes bug <a href="https://bugs.kde.org/386748">#386748</a>
