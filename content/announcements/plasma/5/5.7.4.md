---
aliases:
- ../../plasma-5.7.4
changelog: 5.7.3-5.7.4
date: 2016-08-23
layout: plasma
youtube: A9MtFqkRFwQ
figure:
  src: /announcements/plasma/5/5.7.0/plasma-5.7.png
  class: text-center mt-4
asBugfix: true
---

- Fixed dragging items bug in Kickoff
- Mouse settings being applied in kdelibs4 applications
- Improved handling of screen CRTC information
