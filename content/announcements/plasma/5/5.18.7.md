---
aliases:
- ../../plasma-5.18.7
changelog: 5.18.6-5.18.7
date: 2021-03-30
layout: plasma
youtube: npJEpfXGGfI
figure:
  src: /announcements/plasma/5/5.18.0/plasma-5.18.png
---

{{< i18n_date >}}

{{< i18n "annc-plasma-bugfix-intro" "5" "5.18.7" >}}

{{% i18n "annc-plasma-bugfix-minor-release-2" "5.18" "/announcements/plasma/5/5.18.0" "2020" %}}

{{< i18n "annc-plasma-bugfix-worth-9" >}}

{{< i18n "annc-plasma-bugfix-last" >}}

+ Plasma Addons: Fix outdated API key for flickr provider. [Commit.](http://commits.kde.org/kdeplasma-addons/90a993061f0987bb46d42f2c5e9bf9c0d51291b6) See bug [#427566](https://bugs.kde.org/427566)
+ [folder view] Fix display on not-skinny vertical panels. [Commit.](http://commits.kde.org/plasma-desktop/dec75f0da52562b719b4d259b9061a3665b6c16b) Fixes bug [#406806](https://bugs.kde.org/406806)
+ Plasma Widgets: Fix Environment Canada weather domain. [Commit.](http://commits.kde.org/plasma-workspace/e27ba2d13c4d1f50eb3d83271a1654401d9e0035)
