---
aliases:
- ../../fulllog_releases-23.04.3
title: KDE Gear 23.04.3 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Invalidate model to apply sortCompletedTodosSeparately pref. [Commit.](http://commits.kde.org/eventviews/83076ae14a07ffa3f689e9e603a9d078ffc41598) Fixes bug [#458254](https://bugs.kde.org/458254)
{{< /details >}}
{{< details id="falkon" title="falkon" link="https://commits.kde.org/falkon" >}}
+ Preferences: Use default search engine by default. [Commit.](http://commits.kde.org/falkon/4d0f0040beb2344d4e28e7091b9f22b9d4665e85) 
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Pass exivData as argument to avoid crash in exiv2 library. [Commit.](http://commits.kde.org/gwenview/c15f7b79d68a583359ae597ca5cb405719501c0e) Fixes bug [#470880](https://bugs.kde.org/470880)
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Add 23.04.3 release notes. [Commit.](http://commits.kde.org/itinerary/7d1d96c9554203e816d0ec8f77a08a8d74832c65) 
+ Ensure we pass content: URLs fully encoded to Android API and QFile. [Commit.](http://commits.kde.org/itinerary/0ef3aa53895980a82d79940aa7494f1d700cea84) Fixes bug [#470643](https://bugs.kde.org/470643)
+ Add icon for boat/ferry reservations in the calendar. [Commit.](http://commits.kde.org/itinerary/b213215011f5bbb7ee525fcf56e9a14affe23746) 
+ Explicitly specify supported types when importing from a calendar. [Commit.](http://commits.kde.org/itinerary/40fa8a147de2da4b6d2c51ad47b254f3650ca3c7) 
+ Add map argument unit test for static elements at the begin/end. [Commit.](http://commits.kde.org/itinerary/4fd8a84d6d9257f61049cd26e578c6933fdfc498) 
+ Fix infinite loop in computing static location map arguments. [Commit.](http://commits.kde.org/itinerary/44ed4fbe96d45a60b33e2bf784196489197d7dcc) 
+ Fail less dramatically when ending up with an unknown pass. [Commit.](http://commits.kde.org/itinerary/4cc7472728a6dc0403d4c8d65db3d04ddabe43f4) 
+ Work around the Android back key closing the app on secondary layers. [Commit.](http://commits.kde.org/itinerary/ab598bc51053385c8bdd80d1eb8b54210398ec58) 
{{< /details >}}
{{< details id="kajongg" title="kajongg" link="https://commits.kde.org/kajongg" >}}
+ Fix running on Python 3.11. [Commit.](http://commits.kde.org/kajongg/b94b8fac2b701d400ac4ba4ae233b68a07d2bc62) 
+ Fix running with zope-interface 6. [Commit.](http://commits.kde.org/kajongg/beb9dfdc46cb453e174b69020250fe4418608e40) 
{{< /details >}}
{{< details id="kalendar" title="kalendar" link="https://commits.kde.org/kalendar" >}}
+ Store reminder times to the correct variable. [Commit.](http://commits.kde.org/kalendar/e3718ccda895dbea01232c1e8eefd8744477040f) Fixes bug [#470288](https://bugs.kde.org/470288). Fixes bug [#470525](https://bugs.kde.org/470525)
{{< /details >}}
{{< details id="kanagram" title="kanagram" link="https://commits.kde.org/kanagram" >}}
+ Fix a minor bug. [Commit.](http://commits.kde.org/kanagram/65dec5186c3124a17aa04a1e6d20a5e61ac68cd4) 
{{< /details >}}
{{< details id="kasts" title="kasts" link="https://commits.kde.org/kasts" >}}
+ Fix mMedia nullptr dereference in VLC backend metadata call. [Commit.](http://commits.kde.org/kasts/1781230ad8ba7b271487380a931678ee3a1925fa) See bug [#470263](https://bugs.kde.org/470263)
+ More safeguards for getFeed and getEntry calls. [Commit.](http://commits.kde.org/kasts/3eb66a510ff22948ad8729e9624b4f9aea2e7bd3) 
+ Fix segfault when trying to remove a non-subscribed podcast. [Commit.](http://commits.kde.org/kasts/67e3f5e990fb5b69e77bbe72143a34b7e47a2ed6) Fixes bug [#471107](https://bugs.kde.org/471107)
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Improve code action invocation with no selection. [Commit.](http://commits.kde.org/kate/aaee93e823e815637717a3ff7eedfbed47e7048f) 
+ Fix LSP inlay hint deserialization. [Commit.](http://commits.kde.org/kate/8769d8e40a0d58ab85edd6ae18218c3bc7b131a5) 
+ Lsp: Use label as insertText as a last resort. [Commit.](http://commits.kde.org/kate/204b214c35fd7c554c79cb33db341c9065710c01) 
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Fix kdeconnect_open desktop file type. [Commit.](http://commits.kde.org/kdeconnect-kde/276c2f1334345071eaeef9aea87c96cdd199f8f0) Fixes bug [#424782](https://bugs.kde.org/424782)
+ Use explicit constructor for QSslCertificate with value initialized argument. [Commit.](http://commits.kde.org/kdeconnect-kde/8687f8cfdaf689aea5fb2cdb8043c5c4d5a93658) Fixes bug [#469428](https://bugs.kde.org/469428)
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix tests. [Commit.](http://commits.kde.org/kdenlive/a2aff18bea15b82f7b3c02e3defecb12b6d2ffa4) 
+ Fix effects disappearing from timeline sequence and other sync issues. [Commit.](http://commits.kde.org/kdenlive/8bb0fb6e2878294029ce659ac5fa8f8f95c8d415) 
+ Fix crash loading project with an unknown transition. [Commit.](http://commits.kde.org/kdenlive/9bd025d93d7c1c76d0d8e62bcd3b75a7b689b343) 
+ Fix crash trying to open backup for moved project file. [Commit.](http://commits.kde.org/kdenlive/933bd53941460c2b1312e1b0cce876b204083e41) 
+ Switch vglobal_quality to vqp for nvenc (same as Shotcut). [Commit.](http://commits.kde.org/kdenlive/030df2cc62244b0be5e9b4baf5d86e172ab0fa67) 
+ Backport ASAN fix from master. [Commit.](http://commits.kde.org/kdenlive/d9c0ce8a28cdb7479a1e7a96ffffc038cbc62c08) 
+ Fix ASAN use after freed. [Commit.](http://commits.kde.org/kdenlive/31039db057fe4e146b0d4f4a155402ac1dd82043) 
+ Mixer: polish audio levels display, add scale for gain slider. [Commit.](http://commits.kde.org/kdenlive/dc1d3c94d58c5330c4dcbe26af130d21e19a6f31) 
+ Fix audio levels gradient colors. [Commit.](http://commits.kde.org/kdenlive/f6980031a3708b589e5b685c447ad9b8e2b8f8c2) 
+ Mixes: ensure asset panel cleared on undo insert, fix mix inserted at wrong clip end on drop. [Commit.](http://commits.kde.org/kdenlive/76b6e7dda88d0924ab758e43dbe797807a789740) 
+ Fix mixes incorrectly saved as luma. [Commit.](http://commits.kde.org/kdenlive/6456db0382b83a4ba7c2c101d9a7ffbf872cd996) 
+ Ensure Subtitle widget can fit on smaller screens. [Commit.](http://commits.kde.org/kdenlive/49a79c75d42126b830ffd94cdf0bd844bdf0aeec) See bug [#470498](https://bugs.kde.org/470498)
+ Fix crash pressing Home on subtitle track. [Commit.](http://commits.kde.org/kdenlive/965e73c64b722037f7aa3202cb07ad1b1686b7a8) 
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Use a faster regex for matching Grimaldi Line passenger data. [Commit.](http://commits.kde.org/kitinerary/d6462caedc282f06b878c032b25e43a36d5adec4) 
+ Add Color Line extractor script. [Commit.](http://commits.kde.org/kitinerary/f6059e1f277bb84fb394b83f75728f44b4f9fc64) 
+ Support onepagebooking PDFs as well. [Commit.](http://commits.kde.org/kitinerary/fa548efb07c48ccf05c0769e91b93684c5f7f0ef) 
+ Expand the Fjord Line trigger expression to cover newer tickets. [Commit.](http://commits.kde.org/kitinerary/910acb84c7edc7019d5210b40db41def41a75636) 
+ Treat any sequence of * characters as RCT2 empty marker. [Commit.](http://commits.kde.org/kitinerary/08eb1ea5c421dedbd8745b8a9251a0a9a429e94d) 
+ Accept also slightly larger ERA SSBv3 codes. [Commit.](http://commits.kde.org/kitinerary/5670ae06e010c84a7e65813d773fedfab515f8ac) 
+ Simplify Trenitalia extractor. [Commit.](http://commits.kde.org/kitinerary/68a376c01731e8083d31f5675e19deee6919bc04) 
+ Extract DJH membership card barcodes. [Commit.](http://commits.kde.org/kitinerary/6bcebac396a41c85754be4e2e01979333fafadb2) 
+ Fix extraction of multi-page Trenitalia tickets. [Commit.](http://commits.kde.org/kitinerary/0901abfb9cc840181b107b1a8ad1ab1151f3f04e) 
+ Improve handling of Flixbus train tickets. [Commit.](http://commits.kde.org/kitinerary/dc0584b9b8157b43d1198a6c109fecff887c484f) 
+ Add additional sanity checks for ERA SSBv2 tickets. [Commit.](http://commits.kde.org/kitinerary/ab7acffd861c0184abd458fe8f1e6aba6899386a) 
+ Relax the SNCB barcode trigger pattern a bit. [Commit.](http://commits.kde.org/kitinerary/42d8c2fdcfb82066a98cad232b20e45bed6fab05) 
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Don't remove margins. [Commit.](http://commits.kde.org/kmail/22a364d4ae32a9baa90d497bbcfd5e34732bd07c) 
+ Quote html chars in subject too. [Commit.](http://commits.kde.org/kmail/d18d013d816ead31bdbc8472f67daa1f0fe34dff) 
+ Remove unused QtDBus. [Commit.](http://commits.kde.org/kmail/b9c6fa7c56b1a80f58f34053943f378cce3dcc86) 
+ Kmsearchmessagemodel.cpp - Avoid crash for invalid Akonadi::Item. [Commit.](http://commits.kde.org/kmail/cc39fd0a89413345a0f87c68d5fc586e66c195c3) 
+ Allow to restore recent dir. [Commit.](http://commits.kde.org/kmail/5c89e3f10b7be12a2a4f56d78425591069a9faaf) 
+ Fix display email when we have < ... >. [Commit.](http://commits.kde.org/kmail/771ef76a785d41591e2ed29b6625f40eaa3d6e33) 
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Tab bar settings: fix MIME type filtering of custom CSS file picker. [Commit.](http://commits.kde.org/konsole/b9e6d1afe0896ea32cffabbcb714141d82b3d237) 
+ Hides redundant widgets on dialog for emoji font selection. [Commit.](http://commits.kde.org/konsole/f380f10d69cffe1547c4a6b40cc1a36e4e019bcd) Fixes bug [#469563](https://bugs.kde.org/469563)
{{< /details >}}
{{< details id="konversation" title="konversation" link="https://commits.kde.org/konversation" >}}
+ Highlights sounds file picker: fix setting file filter by MIME types. [Commit.](http://commits.kde.org/konversation/fd8907a4b3d2c472b48e262896b318e387482322) Fixes bug [#433667](https://bugs.kde.org/433667)
{{< /details >}}
{{< details id="kpat" title="kpat" link="https://commits.kde.org/kpat" >}}
+ Solve the simple simon autodrop problem less aggressively. [Commit.](http://commits.kde.org/kpat/31aa7a28cb63c9d7be35371a63ec2f481bec6efe) Fixes bug [#469991](https://bugs.kde.org/469991)
+ Reinit Golf solver correctly after switching games. [Commit.](http://commits.kde.org/kpat/65ed6d5270b6d77ac4f57218f4a5db24b621135a) Fixes bug [#470166](https://bugs.kde.org/470166)
{{< /details >}}
{{< details id="kpimtextedit" title="kpimtextedit" link="https://commits.kde.org/kpimtextedit" >}}
+ Remove prefix. [Commit.](http://commits.kde.org/kpimtextedit/8586877b0b66576bc96d9dea125174ac928bc176) 
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Drop implausible section paths. [Commit.](http://commits.kde.org/kpublictransport/fe15c96e4e2a336bd771aa1bf7d1aeee96c89226) 
+ Apply check for physical limits also to walking sections. [Commit.](http://commits.kde.org/kpublictransport/ecdce1ca81bc6d7650e8d35df9e63c70840e83c3) 
+ Fix long distance bus line mode for VOR. [Commit.](http://commits.kde.org/kpublictransport/6783d0b9d25477bbd07e5cdd372d2089fb1bb982) 
+ Remove dead OTP endpoints in Münster and Ulm. [Commit.](http://commits.kde.org/kpublictransport/2e1ed9acfb762a94fe6831d74ade8441ac452a7f) 
+ Also request the top-level transport mode for Entur stopover queries. [Commit.](http://commits.kde.org/kpublictransport/95c5f3ae44d2c6c9e88f870fab3cb85a659be60d) 
+ Parse OTP transit legs without a transport mode subtype correctly. [Commit.](http://commits.kde.org/kpublictransport/283711b931051dc67f1f77a8dad1ba0f7a536497) 
+ Make VRS work again. [Commit.](http://commits.kde.org/kpublictransport/cc54c276a660a39387729d98b14f8563dc84e927) 
+ Add support for PKCS#12 client certificate bundles. [Commit.](http://commits.kde.org/kpublictransport/6f52d9cecff4631035fe348bf4bfe294d35c2c3e) 
+ Apply SSL configuration to IVV ASS network requests as well. [Commit.](http://commits.kde.org/kpublictransport/0eb88221343b8a7dbf64c45c32f61552e53cd8cb) 
+ Don't crash when setting custom QNAM when there isn't a default one yet. [Commit.](http://commits.kde.org/kpublictransport/e332c7d6c23cbb820e5e690a226464d874510199) 
{{< /details >}}
{{< details id="kreversi" title="kreversi" link="https://commits.kde.org/kreversi" >}}
+ Fix board position in portrait mode. [Commit.](http://commits.kde.org/kreversi/ff30be0a59296491e666a2c96ad8f681ba563838) Fixes bug [#445131](https://bugs.kde.org/445131)
{{< /details >}}
{{< details id="ktorrent" title="ktorrent" link="https://commits.kde.org/ktorrent" >}}
+ Import Torrent dialog: fix file filter. [Commit.](http://commits.kde.org/ktorrent/c7caa4735d4f8d024029e18ebdc5cdb3cbd87348) Fixes bug [#464350](https://bugs.kde.org/464350)
{{< /details >}}
{{< details id="libkomparediff2" title="libkomparediff2" link="https://commits.kde.org/libkomparediff2" >}}
+ Add patch-level to version, also unbreaks KOMPAREDIFF2_VERSION C++ macro. [Commit.](http://commits.kde.org/libkomparediff2/15036dcd8c5bd22f2b600cc88440a152a63d2eb4) 
{{< /details >}}
{{< details id="lokalize" title="lokalize" link="https://commits.kde.org/lokalize" >}}
+ Ignore word wrapping newlines when creating message diffs. [Commit.](http://commits.kde.org/lokalize/ba5afb1274193507fc6bba24ae1363671424a8cc) 
{{< /details >}}
