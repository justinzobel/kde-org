------------------------------------------------------------------------
r1222925 | scripty | 2011-02-28 02:17:21 +1300 (Mon, 28 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1223107 | lvsouza | 2011-02-28 21:59:45 +1300 (Mon, 28 Feb 2011) | 7 lines

Backport r1223106 by lvsouza from trunk to the 4.6 branch:

Do not call setWrong if the password prompt has been cancelled by the user.
Thanks Christopher January for the patch.

CCBUG: 230181

------------------------------------------------------------------------
r1223249 | lvsouza | 2011-03-02 07:02:17 +1300 (Wed, 02 Mar 2011) | 4 lines

Backport r1223248 by lvsouza from trunk to the 4.6 branch:

Restore binary compatibility as suggested by Jonathan Riddell.

------------------------------------------------------------------------
r1223562 | mfuchs | 2011-03-04 04:31:49 +1300 (Fri, 04 Mar 2011) | 2 lines

Backport r1223556
Verifier returns correct availablePartialChecksums now.
------------------------------------------------------------------------
r1223563 | mfuchs | 2011-03-04 04:31:51 +1300 (Fri, 04 Mar 2011) | 3 lines

Backport r1223557
Verifier works correctly now if QCA is available yet does not provide MD5.
Also uses QStringList instead of QList<QString>.
------------------------------------------------------------------------
r1223774 | lvsouza | 2011-03-05 16:11:55 +1300 (Sat, 05 Mar 2011) | 2 lines

Backporting r1223419: Fix user_is_typing_message notifications

------------------------------------------------------------------------
r1223775 | lvsouza | 2011-03-05 16:14:05 +1300 (Sat, 05 Mar 2011) | 2 lines

Backportring r1223423: Fix for contacts being deleted from contactlist after opening jabber conference

------------------------------------------------------------------------
r1223776 | lvsouza | 2011-03-05 16:16:10 +1300 (Sat, 05 Mar 2011) | 2 lines

Backporting r1223427, statistics plugin: batch status updated on exit into a single transaction.

------------------------------------------------------------------------
r1223777 | lvsouza | 2011-03-05 16:18:18 +1300 (Sat, 05 Mar 2011) | 2 lines

Backporting r1223433: improve contact search system.

------------------------------------------------------------------------
r1224119 | lvsouza | 2011-03-08 16:24:25 +1300 (Tue, 08 Mar 2011) | 2 lines

Reverting some changes that should not have been commited.

------------------------------------------------------------------------
r1224164 | scripty | 2011-03-09 03:46:12 +1300 (Wed, 09 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1224230 | lvsouza | 2011-03-09 21:30:48 +1300 (Wed, 09 Mar 2011) | 2 lines

Backporting r1224229: fixing trivial bug.

------------------------------------------------------------------------
r1224588 | scripty | 2011-03-13 02:37:54 +1300 (Sun, 13 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1225172 | lvsouza | 2011-03-18 08:42:29 +1300 (Fri, 18 Mar 2011) | 6 lines

Backporting r1225171: Apply fix for lack of unicode translation in Gadu plugin.
Thanks Maciej `Matrach` Matraszek for the patch.

CCBUG: 147434
FIXED-IN: 4.6.2

------------------------------------------------------------------------
r1225246 | lvsouza | 2011-03-19 07:47:01 +1300 (Sat, 19 Mar 2011) | 5 lines

Backport r1225245: Fix for Kopete disconnects after several xmpp ping messages from jabber server

BUG: 198789
FIXED-IN: 4.6.2

------------------------------------------------------------------------
r1225296 | mfuchs | 2011-03-20 01:47:54 +1300 (Sun, 20 Mar 2011) | 2 lines

Backport r1225295
Fixes updating metalinks.
------------------------------------------------------------------------
r1225300 | scripty | 2011-03-20 02:43:12 +1300 (Sun, 20 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1225336 | lvsouza | 2011-03-20 14:28:50 +1300 (Sun, 20 Mar 2011) | 7 lines

Backporting r1225335 to 4.6.2: Use kapp->processEvents() to minimize statistics plugin blockage.
This does not solve the problem, Kopete feels sluggish when loading
statistics plugin even after this change (CPU still goes to 100% usage),
but it is better than blocking Kopete altogether.

CCBUG: 117989

------------------------------------------------------------------------
r1225403 | lvsouza | 2011-03-21 07:26:59 +1300 (Mon, 21 Mar 2011) | 6 lines

Backporting r1225401: Properly use KNotification::event in Jabber accounts.
Thanks Alexander Potashev for the patch: http://svn.reviewboard.kde.org/r/6620

REVIEW: 6620
FIXED-IN: 4.6.2

------------------------------------------------------------------------
r1225430 | lvsouza | 2011-03-21 10:59:59 +1300 (Mon, 21 Mar 2011) | 6 lines

Backporting 1225429: Fix kppp fails to build on Solaris 10. sethostname exists in libc, but is not
defined in /usr/include/unistd.h
Thanks tropikhajma for the patch.

BUG: 177220

------------------------------------------------------------------------
r1225841 | mfuchs | 2011-03-24 09:59:22 +1300 (Thu, 24 Mar 2011) | 3 lines

Backport r1225838
Downloading the contents of a metalink file or a torrent file to the same directory the metalink is in works now.
CCBUG:268449
------------------------------------------------------------------------
r1226185 | scripty | 2011-03-28 03:09:36 +1300 (Mon, 28 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1226216 | lvsouza | 2011-03-28 08:31:46 +1300 (Mon, 28 Mar 2011) | 8 lines

Make V4L support optional as it has been removed from kernel 2.6.38
http://svn.reviewboard.kde.org/r/6643
Thanks Carlos Federico Aguirre for this patch.

BUG: 269296
REVIEW: 6643
FIXED-IN: 4.6.2

------------------------------------------------------------------------
r1226222 | lvsouza | 2011-03-28 08:46:14 +1300 (Mon, 28 Mar 2011) | 2 lines

Removing some trailing spaces. No real changes added.

------------------------------------------------------------------------
r1226416 | scripty | 2011-03-30 07:19:49 +1300 (Wed, 30 Mar 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
