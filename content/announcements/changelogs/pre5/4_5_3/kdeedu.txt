------------------------------------------------------------------------
r1181736 | bholst | 2010-10-02 11:43:35 +1300 (Sat, 02 Oct 2010) | 4 lines

Fixed a little bug which prevented Marble's zoom buttons from being
checked for activity when clicking on them.

Backport, original fix in r1181734
------------------------------------------------------------------------
r1182726 | pino | 2010-10-06 02:33:10 +1300 (Wed, 06 Oct 2010) | 5 lines

static i18n strings are generally a no-no

BUG: 253302
FIXED-IN: 4.5.3

------------------------------------------------------------------------
r1182811 | lueck | 2010-10-06 07:20:30 +1300 (Wed, 06 Oct 2010) | 1 line

documentation backport from trunk for 4.5.3
------------------------------------------------------------------------
r1183296 | lueck | 2010-10-07 10:39:27 +1300 (Thu, 07 Oct 2010) | 1 line

doc backport of edu man pages
------------------------------------------------------------------------
r1183600 | scripty | 2010-10-08 15:55:59 +1300 (Fri, 08 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1183833 | apol | 2010-10-09 02:35:31 +1300 (Sat, 09 Oct 2010) | 4 lines

Add missing i18n() call.

CCMAIL: lueck@hube-lueck.de

------------------------------------------------------------------------
r1183851 | apol | 2010-10-09 03:05:08 +1300 (Sat, 09 Oct 2010) | 2 lines

Turn i18n() call into i18nc()

------------------------------------------------------------------------
r1183921 | yurchor | 2010-10-09 06:32:13 +1300 (Sat, 09 Oct 2010) | 1 line

backport fixes from trunk
------------------------------------------------------------------------
r1185602 | bholst | 2010-10-14 10:05:31 +1300 (Thu, 14 Oct 2010) | 7 lines

* Round, don't cut.
* Make sure the right zoom-level is saved.

BUG: 249628

backport r1185567
backport r1185591
------------------------------------------------------------------------
r1185626 | laidig | 2010-10-14 12:36:48 +1300 (Thu, 14 Oct 2010) | 4 lines

Use invalidateFilter rather than reset when filter string changed.

backport of r1179892 by fregl
BUG:252413
------------------------------------------------------------------------
r1185806 | mlaurent | 2010-10-14 22:27:26 +1300 (Thu, 14 Oct 2010) | 2 lines

Backport: fix mem leak + const'ify

------------------------------------------------------------------------
r1185808 | mlaurent | 2010-10-14 22:29:00 +1300 (Thu, 14 Oct 2010) | 2 lines

Backport fix mem leak

------------------------------------------------------------------------
r1185965 | bholst | 2010-10-15 09:23:58 +1300 (Fri, 15 Oct 2010) | 7 lines

Corrected Marble's atmosphere painting:
* We have to paint the atmosphere in any case (because we update the background, stars).
* We have to paint the atmosphere at the correct moment (between stars and ground).

BUG: 254154

backport r1185961
------------------------------------------------------------------------
r1187220 | rdieter | 2010-10-19 06:22:59 +1300 (Tue, 19 Oct 2010) | 6 lines

backport r1187219

drop unhandled (and invalid) mimetype.

BUG: 235563

------------------------------------------------------------------------
r1187670 | scripty | 2010-10-20 15:43:38 +1300 (Wed, 20 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1188037 | lueck | 2010-10-21 19:27:49 +1300 (Thu, 21 Oct 2010) | 2 lines

backport from trunk rev 1188036: load catalog libkdeedu with translations of element names, thanks to Alexander Potashev for spotting this
BUG:254820
------------------------------------------------------------------------
r1188209 | aacid | 2010-10-22 06:44:32 +1300 (Fri, 22 Oct 2010) | 2 lines

The place got renamed

------------------------------------------------------------------------
r1188340 | scripty | 2010-10-22 15:54:29 +1300 (Fri, 22 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1189277 | aspotashev | 2010-10-25 01:48:29 +1300 (Mon, 25 Oct 2010) | 4 lines

Backport fix for 255086 to 4.5

CCBUG:255086

------------------------------------------------------------------------
r1190458 | apol | 2010-10-28 07:21:45 +1300 (Thu, 28 Oct 2010) | 4 lines

Backport fix from trunk to 4.5.

CCBUG: 255430

------------------------------------------------------------------------
r1190722 | nienhueser | 2010-10-29 06:54:02 +1300 (Fri, 29 Oct 2010) | 2 lines

Increase version to 0.10.3

------------------------------------------------------------------------
