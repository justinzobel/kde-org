------------------------------------------------------------------------
r1098668 | scripty | 2010-03-04 15:17:11 +1300 (Thu, 04 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1099623 | lueck | 2010-03-06 09:52:05 +1300 (Sat, 06 Mar 2010) | 1 line

merge into one docbook for easier maintenance
------------------------------------------------------------------------
r1103402 | rkcosta | 2010-03-15 13:09:01 +1300 (Mon, 15 Mar 2010) | 9 lines

Backport r1103400.

Make do{Kill,Resume,Suspend} public as in their parent class.

Thanks to Michal Sciubidlo, who did that in a completely unrelated
patch :)

CCMAIL: michal.sciubidlo@gmail.com

------------------------------------------------------------------------
r1105567 | dakon | 2010-03-21 05:02:50 +1300 (Sun, 21 Mar 2010) | 4 lines

fix error message when importing keys from keyserver

BUG:231079

------------------------------------------------------------------------
r1105768 | dakon | 2010-03-22 00:22:36 +1300 (Mon, 22 Mar 2010) | 1 line

get rid of GnuPG greeting when expanding a key with photo id
------------------------------------------------------------------------
r1105827 | scripty | 2010-03-22 02:22:32 +1300 (Mon, 22 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1106908 | dakon | 2010-03-24 19:25:32 +1300 (Wed, 24 Mar 2010) | 6 lines

properly handle 8bit messages from "gpg --decrypt"

Backport of r1106906

CCBUG:231154

------------------------------------------------------------------------
r1107487 | rkcosta | 2010-03-26 08:53:39 +1300 (Fri, 26 Mar 2010) | 10 lines

Backport r1107485.

Revert r978708.

It is actually necessary to override the cursor, since the Part
will set the cursor to WaitCursor while some operation is being
performed.

CCBUG: 231974

------------------------------------------------------------------------
r1107498 | rkcosta | 2010-03-26 09:14:31 +1300 (Fri, 26 Mar 2010) | 6 lines

Backport r1107496.

Set the correct compression type for xz and lzma files.

CCBUG: 231100

------------------------------------------------------------------------
