------------------------------------------------------------------------
r1085162 | adamc | 2010-02-04 13:28:48 +0000 (Thu, 04 Feb 2010) | 3 lines

allow colouring of background in pidgin style (importand, if the  user sets a dark colour scheme in the desktop settings)


------------------------------------------------------------------------
r1086075 | mfuchs | 2010-02-06 12:57:42 +0000 (Sat, 06 Feb 2010) | 1 line

Correctly save the downloaded size and changes the segment size of Metalink to 512kb.
------------------------------------------------------------------------
r1086090 | mfuchs | 2010-02-06 13:22:58 +0000 (Sat, 06 Feb 2010) | 1 line

Do not modify downloadedSize when loading, as the correct size was saved.
------------------------------------------------------------------------
r1086380 | scripty | 2010-02-07 05:01:08 +0000 (Sun, 07 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1086498 | rjarosz | 2010-02-07 13:49:42 +0000 (Sun, 07 Feb 2010) | 4 lines

Backport commit 1086497.
Can't delete socket in slot connected to disconnected signal.
CCBUG: 224800

------------------------------------------------------------------------
r1086538 | rjarosz | 2010-02-07 15:48:50 +0000 (Sun, 07 Feb 2010) | 7 lines

Backport commit 1086537.
Don't crash if we get message for connection without contact.
Could happen if incoming port is blocked.
TODO: Create contact and don't discard the message.
CCBUG: 217423


------------------------------------------------------------------------
r1086541 | mfuchs | 2010-02-07 15:53:51 +0000 (Sun, 07 Feb 2010) | 2 lines

Backport r1086536
ChecksumSearch now does look which files are in the directory and caches that, then downloads are only created for existing files.
------------------------------------------------------------------------
r1087031 | rkcosta | 2010-02-08 13:02:25 +0000 (Mon, 08 Feb 2010) | 11 lines

Backport r1083071.

Clean up FindLibV4L2.cmake:

 * Explain what each defined variable means
 * Actually make use of FIND_PACKAGE_HANDLE_STANDARD_ARGUMENTS and
   consequently get rid of a lot of duplicate code.
 * Use MARK_AS_ADVANCED for the include and library variables.

CCMAIL: fschaefer.oss@googlemail.com

------------------------------------------------------------------------
r1087278 | mfuchs | 2010-02-08 18:50:20 +0000 (Mon, 08 Feb 2010) | 2 lines

Backport r1087276
Adds possibility to unregister searches.
------------------------------------------------------------------------
r1087369 | mfuchs | 2010-02-08 22:04:28 +0000 (Mon, 08 Feb 2010) | 1 line

Also recognises Job::FinishedKeepAlive as finished if the Transfers' status is checked.
------------------------------------------------------------------------
r1087781 | mfuchs | 2010-02-09 14:52:00 +0000 (Tue, 09 Feb 2010) | 3 lines

Backport and adapts r1087773
DataSourceFactory sets to Job::Aborted if the file can not be created.
Also saves wether the size was initially defined or not.
------------------------------------------------------------------------
r1087782 | mfuchs | 2010-02-09 14:52:03 +0000 (Tue, 09 Feb 2010) | 2 lines

When loading Transfers do not check the source and destination as that was checked already when the Transfer was initially added.
CCBUG: 190440
------------------------------------------------------------------------
r1087857 | lueck | 2010-02-09 17:23:26 +0000 (Tue, 09 Feb 2010) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r1088487 | rjarosz | 2010-02-10 23:21:01 +0000 (Wed, 10 Feb 2010) | 5 lines

Backport commit 1088486.
Always use jid.domain() as a key in transport map.

CCBUG: 224515

------------------------------------------------------------------------
r1088978 | scripty | 2010-02-12 04:16:45 +0000 (Fri, 12 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1089671 | uwolfer | 2010-02-13 17:28:30 +0000 (Sat, 13 Feb 2010) | 4 lines

Backport:
SVN commit 1089670 by uwolfer:

Do not check Google Talk libjingle by default. If you do, it fails with default settings for any other Jabber service than the Google one.
------------------------------------------------------------------------
r1090043 | segato | 2010-02-14 14:36:29 +0000 (Sun, 14 Feb 2010) | 4 lines

backport r1090040
ask the user for a new password if the one stored in kwallet is wrong
CCBUG: 226283

------------------------------------------------------------------------
r1090341 | scripty | 2010-02-15 04:25:36 +0000 (Mon, 15 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1090812 | scripty | 2010-02-16 04:33:07 +0000 (Tue, 16 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1091732 | gateau | 2010-02-17 12:13:06 +0000 (Wed, 17 Feb 2010) | 1 line

Backported r1091714: Turn Kopete Edit Message custom menu item into a dialog
------------------------------------------------------------------------
r1092225 | fschaefer | 2010-02-18 15:11:55 +0000 (Thu, 18 Feb 2010) | 12 lines

Make libv4l mandatory on Linux

Too many devices are not working properly without it.
See discussion on the kopete-devel-mailinglist (Jan/Feb 2010).

Backport of r1092217

CCMAIL: kde-buildsystem@kde.org
CCBUG: 195095                  
CCBUG: 165357                  
CCBUG: 166563

------------------------------------------------------------------------
r1092895 | rkcosta | 2010-02-19 17:20:50 +0000 (Fri, 19 Feb 2010) | 8 lines

Backport r1092894.

Correct ICQ registration website URL.

Patch by Panagiotis Papadopoulos, thanks!

CCMAIL: pano_90@gmx.net

------------------------------------------------------------------------
r1093604 | scripty | 2010-02-21 04:29:45 +0000 (Sun, 21 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1094001 | mzanetti | 2010-02-21 21:32:12 +0000 (Sun, 21 Feb 2010) | 5 lines

backport bugfix

CCBUG: 227015


------------------------------------------------------------------------
r1094068 | scripty | 2010-02-22 04:33:47 +0000 (Mon, 22 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
