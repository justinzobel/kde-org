---
aliases:
- ../announce-applications-16.12.0
changelog: true
date: 2016-12-15
description: KDE levererar KDE-program 16.12.0
layout: application
title: KDE levererar KDE-program 16.12.0
version: 16.12.0
---
15:e september, 2016. KDE introducerar idag KDE Program 16.12 med en imponerande samling uppgraderingar när det gäller bättre åtkomst, tillägg av mycket användbara funktioner, och att bli av med några småproblem, som för KDE Program ett steg närmare att erbjuda dig den perfekta lösningen för ditt system.

<a href='https://okular.kde.org/'>Okular</a>, <a href='https://konqueror.org/'>Konqueror</a>, <a href='https://www.kde.org/applications/utilities/kgpg/'>KGpg</a>, <a href='https://www.kde.org/applications/education/ktouch/'>KTouch</a>, <a href='https://www.kde.org/applications/education/kalzium/'>Kalzium</a>med flera (<a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Versionsfakta</a>) har nu konverterats till KDE Ramverk 5. Vi ser fram emot dina kommentarer och din insikt i de nyaste funktionerna som introduceras i den här utgåvan.

Som del av det fortgående arbetet med att göra det lättare att bygga fristående program, har vi delat de komprimerade arkiven för kde-baseapps, kdepim och kdewebdev. De nyskapade komprimerade arkiven finns i dokumentet <a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_we_have_split'>Versionsfakta</a>.

Följande paket har avvecklats: kdgantt2, gpgmepp and kuser. Det hjälper oss att fokusera på den återstående koden.

### Kwave ljudeditor ansluter sig till KDE program.

{{<figure src="https://www.kde.org/images/screenshots/kwave.png" width="600px" >}}

<a href='http://kwave.sourceforge.net/'>Kwave</a> är en ljudeditor. Det kan spela in, spela upp, importera och redigera många olika ljudfiler inklusive filer med flera kanaler. Kwave inkluderar några insticksprogram för att transformera ljudfiler på flera olika sätt, och erbjuder en grafisk visning med fullständig zoom- och panoreringsmöjlighet.

### Världen som skrivbordsunderlägg

{{<figure src="https://frinring.files.wordpress.com/2016/08/screenshot_20160804_171642.png" width="600px" >}}

Marble innehåller nu både ett skrivbordsunderlägg och en grafisk komponent för Plasma som visar tiden på en satellitvy av jorden, med visning av dag och natt i realtid. De var tidigare tillgängliga för Plasma 4, och har nu uppdaterats för att fungera med Plasma 5.

Du hittar mer information på <a href='https://frinring.wordpress.com/2016/08/04/wip-plasma-world-map-wallpaper-world-clock-applet-powered-by-marble/'>Friedrich W. H. Kossebaus blogg</a>.

### Smilisar i överflöd!

{{<figure src="/announcements/applications/16.12.0/kcharselect1612.png" width="600px" >}}

KCharSelect har fått möjlighet att visa smilisblocket i Unicode (och andra SMP-symbolblock).

Det har också fått en bokmärkesmeny så att du kan skapa favoriter för alla tecken du gillar.

### Matte är bättre med Julia

{{<figure src="https://2.bp.blogspot.com/-BzJNpF5SXZQ/V7skrKcQttI/AAAAAAAAAA8/7KD8g356FfAd9-ipPcWYi6QX5_nCQJFKgCLcB/s640/promo.png" width="600px" >}}

Cantor har ett nytt bakgrundsprogram för Julia, som ger användarna möjlighet att använda de senaste förbättringarna av vetenskapliga beräkningar.

Du hittar mer information på <a href='https://juliacantor.blogspot.com/2016/08/cantor-gets-support-of-julia-language.html'>Ivan Lakhtanovs blogg</a>.

### Avancerad arkivering

{{<figure src="https://rthomsen6.files.wordpress.com/2016/11/blog-1612-comp-method.png" width="600px" >}}

Ark har flera nya funktioner:

- Filer och kataloger kan nu döpas om, kopieras eller flyttas inom arkivet
- Det går nu att välja komprimerings- och krypteringsalgoritmer när arkiv skapas
- Ark kan nu öppna AR-filer (t.ex. Linux \*.a statiska bibliotek)

Du hittar mer information på <a href='https://rthomsen6.wordpress.com/2016/11/26/new-features-in-ark-16-12/'>Ragnar Thomsens blogg</a>.

### Med mera

Kopete har fått stöd för X-OAUTH2 SASL-behörighetskontroll i Jabber-protokollet och har rättat några problem med OTR-krypteringsinsticksprogrammet.

Kdenlive har en ny Rotoskopieffekt, stöd för nerladdningsbart innehåll och en uppdaterad rörelsedetektering. Det tillhandahåller också <a href='https://kdenlive.org/download/'>Snap- och AppImage</a>-filer för enklare installation.

Kmail och Akregator kan använda Googles säkra webbläsning för att kontrollera om en länk som klickas är skadlig. Båda har också återställt utskriftsstöd (kräver Qt 5.8).

### Offensiv skadedjursbekämpning

Mer än 130 fel har rättats i program, inklusive Dolphin, Akonadi, Adressboken, Anteckningar, Akregator, Cantor, Ark, Kdenlive med flera.

### Fullständig ändringslogg
