---
date: 2013-08-14
hidden: true
title: KDE-program 4.11 med enorma framsteg när det gäller personlig informationshantering
  och förbättringar överallt
---
Filhanteraren Dolphin inför många små rättningar och optimeringar i den här utgåvan. Inläsning av stora kataloger har snabbats upp och kräver up till 30 &#37; mindre minne. Omfattande disk- och processoraktivitet förhindras genom att bara läsa in förhandsgranskningar av de synliga objekten. Många fler förbättringar har gjorts, exempelvis har många fel som påverkade expanderade kataloger i detaljerad visning rättats, inga platsmarkörer med ikonen &quot;okänd&quot; visas längre när man går in i en katalog, och ett klick med musens mittenknapp öppnar nu en ny flik med arkivets innehåll, vilket skapar en mer likformig övergripande erfarenhet.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Det nya arbetsflödet för att skicka senare i Kontact` width="600px">}}

## Förbättringar av Kontact-sviten

Kontact-sviten har återigen haft betydande fokus på stabilitet, prestanda och minnesanvändning. Import av korgar, byta mellan kartor, hämta brev, markera eller flytta ett stort antal brev och starttider har alla förbättrats under de senaste sex månaderna. Se <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>den här bloggen</a> för detaljinformation. <a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/'>Arkivfunktionen har fått många fel rättade</a> och förbättringar av importguiden har också skett, vilket gör det möjligt att importera inställningar från e-postprogrammet Trojitá och ger bättre import från diverse andra program. Mer information finns <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>här</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kmail-archive-agent.png" caption=`Arkiveringsmodulen hanterar lagring av e-post i komprimerad form` width="600px">}}

Den här utgåvan levereras med några betydande nya funktioner. Det finns en <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>ny temaeditor för e-posthuvuden</a> och storleken på e-postbilder kan ändras i farten. Funktionen för att <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>skicka senare</a> gör det möjligt att schemalägga att brev skickas vid ett visst datum och en viss tid, med den ytterligare möjligheten att upprepa utskicket enligt ett angivet intervall. Stöd för Sieve-filter (en IMAP-funktion som möjliggör filtrering på servern) i Kmail har förbättrats, så att användare kan skapa filtreringsskript för Sieve <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-improve-sieve-support-22/'>med ett lättanvänt gränssnitt</a>. På säkerhetsområdet introducerar Kmail automatisk detektering av 'bedrägeribrev', <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>som visar en varning</a> när brev innehåller typiska trick för nätfiske. Nu får du ett <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>informativt meddelande</a> när nya brev anländer, och sist men inte minst levereras bloggverktyget Blogilo med en mycket förbättrad HTML-editor baserad på QtWebKit.

## Utökat språkstöd för Kate

Den avancerade texteditorn Kate introducerar nya insticksprogram: Python (2 och 3), Javascript & Jquery, Django och XML. De introducerar funktioner som statisk och dynamisk automatisk komplettering, syntaxkontroll, infoga textsnuttar och möjlighet att automatiskt indentera XML med en genväg. Men det finns mer för vänner av Python: en Python-terminal som tillhandahåller djuplodande information om en öppnad källkodsfil. Några mindre förbättringar av användargränssnittet har också gjorts, inklusive <a href='http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/'>nya passiva underrättelser för sökfunktionen</a>, <a href='http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/'>optimeringar av VIM-läget</a> och <a href='http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/'>ny funktion för textvikning</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kstars.png" caption=`Kstars visar intressanta kommande händelser synliga från din position` width="600px">}}

## Andra programförbättringar

I spel- och utbildningsarenan har flera mindre och större nya funktioner samt optimeringar anlänt. Blivande maskinskrivare kan gilla stödet för höger-till-vänster i Ktouch, medan stjärnskådarnas vän, Kstars, nu har ett verktyg som visar intressanta kommande händelser på din position. Matematikverktygen Rocs, Kig, Cantor och Kalgebra har alla uppmärksammats, och stöder fler bakgrundsprogram och beräkningar. Och spelet Hoppande kuben har nu större brädstorlekar, nya skicklighetsnivåer, snabbare svar och ett förbättrat användargränssnitt.

Det enkla ritprogrammet Kolourpaint kan hantera bildformatet WebP, och den universella dokumentvisaren Okular har inställningsbara granskningsverktyg samt introducerar stöd för ångra och gör om i formulär och kommentarer. Ljudspelaren och taggverktyget JuK stöder uppspelning och redigering av metadata för det nya ljudformatet Ogg Opus (det kräver dock att ljuddrivrutinen och TagLib också stöder Ogg Opus).

#### Installera KDE-program

KDE:s programvara, inklusive alla dess bibliotek och program, är fritt tillgänglig med licenser för öppen källkod. KDE:s programvara kör på diverse hårdvarukonfigurationer och processorarkitekturer, såsom ARM och x86, olika operativsystem och fungerar med alla sorters fönsterhanterare och skrivbordsmiljöer. Förutom Linux och andra UNIX-baserade operativsystem finns det versioner av de flesta KDE-program för Microsoft Windows på <a href='http://windows.kde.org'>KDE:s programvara för Windows</a>, och versioner för Apple Mac OS X på <a href='http://mac.kde.org/'>KDE:s programvara för Mac</a>. Experimentella byggen av KDE-program för diverse mobila plattformar som MeeGo, MS Windows Mobile och Symbian finns på webben, men stöds för närvarande inte. <a href='http://plasma-active.org'>Plasma Aktiv</a> erbjuder användningsmöjlighet på en större omfattning apparater, som läsplattor och annan mobil hårdvara. <br />

KDE:s programvara kan erhållas som källkod och som diverse binärformat från <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> och kan också erhållas på <a href='/download'>Cd-rom</a> eller med något av de <a href='/distributions'>större GNU/Linux- och UNIX-systemen</a> som levereras idag.

##### Paket

Vissa Linux- och UNIX-operativsystemleverantörer har vänligen tillhandahållit binärpaket av 4.11.0 för vissa versioner av sina distributioner, och i andra fall har volontärer i gemenskapen gjort det. <br />

##### Paketplatser

Besök <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Gemenskapens Wiki</a> för en aktuell lista med tillgängliga binärpaket som har kommit till KDE-projektets kännedom.

Fullständig källkod för 4.11.0 kan <a href='/info/4/4.11.0'>laddas ner fritt</a>. Instruktioner om hur man kompilerar och installerar KDE:s programvara 4.11.0 är tillgängliga på <a href='/info/4/4.11.0#binary'>informationssidan om 4.11.0</a>.

#### Systemkrav

För att få ut så mycket som möjligt av dessa utgåvor, rekommenderar vi att använda en aktuell version av Qt, såsom 4.8.4. Det är nödvändigt för att försäkra sig om en stabil och högpresterande upplevelse, eftersom vissa förbättringar som har gjorts av KDE:s programvara har i själva verket skett i det underliggande Qt-ramverket.<br /> För att helt dra nytta av kapaciteten i KDE:s programvara, rekommenderar vi också att använda de senaste grafikdrivrutinerna för systemet, eftersom det avsevärt kan förbättra användarupplevelsen både när det gäller tillvalsfunktioner, och övergripande prestanda och stabilitet.

## Också tillkännagivet idag:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma arbetsrymder 4.11 fortsätter förfina användarupplevelsen</a>

Plasma arbetsrymder växlar upp för långtidsunderhåll, och levererar ytterligare förbättringar av grundfunktionerna med en smidigare aktivitetsrad, smartare grafisk batterikomponent och förbättrad ljudmixer. Introduktionen av Kscreen ger arbetsrymderna intelligent hantering av flera bildskärmar, och storskaliga förbättringar av prestanda kombinerat med små justeringar av användbarhet leder till en trevligare övergripande upplevelse.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE plattform 4.11 levererar bättre prestanda</a>

Den här utgåvan av KDE plattform 4.11 fortsätter att fokusera på stabilitet. Nya funktioner implementeras för den framtida utgåvan av KDE:s Ramverk 5.0, men för den stabila utgåvan lyckades vi klämma in optimeringar för vårt Nepomuk-ramverk.
