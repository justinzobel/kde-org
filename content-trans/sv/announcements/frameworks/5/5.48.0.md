---
aliases:
- ../../kde-frameworks-5.48.0
date: 2018-07-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Konvertera återstående användningar av qDebug() till qcDebug(ATTICA)
- Rätta kontroll av ogiltig leverantörswebbadress
- Rätta felaktig webbadress till specifikation av programmeringsgränssnitt

### Baloo

- Ta bort oanvänd post X-KDE-DBus-ModuleName från metadata för kded-insticksprogram
- [tags_kio] Webbförfrågan ska var ett par med nyckel-värde
- Signalen för strömtillstånd ska bara skickas när strömtillståndet ändras
- baloodb: Gör ändringar av beskrivning av kommandoradsväljare efter namnbyte av prune -&gt; clean
- Visa duplicerade filnamn tydligt i etikettkataloger

### BluezQt

- Uppdatera D-Bus XML-filer att använda "Out*" för signaltyp Qt annotations
- Lägg till signaler för att enheters adress ändras

### Breeze-ikoner

- Använd också kvastliknande ikon för edit-clear-all
- Använd en kvastliknande ikon för edit-clear-history
- Ändra 24-bildpunkters ikonen view-media-artist

### Extra CMake-moduler

- Android: Gör det möjligt att överskrida ett måls APK-katalog
- Ta bort föråldrad QT_USE_FAST_OPERATOR_PLUS
- Lägg till -Wlogical-op -Wzero-as-null-pointer-constant i KF5-varningar
- [ECMGenerateHeaders] Lägg till alternativ för annan filändelse för deklarationsfiler än .h
- Inkludera inte 64 när 64-bitars arkitekturer byggs med flatpak

### KActivitiesStats

- Rätta plus ett fel i Cache::clear (fel 396175)
- Rätta flytt av ResultModel objekt (fel 396102)

### KCompletion

- Ta bort onödig inkludering av moc
- Säkerställ att KLineEdit::clearButtonClicked skickas

### KCoreAddons

- Ta bort Qt-definitioner duplicerade från KDEFrameworkCompilerSettings
- Gör så att det kompileras med strikta kompileringsflaggor
- Ta bort oanvänd nyckel X-KDE-DBus-ModuleName från metadata för test av tjänsttyp

### KCrash

- Reducera QT_DISABLE_DEPRECATED_BEFORE till minimalt beroende för Qt
- Ta bort Qt-definitioner duplicerade från KDEFrameworkCompilerSettings
- Säkerställ att byggning görs med strikta kompileringsflaggor

### KDeclarative

- Kontrollera att noden faktiskt har en giltig struktur (fel 395554)

### KDED

- KDEDModule definition av tjänsttyp: ta bort oanvänd nyckel X-KDE-DBus-ModuleName

### Stöd för KDELibs 4

- Ställ in QT_USE_FAST_OPERATOR_PLUS själva
- Exportera inte kf5-config till CMake inställningsfil
- Ta bort oanvänd post X-KDE-DBus-ModuleName från metadata för kded-insticksprogram

### KDE WebKit

- Konvertera KRun::runUrl() och KRun::run() till programmeringsgränssnitt som inte avråds från
- Konvertera KIO::Job::ui() -&gt; KIO::Job::uiDelegate()

### KFileMetaData

- Undvik kompilatorvarningar för taglib deklarationsfiler
- PopplerExtractor: Använd QByteArray() args direkt istället för 0-pekare.
- taglibextractor: Återställ extrahering av ljudegenskaper utan att taggar finns
- OdfExtractor: Hantera ovanliga prefixnamn
- Lägg inte till -ltag i det öppna länkgränssnittet
- Implementera taggen lyrics i taglibextractor
- Automatiska tester: Inbädda inte EmbeddedImageData redan i biblioteket
- Lägg till möjlighet att läsa inbäddade omslagsfiler
- Implementera läsning av betygstagg
- Kontrollera nödvändiga versioner av libavcode, libavformat och libavutil

### KGlobalAccel

- Uppdatera D-Bus XML-fil att använda "Out*" för signaltyp Qt annotations

### KHolidays

- holidays/plan2/holiday_jp_* - Lägg till saknad metainformation
- Uppdatera japanska helger (på japanska och engelska) (fel 365241)
- holidays/plan2/holiday_th_en-gb - uppdaterade Thailand (brittisk engelska) för 2018 (fel 393770)
- Lägg till helger i Venezula (spanska) (fel 334305)

### KI18n

- I cmake makrofil använd CMAKE_CURRENT_LIST_DIR konsekvent istället för blandad användning med KF5I18n_DIR
- KF5I18NMacros: Installera inte en tom katalog när inga po-filer finns

### KIconThemes

- Stöd att välja .ico-filer i anpassad ikonfilväljare (fel 233201)
- Stöd ikonen Scale från ikonnamngivningsspecifikation 0.13 (fel 365941)

### KIO

- Använd ny metod fastInsert överallt där det passar
- Återställ kompatibilitet för UDSEntry::insert, genom att lägga till metoden fastInsert
- Konvertera KLineEdit::setClearButtonShown -&gt; QLineEdit::setClearButtonEnabled
- Uppdatera D-Bus XML-fil att använda "Out*" för signaltyp Qt annotations
- Ta bort Qt-definition duplicerad från KDEFrameworkCompilerSettings
- Använd en korrekt emblemikon för skrivskyddade filer och kataloger (fel 360980)
- Gör det möjligt att gå upp till roten igen, i den grafiska filkomponenten
- [Egenskapsdialogruta] Förbättra några rättighetsrelaterade strängar (fel 96714)
- [KIO] Lägg till stöd för XDG_TEMPLATES_DIR i KNewFileMenu (fel 191632)
- Uppdatera papperskorgens docbook till 5.48
- [Egenskapsdialogruta] Gör alla fältvärden under fliken Allmänt markeringsbara (fel 105692)
- Ta bort oanvänd post X-KDE-DBus-ModuleName från metadata för kded-insticksprogram
- Aktivera jämförelse av KFileItems enligt webbadress
- [KFileItem] Kontrollera den mest lokala webbadressen om den är delad
- Rätta regression när binärdata klistras in från klippbordet

### Kirigami

- Mer konsekvent färg när musen hålls över
- Öppna inte undermeny för åtgärder utan underliggande alternativ
- Omstrukturera det globala verktygsradskonceptet (fel 395455)
- Hantera egenskapen enabled för enkla modeller
- Introducera ActionToolbar
- Rätta dra för att uppdatera
- Ta bort doxygen dokument för intern klass
- Länka inte med Qt5::DBus när DISABLE_DBUS anges
- Ingen extra marginal för overlaysheets i overlay
- Rätta menyn för Qt 5.9
- Kontrollera också egenskapen visible för åtgärden
- Bättre utseende/justering i kompakt läge
- Sök inte efter insticksprogram för varje platformTheme som skapas
- Bli av med mängden "custom"
- Lägg till nollställning för alla egna färger
- Konvertera färgläggning av verktygsknapp till eget colorSet
- Introducera eget color set
- Skrivbar buddyFor för att placera beteckningar med hänsyn till delobjekt
- När en annan bakgrundsfärg används, använd highlightedText som textfärg

### KNewStuff

- [KMoreTools] Aktivera att installera verktyg via appstream webbadress
- Ta bort KNS::Engine d-pekarfix

### KService

- Ta bort oanvänd nyckel X-KDE-DBus-ModuleName från metadata för test av tjänsttyp

### KTextEditor

- Skydda updateConfig för inaktiverade statusrader
- Lägg till sammanhangsberoende meny i statusraden för att växla mellan visning av total antal rader och ordantal
- Implementera att visa totalt antal rader i Kate (fel 387362)
- Gör så att verktygsradsknappar med menyinnehåll visar sina menyer med normalt klick istället för klicka och håll (fel 353747)
- CVE-2018-10361: eskalering av behörighet
- Rätta insättningsbredd (fel 391518)

### Kwayland

- [server] Skicka händelsen frame istället för flush vid relativ pekarrörelse (fel 395815)
- Rätta XDGV6 test av meddelanderuta
- Rätta dumt kopieringsfel i XDGShellV6-klient
- Avbryt inte gammal klippbordsmarkering om den är samma som den nya (fel 395366)
- Ta hänsyn till BUILD_TESTING
- Rätta några stavfel föreslagna av nytt lint-verktyg
- Lägg till arclint-filen i kwayland
- Korrigera @since för programmeringsgränssnittet skip switcher

### KWidgetsAddons

- [KMessageWidget] Uppdatera stilmall när palett ändras
- Uppdatera kcharselect-data till Unicode 11.0
- [KCharSelect] Konvertera generate-datafile.py till Python 3
- [KCharSelect] Förbered översättningar för uppdatering till Unicode 11.0

### ModemManagerQt

- Implementera stöd för gränssnitten Voice och Call
- Ställ inte in egna domänfilterregler

### Oxygen-ikoner

- Visa en ikon för dolda filer i Dolphin (fel 395963)

### Plasma ramverk

- FrameSvg: Uppdatera maskeringsram om bildsökvägen har ändrats
- FrameSvg: Förstör inte delade maskeringsramar
- FrameSvg: Förenkla updateSizes
- Ikoner för tangentbordsindikator T9050
- Rätta färg för mediaikon
- FrameSvg: Lagra maskFrame i cache igen om enabledBorders har ändrats (fel 391659)
- FrameSvg: Rita bara hörn om båda kanterna i båda riktningarna är aktiverade
- Lär ContainmentInterface::processMimeData hur släpp från aktivitetshanteraren hanteras
- FrameSVG: Ta bort redundanta kontroller
- FrameSVG: Rätta inkludering av QObject
- Använd QDateTime för gränssnitt med QML (fel 394423)

### Syfte

- Lägg till alternativet Dela i Dolphins sammanhangsberoende meny
- Nollställ insticksprogram på rätt sätt
- Filtrera bort duplicerade insticksprogram

### QQC2StyleBridge

- Inga bildpunktsvärden i checkindicator
- Använd RadioIndicator för alla
- Undvik överflöde omkring meddelanderutor
- Med Qt&lt;5.11 ignoreras kontrollpaletten fullständigt
- Förankra knappbakgrund

### Solid

- Rätta enhetsbeteckning med okänd storlek

### Syntaxfärgläggning

- Rättningar av Java kommentarer
- Färglägg också Gradle-filer med Groovy-syntax
- CMake: Rätta färgläggning efter strängar med en enda <code>@</code> symbol
- CoffeeScript: Lägg till filändelse .cson
- Rust: Lägg till nyckelord och bytes, rätta identifierare, och andra förbättringar/rättningar
- Awk: Rätta reguljärt uttryck i en funktion och uppdatera syntax för gawk
- Pony: Rätta undantag med enkla citationstecken och möjlig oändlig snurra med #
- Uppdatera CMake syntax för kommande utgåva 3.12

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
