---
aliases:
- ../../kde-frameworks-5.75.0
date: 2020-10-10
layout: framework
libCount: 70
qtversion: 5.12
---
### Baloo

+ [AdvancedQueryParser] Mildra tolkning av sträng som slutar med parenteser
+ [AdvancedQueryParser] Mildra tolkning av sträng som slutar med en jämförelse
+ [AdvancedQueryParser] Rätta åtkomst utanför gräns om sista tecknet är en jämförelse
+ [Term] Ersätt konstruktorn Term::Private med förvalda värden
+ [BasicIndexingJob] Kortslut hämtning av XAttr för filer utan attribut
+ [extractor] Rätta dokumenttyp i extraheringsresultat
+ Licensiera om fil till LGPL-2.0-or-later

### BluezQt

+ Lägg till egenskapen rfkill till hanterare
+ Lägg till statusegenskap i rfkill
+ Registrera Rfkill för QML
+ Exportera Rfkill
+ Stöd att tillhandahålla tjänstdatavärden för LE-tillkännagöranden

### Breeze-ikoner

+ Lägg till ny generell "beteendeikon"
+ Gör så att ikonvalidering bara beror på ikongenerering om aktiverad
+ Ersätt 24 bildpunkters ikon bash-skript med python-skript
+ Använd ikonografi med flaggstil för view-calendar-holiday
+ Lägg till Plasma Nano logotyp
+ Lägg till application-x-kmymoney
+ Lägg till ikon för KMyMoney

### Extra CMake-moduler

+ rätta fetch-translations för invent webbadresser
+ Inkludera FeatureSummary och hitta moduler
+ Introducera rimlighetskontroller för utgående licens
+ Lägg till CheckAtomic.cmake
+ Rätta inställning med pthread på Android 32-bitar
+ lägg till parametern RENAME i ecm_generate_dbus_service_file
+ Rätta find_library på Android med NDK &lt; 22
+ Sortera Android versionslista explicit
+ Lagra Android {min,target,compile}Sdk i variabler

### KDE Doxygen-verktyg

+ Licensieringsförbättringar
+ Rätta api.kde.org på mobil
+ Gör api.kde.org till PWA

### KArchive

+ Licensiera om fil till LGPL-2.0-or-later

### KAuth

+ använd ny installationsvariabel (fel 415938)
+ Markera David Edmundson som underhållsansvarig för KAuth

### KCalendarCore

+ Licensiera om fil till LGPL-2.0-or-later

### KCMUtils

+ Ta bort hantering av insideshändelser från flikfix (fel 423080)

### KCompletion

+ Licensera om filer till LGPL-2.0-or-later

### KConfig

+ CMake: Ställ också in SKIP_AUTOUIC för genererade filer
+ Använd omvänd ordning i KDesktopFile::locateLocal för att iterera över generella inställningssökvägar

### KConfigWidgets

+ Rätta isDefault som gjorde att KCModule inte uppdaterade sitt förvalda tillstånd riktigt
+ [kcolorscheme]: Lägg till isColorSetSupported för att kontrollera om ett färgschema har en given färgmängd
+ [kcolorscheme] Läs egna inaktiva färger för bakgrunderna riktigt

### KContacts

+ Ta bort föråldrad licensfil för LGPL-2.0-only
+ Licensera om filer till LGPL-2.0-or-later

### KCoreAddons

+ KJob: skicka result() och finished() som mest en gång
+ Lägg till skyddad hämtningsfunktion KJob::isFinished()
+ Avråd från KRandomSequence till förmån för QRandomGenerator
+ initiera variabel i huvudklass + gör variabel/pekare konstanta
+ Gör meddelandebaserade tester robustare för miljö (fel 387006)
+ förenkla bevakningstest för qrc (fel 387006)
+ räkna referenser för och ta bort instanser av KDirWatchPrivate (fel 423928)
+ Avråd från KBackup::backupFile() på grund av funktionsförlust
+ Avråd från KBackup::rcsBackupFile(...) beroende på inga kända användare

### KDBusAddons

+ Licensera om filer till LGPL-2.0-or-later

### KDeclarative

+ QML för I18n är tillagd i KF 5.17
+ Licensera om filer till LGPL-2.0-or-later
+ Blockera genvägar medan tangentföljder spelas in (fel 425979)
+ Lägg till SettingHighlighter som en manuell version av färgläggningen som görs av SettingStateBinding

### Stöd för KDELibs 4

+ KStandardDirs: lös alltid upp symboliska länkar för inställningsfiler

### KHolidays

+ Tog bort kommentarer för beskrivningsfält för mt_* helgfiler
+ Lägg till nationella helger i Malta både för engelska (en-gb) och maltesiska (mt)

### KI18n

+ Licensiera om fil till LGPL-2.0-or-later

### KIO

+ KUrlNavigator: använd alltid "desktop:/" inte "desktop:"
+ Stöd DuckDuckGo syntax med nummertecken och utropstecken i Webshortcuts (fel 374637)
+ KNewFileMenu: KIO::mostLocalUrl är bara användbar med :local protokoll
+ Avråd från KIO::pixmapForUrl
+ kio_trash: ta bort onödigt strikta rättighetskontroller (fel 76380)
+ OpenUrlJob: hantera alla textskript konsekvent (fel 425177)
+ KProcessRunner: mer systemd metadata
+ KDirOperator: anropa inte setCurrentItem för en tom webbadress (fel 425163)
+ KNewFileMenu: rätta att skapa nya kataloger med namn som börjar med ':' (fel 425396)
+ StatJob: klargör att mostLocalUrl bara fungerar med :local protokoll
+ Dokumentera hur man lägger till nya "slumpmässiga" roller i kfileplacesmodel.h
+ Ta bort gammal kio_fonts fix i KCoreDirLister, värddatornamn avkortades felaktigt
+ KUrlCompletion: hantera protokollen ":local" som använder värddatornamn i webbadress
+ Dela koden i metoden addServiceActionsTo till mindre metoder
+ [kio] FEL: Tillåta dubbla citationstecken i dialogrutor för öppna och spara (fel 185433)
+ StatJob: avbryt jobb om webbadress är ogiltig (fel 426367)
+ Anslut slots explicit istället för att använda automatisk anslutning
+ Gör så att programmeringsgränssnittet filesharingpage faktiskt fungerar

### Kirigami

+ AbstractApplicationHeader: förankrad ifyllnad istället för positionsberoende förankring
+ Förbättra utseende och följdriktighet för GlobalDrawerActionItem
+ Ta bort formulärindentering för smala layouter
+ Återställ "tillåt att rubrikfärgerna anpassas"
+ tillåt att rubrikfärgerna anpassas
+ Lägg till saknad @since i egenskaperna för målat område
+ Introducera QtQuick egenskaperna paintedWidth/paintedHeight för bildstil
+ Lägg till en platsmarkör för bildegenskap i ikon (i reservstilen)
+ Ta bort ikonens egna beteende för implicitWidth/implicitHeight
+ Vakta potentiell null-pekare (visar sig vara förvånande vanligt)
+ protected setStatus
+ Introducera statusegenskap
+ Stöd ImageResponse och Texture typ av bildleverantörer i Kirigami::Icon
+ Varna folk för att använda ScrollView i ScrollablePage
+ Återställ "visa alltid avskiljare"
+ gör så att mobilemode stöder egna rubrikdelegater
+ Dölj länkstigens avskiljarlinje om knapparnas layout är synlig men har bredden 0 (fel 426738)
+ [icon] Anse att ikonen är ogiltig när källan är en tom webbadress
+ Ändra Units.fontMetrics så att FontMetrics faktiskt används
+ Lägg till egenskapen Kirigami.FormData.labelAlignment
+ visa alltid avskiljare
+ Använd rubrikfärger för skrivbordsstil AbstractApplicationHeader
+ Använd komponentens sammanhang när delegater skapas för ToolBarLayout
+ Avbryt och ta bort inkubatorer när ToolBarLayoutDelegate tas bort
+ Ta bort åtgärder och delegater från ToolBarLayout när de förstörs (fel 425670)
+ Ersätt användning av C-stil typkonvertering av pekare i sizegroup
+ binära konstanter är en utökning i C++14
+ sizegroup: Rätta att enum inte hanterar varningar
+ sizegroup: Rätta anslutningar med tre argument
+ sizegroup: Lägg till CONSTANT i signal
+ Rätta några fall där intervallsnurror används för icke-konstanta Qt-behållare
+ Begränsa knapphöjden i global verktygsrad
+ Visa en avskiljare mellan länkstigen och ikonerna till vänster
+ Använd KDE_INSTALL_TARGETS_DEFAULT_ARGS
+ Ändra storlek på ApplicationHeaders genom att använda SizeGroup
+ Introducera SizeGroup
+ Rättning: gör så att uppdateringsindikator visas ovanför listrubriker
+ lägg overlaysheets över lådor

### KItemModels

+ ignorera sourceDataChanged för ogiltiga index
+ Stöd för KDescendantProxyModel "hopdragna" noder

### KNewStuff

+ Spåra manuellt livscykeln för vårt interna körprogram i kpackage
+ Uppdatera versioner för "låtsasuppdateringar" av kpackage (fel 427201)
+ Använd inte förvald parameter när det inte behövs
+ Rätta krasch när kpackages installeras (fel 426732)
+ Detektera när cachen ändras och reagera i enlighet med det
+ Rätta uppdatering av post om versionsnummer är tomt (fel 417510)
+ Gör pekare konstant + initiera variabel i huvud
+ Licensiera om fil till LGPL-2.0-or-later
+ Acceptera förslag att ta över underhållsansvar

### KNotification

+ Sänk Android-insticksprogram tills ny Gradle är tillgänglig
+ Använd Android SDK versioner från ECM
+ Avråd från att konstruktorn KNotification har grafisk komponentparameter

### Ramverket KPackage

+ Rätta D-Bus underrättelser när installerat eller uppdaterat
+ Licensiera om fil till LGPL-2.0-or-later

### KParts

+ Installera definitionsfiler för tjänsttyperna krop och krwp enligt filnamn matchande typ

### KQuickCharts

+ Undvik bindningssnurra inne i Legend
+ Ta bort check av GLES3 i SDFShader (fel 426458)

### Kör program

+ Lägg till egenskapen matchRegex för att förhindra att onödiga trådar startas
+ Tillåt att ange åtgärder i QueryMatch
+ Tillåt att ange individuella åtgärder för matchningar av D-Bus körprogram
+ Licensera om filer till LGPL-2.0-or-later
+ Lägg till min bokstavsantal egenskap
+ Ta hänsyn till XDG_DATA_HOME miljövariabel för mallinstallationskataloger
+ Förbättra felmeddelanden för D-Bus körprogram
+ Börja skicka ut varningar om metadatakonvertering vid körtid

### KService

+ ta tillbaka disableAutoRebuild från randen (fel 423931)

### KTextEditor

+ [kateprinter] Konvertera från QPrinter metoder som avråds från
+ Skapa inte tillfällig buffert för att detektera Mime-typ för sparad lokal fil
+ undvik frysning på grund av att ordlista och trigram läses in när första tecknet skrivs
+ [Vi-läge] Visa alltid a-z buffrar med små bokstäver
+ [KateFadeEffect] skicka hideAnimationFinished() när en uttoning avbryts av en intoning
+ säkerställ bildpunktsperfekt kant även för skalad återgivning
+ säkerställ att vi målar över kantavskiljaren över alla andra saker såsom färgläggning av vikning
+ flytta separator från mellan ikonkant och radnummer till mellan rad och text
+ [Vi-läge] Rätta beteende hos numrerade register
+ [Vi-läge] Placera borttagen text i rätt register
+ Återställ beteende för sökning vald när ingen markering tillgänglig
+ inga ytterligare bara LGPL-2.1 eller bara LGPL-3.0 filer
+ licensiera om filer till LGPL-2.0-or-later
+ använd icke nödvändig set metod för vissa temafärger
+ 5.75 är inkompatibel en gång, förval till 'Automatiskt färgtemaval' för teman
+ ändra schema =&gt; tema i koden för att undvika förvirring
+ avkorta föreslaget licenshuvud till nuvarande tillstånd
+ säkerställ att vi alltid slutar med något giltigt tema
+ förbättra dialogrutan Kopiera...
+ rätta mer ny =&gt; kopiera namngivning
+ lägg till någon KMessageWidget som ger tips att kopiera skrivskyddade teman, byt namn påny =&gt; kopiera
+ inaktivera redigering av skrivskyddade teman
+ spara färgläggningsspecifika stilöverskridningar fungerar, bara skillnader sparas
+ förenkla att skapa egenskaper, genomskinliga färger hanteras nu riktigt i Format
+ försök att begränsa export till ändrade egenskaper, det fungerar delvis, men vi exporterar fortfarande fel namn för inkluderade definitioner
+ börja beräkna 'riktiga' standardvärden baserade på aktuellt tema och format utan stilöverskridningar för färgläggningen
+ rätta återställningsåtgärd
+ lagra syntaxspecifika överskridningar, för närvarande lagras bara allt som lästes in i trädvyn
+ börja arbeta på specifika överskridningar av syntaxfärgläggning, för närvarande, visa bara stilen som en färgläggning faktiskt själv har
+ tillåt att ändringar av standardstil sparas
+ tillåt att färgändringar sparas
+ implementera temaexport: enkel filkopiering
+ ingen specifik import/export för färgläggning, är inte vettigt med nytt temaformat
+ implementera tema filimport
+ ny och ta bort fungerar för tema, ny kopierar det aktuella temat som startpunkt
+ använd temafärger överallt
+ riv ut mer gammal schemakod till förmån för KSyntaxHighlighting::Theme
+ börja använda färgerna som är inställda av temat, utan egen logik omkring det
+ initialisera m_pasteSelection och öka UI-filversion
+ lägg till genväg för inklistring av musmarkering
+ undvik setTheme, vi kan bara skicka vårt tema till hjälpfunktioner
+ rätta verktygstips, det nollställer bara till standardtema
+ exportera standardinställningen av stil till json-tema
+ börja arbeta på json-export för tema, aktiverat genom att använda filändelsen .theme i exportdialogrutan
+ byt namn på 'Använd KDE färgtema' till 'Använd standardfärger', det är den verkliga effekten
+ leverera inte normalt tomt 'KDE'-tema
+ stöd automatisk val av rätt tema för aktuellt Qt/KDE färgtema
+ konvertera gamla temanamn till nya, använd ny inställningsfil, överför data en gång
+ flytta teckensnittsinställning till utseende, byt namn på schema =&gt; färgtema
+ ta bort hårdkodat förvalt temanamn, använd åtkomstfunktioner i KSyntaxHighlighting
+ läs in reservfärger från tema
+ lägg inte in inbäddade färger alls
+ använd rätt funktion för att slå upp tema
+ editorfärger används nu från tema
+ använd KSyntaxHighlighting::Theme::EditorColorRole uppräkningstyp
+ hantera övergången från Normal =&gt; Förval
+ första steg: läs in temalista från KSyntaxHighlighting, nu när vi redan har dem som kända scheman i KTextEditor

### KUnitConversion

+ Använd stora bokstäver för bränsleeffektivitet

### Kwayland

+ Lagra inte QList::end() iterator i cache om erase() används

### KWidgetsAddons

+ kviewstateserializer.cpp - vakt för krascher i restoreScrollBarState()

### KWindowSystem

+ Ändra licens i filer för att vara kompatibla med LGPL-2.1

### KXMLGUI

+ [kmainwindow] Ta inte bort poster från en ogiltig kconfiggroup (fel 427236)
+ Överlappa inte huvudfönster när ytterligare instanser öppnas (fel 426725)
+ [kmainwindow] Skapa inte inbyggda fönster för annat än toppnivåfönster (fel 424024)
+ KAboutApplicationDialog: undvik tom flik "Bibliotek" om HideKdeVersion är satt
+ Visa språkkod förutom (översatt) språknamn i dialogrutan för byte av programmets språk
+ Avråd från KShortcutsEditor::undoChanges() till förmån för ny undo()
+ Hantera dubbel stängning i huvudfönster (fel 416728)

### Plasma ramverk

+ Rätta att plasmoidheading.svgz installeras på fel ställe (fel 426537)
+ Tillhandahåll värdet lastModified i ThemeTest
+ Detektera att vi letar efter ett tomt element och avsluta tidigt
+ Gör så att PlasmaComponents3 verktygstips använder den typiska stilen för verktygstips (fel 424506)
+ Använd rubrikfärger PlasmoidHeading-rubriker
+ Ändra förflyttningsanimering av PC2 TabBar färgläggning genom lättnad av typen till OutCubic
+ Lägg till stöd för färgmängd Tooltip
+ Rätta att PC3 ikoner för Button/ToolButton inte alltid har rätt färg inställd (fel 426556)
+ Säkerställ att FrameSvg använder lastModified tidsstämpel när cache används (fel 426674)
+ Säkerställ att vi alltid har en giltig lastModified tidsstämpel när setImagePath anropas
+ Avråd från lastModified tidsstämpel 0 i Theme::findInCache (fel 426674)
+ Anpassa import av QQC2 till nytt versionsschema
+ [windowthumbnail] Verifiera att relevant GLContext finns, inte vilket som helst
+ Lägg till saknad import av PlasmaCore i ButtonRow.qml
+ Rätta några fler referensfel i PlasmaExtras.ListItem
+ Rätta fel för implicitBackgroundWidth i PlasmaExtras.ListItem
+ Kalla redigeringsläge "Edit Mode"
+ Rätta TypeError i QueryDialog.qml
+ Rätta ReferenceError till PlasmaCore i Button.qml

### QQC2StyleBridge

+ Använd också färgläggningens textfärg när checkDelegate är färglagd (fel 427022)
+ Respektera rullningslistens inställning av klick för att gå till position (fel 412685)
+ Ärv normalt inte färger i skrivbordets verktygsradsstil
+ Licensiera om fil till LGPL-2.0-or-later
+ Använd bara rubrikfärger för rubrikverktygsrader
+ Flytta deklaration av färgmängd till en plats där den kan överskridas
+ Använd rubrikfärger för verktygsrad med skrivbordsstil
+ lägg till den saknade egenskapen isItem nödvändig för träd

### Sonnet

+ Degradera trigram-utmatning

### Syntaxfärgläggning

+ AppArmor: rätta reguljärt uttryck för detektering av sökvägar
+ AppArmor: uppdatera färgläggning av AppArmor 3.0
+ färgcache för konvertering från rgb till ansi256colors (snabbar upp inläsning av markdown)
+ SELinux: använd nyckelord include
+ SubRip-textning och Logcat: små förbättringar
+ generator för doxygenlua.xml
+ Rätta doxygen latex-formler (fel 426466)
+ använd Q_ASSERT såsom i återstående ramverk + rätta assert
+ byt namn på --format-trace till --syntax-trace
+ använd en stil på områden
+ följ sammanhang och områden
+ använd normalt editorns bakgrundsfärg
+ ANSI färgläggning
+ Lägg till copyright och uppdatera avskiljarens färg i temat Radikal
+ Uppdatera avskiljarens färg i temat Solariserad
+ Förbättra färg på avskiljare och ikonkant för mörka Ayu, Nord och Vim teman
+ gör avskiljarens färg mindre störande
+ Importera Kate-schema till temakonverterare av Juraj Oravec
+ mer framträdande avsnitt om licensering, länka till vår kopia av MIT.txt
+ första mallen för base16-generator, https://github.com/chriskempson/base16
+ lägg till riktig licensinformation i alla teman
+ Lägg till färgtemat Radikal
+ Lägg till färgtemat Nord
+ förbättra temamonter för att visa fler stilar
+ lägg till ljust och mörkt gruvbox tema, med MIT licens
+ Lägg till färgtemat ayu (med varianterna ljus, mörk och hägring)
+ Lägg till POSIX alternativ för enkel variabeltilldelning
+ verktyg för att generera en graf från en syntaxfil
+ rätta automatisk konvertering för ej tilldelad färg QRgb == 0 till "black" istället för "transparent"
+ Lägg till Debian ändringslogg och kontrollexempelfiler
+ lägg till färgtemat 'Dracula'

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
