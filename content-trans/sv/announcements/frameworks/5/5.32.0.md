---
aliases:
- ../../kde-frameworks-5.32.0
date: 2017-03-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Implementera nästlade etiketter

### Breeze-ikoner

- Tillägg av ikoner för Plasma Vault
- Bytte namn på ikoner för krypterade och okrypterade kataloger
- Lägg till 22 bildpunkters torrent-ikon
- Lägg till nm-ikoner för bricka (fel 374672)
- color-management: Tog bort odefinierade länkar (fel 374843)
- system-run är nu en åtgärd &lt;= 32 bildpunkter och 48 bildpunkter en programikon (fel 375970)

### Extra CMake-moduler

- Detektera inotify
- Återställ "Markera automatiskt klasser med rent virtuella funktioner som /Abstract/."

### KActivitiesStats

- Tillåt att planera i förväg och ange ordning för ett objekt som ännu inte finns i listan

### KArchive

- Rätta möjlig minnesläcka utpekad av 'limitedDev'

### KCMUtils

- Rättade möjlig krasch i QML inställningsmoduler när programmets palett ändras

### KConfig

- KConfig: Stoppa export och installera KConfigBackend

### KConfigWidgets

- KColorScheme: Använd programmets schema som förval om inställt av KColorSchemeManager (fel 373764)
- KConfigDialogManager: Hämta ändringssignal från metaObject eller specialegenskap
- Rätta felkontroll i KCModule::setAuthAction

### KCoreAddons

- Undanta (6) från igenkänning av smilisar
- KDirWatch: Rätta minnesläcka vid destruering

### Stöd för KDELibs 4

- Rätta fel i kfiledialog.cpp som orsakar krasch när inbyggda grafiska komponenter används

### KDocTools

- meinproc5: Länka till filerna, inte till biblioteket (fel 377406)
- Ta bort det statiska biblioteket KF5::XsltKde
- Exportera ett riktigt delat bibliotek för KDocTools
- Konvertera till kategoriserad loggning och städa inkluderade filer
- Lägg till funktion för att extrahera en enstaka fil
- Misslyckas bygga tidigt om xmllint inte är tillgänglig (fel 376246)

### KFileMetaData

- Ny underhållsansvarig för kfilemetadata
- [ExtractorCollection] Använd arv av Mime-typ för att returnera insticksprogram
- Lägg till en ny egenskap DiscNumber för ljudfiler från album med flera skivor

### KIO

- Inställningsmodulen av kakor: Inaktivera knappen "Ta bort" när det inte finns något aktuellt objekt
- kio_help: Använd det nya delade biblioteket exporterat av KDocTools
- kpac: Sanera webbadresser innan de skickas till FindProxyForURL (säkerhetsrättning)
- Importera I/O-fjärrslavar från plasma-workspace
- kio_trash: Implementera namnändring av filer och kataloger på toppnivå
- PreviewJob: Ta bort maximal storlek för lokala filer som standard
- DropJob: Tillåt att lägga till programåtgärder i en öppen meny
- ThumbCreator: Avråd från användning av DrawFrame, som diskuterats i https://git.reviewboard.kde.org/r/129921/

### KNotification

- Lägg till stöd för flatpak-portaler
- Skicka desktopfilename som en del av tips i notifyByPopup
- [KStatusNotifierItem] Återställ minimerat fönster som normalt

### Ramverket KPackage

- Slutför stöd för att öppna komprimerade paket

### KTextEditor

- Kom ihåg filtyp inställd av användaren mellan sessioner
- Nollställ filtyp när webbadress öppnas
- Lägg till en hämtningsmetod för inställningsvärdet word-count
- Konsekvent konvertering från/till markör till/från koordinater
- Uppdatera bara filtyp när den sparas om sökvägen ändras
- Stöd för inställningsfiler för EditorConfig (för detaljerad information: http://editorconfig.org/)
- Lägg till FindEditorConfig i ktexteditor
- Rättning: Åtgärden emmetToggleComment fungerar inte (fel 375159)
- Använd skiftläge med meningsstil för beteckningstexter i redigeringsfält
- Vänd på betydelsen av :split, :vsplit för att motsvara åtgärderna i vi och Kate
- Använd C++11 log2() istället för log() / log(2)
- KateSaveConfigTab: Lägg distans efter sista gruppen under fliken Avancerat, inte inne i den
- KateSaveConfigTab: Ta bort felaktig marginal omkring innehållet under fliken Avancerat
- Kanter på delsida i inställningar: Rätta synlighet av rullningslist kombinationsruta felplacerad

### KWidgetsAddons

- KToolTipWidget: Dölj verktygstips i enterEvent om hideDelay är noll
- Rätta att KEditListWidget förlorar fokus vid klick på knappar
- Lägg till uppdelning av Hangul-stavelser i Hangul Jamo
- KMessageWidget: Rätta beteende vid överlappande anrop av animatedShow/animatedHide

### KXMLGUI

- Använd inte KConfig-nycklar med bakstreck

### NetworkManagerQt

- Synkronisera introspektion och skapade filer med NM 1.6.0
- Manager: Rätta att deviceAdded skickas två gånger när NM startar om

### Plasma ramverk

- Ställ in standardtips när repr inte exporterar Layout.* (fel 377153)
- Möjlighet att ställa in expanded=false för en omgivning
- [Menu] Förbättrade tillgänglig utrymmeskorrigering för openRelative
- Flytta logik från setImagePath in i updateFrameData() (fel 376754)
- IconItem: Lägg till egenskapen roundToIconSize
- [SliderStyle] Gör det möjligt att tillhandahålla elementet "hint-handle-size"
- Anslut alla anslutningar till åtgärder i QMenuItem::setAction
- [ConfigView] Respektera KIOSK-inställningsmodulens begränsningar
- Rätta inaktivering av spinnande animering när upptagetindikatorn inte har någon genomskinlighet
- [FrameSvgItemMargins] Uppdatera inte vid repaintNeeded
- Miniprogramikoner för Plasma Vault
- Konvertera AppearAnimation och DisappearAnimation till Animators
- Justera underkant till överkant för visualParent i fallet TopPosedLeftAlignedPopup
- [ConfigModel] Skicka dataChanged när en ConfigCategory ändras
- [ScrollViewStyle] Utvärdera frameVisible på riktigt sätt
- [Button Styles] Använd Layout.fillHeight istället för parent.height i en Layout (fel 375911)
- [ContainmentInterface] Justera också omgivningens sammanhangsberoende meny till panelen

### Prison

- Rätta minsta Qt-version

### Solid

- Disketter visas nu som "Diskett" istället för "0 B flyttbart medium"

### Syntaxfärgläggning

- Lägg till fler nyckelord. Inaktivera stavningskontroll för nyckelord
- Lägg till fler nyckelord
- Lägg till filändelsen *.RHTML för Ruby on Rails-syntaxfärgläggning (fel 375266)
- Uppdatera SCSS- och CSS-syntaxfärgläggning (fel 376005)
- less-färgläggning: Rätta enradskommentarer som startar nya regioner
- Latex-färgläggning: Rätta justerad omgivning (fel 373286)

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
