---
aliases:
- ../../kde-frameworks-5.1
- ./5.1
customIntro: true
date: '2014-08-08'
description: KDE levererar andra alfaversion av Ramverk 5.
layout: framework
qtversion: 5.2
title: Andra utgåvan av KDE Ramverk 5
---
7:e augusti 2014. KDE tillkännager idag den andra utgåvan av KDE Ramverk 5. I enlighet med den planerade utgivningspolicyn för KDE Ramverk kommer denna utgåva en månad efter den ursprungliga versionen, och har både felrättningar och nya funktioner.

{{% i18n "annc-frameworks-intro" "60" "/announcements/frameworks/5/5.0" %}}

## {{< i18n "annc-frameworks-new" >}}

This release, versioned 5.1, comes with a number of bugfixes and new features including:

- KTextEditor: Major refactorings and improvements of the vi-mode
- KAuth: Now based on PolkitQt5-1
- New migration agent for KWallet
- Windows compilation fixes
- Translation fixes
- New install dir for KXmlGui files and for AppStream metainfo
- <a href='http://www.proli.net/2014/08/04/taking-advantage-of-opengl-from-plasma/'>Plasma Taking advantage of OpenGL</a>
