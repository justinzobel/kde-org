---
aliases:
- ../announce-applications-19.08.0
changelog: true
date: 2019-08-15
description: Es distribueixen les aplicacions 19.08 del KDE.
layout: application
release: applications-19.08.0
title: KDE distribueix les aplicacions 19.08.0 del KDE
version: '19.08'
version_number: 19.08.0
---
{{< peertube "/6443ce38-0a96-4b49-8fc5-a50832ed93ce" >}}

{{% i18n_date %}}

La comunitat KDE es complau d'anunciar la publicació de les Aplicacions 19.08 del KDE.

Aquest llançament forma part del compromís del KDE de proporcionar contínuament versions millorades dels programes que lliurem als nostres usuaris. Les noves versions de les Aplicacions ofereixen més característiques i un programari millor dissenyat que augmentarà la usabilitat i l'estabilitat d'aplicacions com el Dolphin, Konsole, Kate, Okular i totes les vostres altres utilitats preferides del KDE. El nostre objectiu és garantir que seguiu sent productiu i fer que el programari KDE sigui més fàcil i més agradable d'emprar.

Esperem que gaudiu de totes les noves millores que trobareu a la versió 19.08!

## Novetats a les Aplicacions 19.08 del KDE

S'han resolt més de 170 errors. Aquestes esmenes tornen a implementar característiques inhabilitades, normalitzen les dreceres i solucionen les fallades, fent que les vostres aplicacions siguin més fàcils d'emprar i permetent-vos treballar i jugar de manera més intel·ligent.

### Dolphin

El Dolphin és l'explorador de fitxers i carpetes del KDE que ara podreu llançar des de qualsevol lloc usant la nova drecera global <keycap>Meta + E</keycap>. També hi ha una nova característica que minimitzarà el desordre al vostre escriptori. Quan el Dolphin ja s'estigui executant, si obriu carpetes amb altres aplicacions, aquestes carpetes s'obriran en una pestanya nova a la finestra existent en lloc d'una finestra nova del Dolphin. Recordeu que aquest comportament ara està activat de manera predeterminada, però també el podreu desactivar.

El plafó d'informació (ubicat de manera predeterminada a la dreta del plafó principal del Dolphin) s'ha millorat de diverses maneres. Podreu, per exemple, triar reproduir automàticament els fitxers multimèdia quan els ressalteu al plafó principal, i ara podreu seleccionar i copiar el text que es mostra en el plafó. Si voleu canviar el que mostra el plafó informatiu, podreu fer-ho allà mateix, ja que el Dolphin no obrirà una finestra separada quan escolliu configurar el plafó.

També hem resolt molts dels defectes i petits errors, per assegurar que la vostra experiència en general emprant el Dolphin sigui molt més fluida.

{{<figure src="/announcements/applications/19.08.0/dolphin_bookmark.png" alt=`La nova característica Adreces d'interès del Dolphin` caption=`La nova característica Adreces d'interès del Dolphin` width="600px" >}}

### Gwenview

El Gwenview és el visualitzador d'imatges del KDE, i en aquest llançament els desenvolupadors han millorat la seva característica de visualització de miniatures en tots els àmbits. Ara podreu usar un «Mode d'ús baix de recursos» que carregarà les miniatures en baixa resolució (quan estigui disponible). Aquesta nova manera és molt més ràpida i més eficient en l'ús de recursos en carregar les miniatures per a les imatges JPEG i fitxers RAW. En els casos en què el Gwenview no pugui generar una miniatura per a una imatge, ara mostrarà una imatge de marcador de posició en lloc de reutilitzar la miniatura de la imatge anterior. També s'han resolt els problemes que va tenir el Gwenview amb la visualització de miniatures de les càmeres Sony i Canon.

A més dels canvis a l'apartat de miniatures, el Gwenview també ha implementat un nou menú «Comparteix», el qual permetrà enviar imatges a diversos llocs, carregar i mostrar correctament els fitxers a ubicacions remotes a les quals s'accedeix mitjançant KIO. La nova versió del Gwenview també mostra moltes més metadades EXIF per a les imatges RAW.

{{<figure src="/announcements/applications/19.08.0/gwenview_share.png" alt=`El nou menú «Comparteix» al Gwenview` caption=`El nou menú «Comparteix» al Gwenview` width="600px" >}}

### Okular

Els desenvolupadors han introduït moltes millores a les anotacions de l'Okular, el visualitzador de documents del KDE. A més de la interfície d'usuari millorada per als diàlegs de configuració de les anotacions, l'anotació de línia ara pot afegir diversos adorns visuals als extrems, el qual, per exemple, les permetrà convertir-se en fletxes. L'altra cosa que podreu fer amb les anotacions és expandir-les i contraure-les totes alhora.

El suport de l'Okular per als documents EPub també ha rebut un impuls en aquesta versió. L'Okular ja no es bloqueja en intentar carregar fitxers ePub amb un format incorrecte, i el seu rendiment amb els fitxers ePub grans ha millorat significativament. La llista de canvis en aquesta versió inclou vores de pàgina millorades i l'eina Marcador del mode Presentació en el mode PPP alt.

{{<figure src="/announcements/applications/19.08.0/okular_line_end.png" alt=`La configuració de l'eina d'anotacions de l'Okular amb la nova opció Final de línia` caption=`La configuració de l'eina d'anotacions de l'Okular amb la nova opció Final de línia` width="600px" >}}

### Kate

Gràcies als nostres desenvolupadors, s'han eliminat tres molestos errors en aquesta versió de l'editor de text avançat del KDE. El Kate, una vegada més, porta la finestra existent al davant quan se li demana que obri un document nou des d'una altra aplicació. La característica «Obertura ràpida» classificarà els elements segons l'ús més recent i preseleccionarà l'element superior. El tercer canvi es troba a la característica «Documents recents», la qual ara funciona quan la configuració actual està establerta a no desar la configuració de cada finestra individual.

### Konsole

El canvi més notable al Konsole, l'aplicació d'emulador de terminal del KDE, és l'impuls a la característica de mosaic. Ara podreu dividir el plafó principal com vulgueu, tant en vertical com en horitzontal. Els subplafons es podran dividir de nou com vulgueu. Aquesta versió també afegeix la capacitat d'arrossegar i deixar anar els plafons, de manera que podreu reorganitzar amb facilitat la disposició per adaptar-la al vostre flux de treball.

A més d'això, la finestra Configuració ha rebut una revisió perquè sigui més clara i fàcil d'emprar.

{{< video src-webm="/announcements/applications/19.08.0/konsole-tabs.webm" >}}

### Spectacle

L'Spectacle és l'aplicació de captura de pantalla del KDE i està obtenint característiques cada vegada més interessants amb cada nova versió. Aquesta versió no serà una excepció, ja que l'Spectacle ve amb diverses característiques noves que regulen la seva funcionalitat de retard. En prendre una captura de pantalla amb retard, l'Spectacle mostrarà el temps restant en el títol de la seva finestra. Aquesta informació també serà visible en el seu element Gestor de tasques.

Encara a la característica de Retard, el botó del Gestor de tasques de l'Spectacle també mostrarà una barra de progrés, de manera que pugueu realitzar un seguiment de quan es prendrà una captura. I, finalment, si minimitzeu l'Spectacle mentre espereu, veureu que el botó «Pren una captura de pantalla nova» es convertirà en un botó «Cancel·lar». Aquest botó també conté una barra de progrés, la qual us oferirà l'oportunitat d'aturar el compte enrere.

Desar captures de pantalla també té una nova funcionalitat. Quan hàgiu desat una captura de pantalla, l'Spectacle mostrarà un missatge a l'aplicació que us permetrà obrir la captura de pantalla o la carpeta que la conté.

{{< video src-webm="/announcements/applications/19.08.0/spectacle_progress.webm" >}}

### Kontact

El Kontact, la suit de programes de correu electrònic/calendari/contactes del KDE i el treball en grup en general dels programes, ofereix «emoji» en color amb Unicode i admet Markdown a l'editor de correu electrònic. La nova versió del KMail no només us permetrà fer que els vostres missatges es vegin bé, sinó que, gràcies a la integració amb els correctors gramaticals com LanguageTool i Grammalecte, us ajudarà a revisar i corregir el vostre text.

{{<figure src="/announcements/applications/19.08.0/kontact_emoji.png" alt=`Selector dels emojis` caption=`Selector dels emojis` width="600px" >}}

{{<figure src="/announcements/applications/19.08.0/kmail_grammar.png" alt=`La integració del comprovador gramatical al KMail` caption=`La integració del comprovador gramatical al KMail` width="600px" >}}

En planificar esdeveniments, els correus electrònics d'invitació en el KMail ja no s'eliminaran després de respondre'ls. Ara serà possible moure un esdeveniment des d'un calendari a un altre a l'editor d'esdeveniments al KOrganizer.

Finalment, però no menys important, el KAddressBook ara podrà enviar missatges SMS als contactes a través del KDE Connect, el qual resultarà en una integració més adequada entre el vostre escriptori i els dispositius mòbils.

### Kdenlive

La nova versió del Kdenlive, el programari d'edició de vídeo del KDE, té un nou conjunt de combinacions de teclat i ratolí que us ajudaran a ser més productiu. Per exemple, podreu canviar la velocitat d'un clip a la línia de temps prement la tecla Ctrl i arrossegant sobre el clip, o activar la vista prèvia de les miniatures dels videoclips mantenint premuda la tecla «Maj» i movent el ratolí sobre la miniatura d'un clip al contenidor del projecte. Els desenvolupadors també han posat un gran esforç en la usabilitat en fer que les operacions d'edició de 3 punts siguin coherents amb altres editors de vídeo, el qual segurament apreciareu si canvieu al Kdenlive des d'un altre editor.

{{<figure src="https://cdn.kde.org/screenshots/kdenlive/19-08.png" alt=`Kdenlive 19.08.0` caption=`Kdenlive 19.08.0` width="600px" >}}
