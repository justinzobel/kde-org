---
aliases:
- ../../kde-frameworks-5.10.0
date: '2015-05-08'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### KActivities

- (변경 기록 없음)

### KConfig

- kconfigcompiler를 사용하여 QML 안전 클래스 생성

### KCoreAddons

- 새로운 CMake 매크로 kcoreaddons_add_plugin을 추가하여 KPluginLoader를 사용하는 플러그인을 쉽게 만들 수 있습니다.

### KDeclarative

- 텍스처 캐시 충돌 수정.
- 및 기타 수정

### KGlobalAccel

- 전역 설정에서 단축키를 가져오는 새 globalShortcut 메서드를 추가했습니다.

### KIdleTime

- Wayland 플랫폼에서 KIdleTime 충돌 방지

### KIO

- KPropertiesDialog::KPropertiesDialog(urls) 및 KPropertiesDialog::showDialog(urls) 메서드를 추가했습니다.
- QIODevice-based data fetch for KIO::storedPut 및 KIO::AccessManager::put에서 QIODevice 기반 비동기 데이터 가져오기를 사용합니다.
- QFile::rename 반환값의 조건 수정(버그 343329)
- KIO::suggestName에서 더 적절한 이름을 제안하도록 수정(버그 341773)
- kioexec: kurl의 기록 가능한 경로 수정(버그 343329)
- user-places.xbel 파일에만 책갈피 저장(버그 345174)
- 다른 파일 두 개의 이름이 같을 때 RecentDocuments에 항목 두 개 두기
- 단일 파일이 휴지통에 들어가기에 크기가 너무 클 때 오류 메시지 개선(버그 332692)
- 슬롯에서 openURL을 호출할 때 전환이 발생했을 때 KDirLister 충돌 수정

### KNewStuff

- New set of classes, called KMoreTools and related. KMoreTools helps to add hints about external tools which are potentially not yet installed. Furthermore, it makes long menus shorter by providing a main and more section which is also user-configurable.

### KNotifications

- 우분투의 NotifyOSD와 사용했을 때 KNotifications 수정(버그 345973)
- 같은 속성을 설정했을 때 알림 업데이트를 하지 않음(버그 345973)
- LoopSound 플래그 추가, 필요한 경우 알림에서 소리를 반복 재생하도록 함(버그 346148)
- 알림에 위젯이 없어도 충돌하지 않도록 수정

### KPackage

- KPluginLoader::findPlugins 함수와 비슷하게 동작하는 KPackage::findPackages 함수 추가

### KPeople

- KService 대신 KPluginFactory로 플러그인 인스턴스 생성(호환성을 위해서 기존 방식은 유지됨).

### KService

- 항목 경로를 잘못 나누는 문제 수정(버그 344614)

### KWallet

- 이전 에이전트를 시작할 때 이전 지갑이 비어 있는지 여부 검사(버그 346498)

### KWidgetsAddons

- KDateTimeEdit: 사용자 입력이 실제로 등록되도록 수정. 여백이 두 번 추가되는 문제 수정.
- KFontRequester: 고정폭 글꼴만 선택할 때 문제 수정

### KWindowSystem

- KXUtils::createPixmapFromHandle에서 QX11Info에 의존하지 않도록 수정(버그 346496)
- 새 메서드 NETWinInfo::xcbConnection() -&gt; xcb_connection_t*

### KXmlGui

- 두 번째 단축키를 설정했을 때 단축키 문제 수정(버그 345411)
- 버그를 보고할 때 Bugzilla 제품/구성 요소 목록 업데이트(버그 346559)
- 전역 단축키: 대체 단축키 설정 허용

### NetworkManagerQt

- 설치된 헤더가 다른 프레임워크와 동일한 구조로 배치됩니다.

### Plasma 프레임워크

- PlasmaComponents.Menu에서 섹션을 지원함
- C++ 데이터 엔진을 불러올 때 ksycoca 대신 KPluginLoader 사용
- popupPosition에서 visualParent 회전 참조(버그 345787)

### Sonnet

- 맞춤법 검사기를 찾을 수 없을 때 강조하지 않습니다. rehighlightRequest 타이머가 계속 호출되어 무한 반복되는 문제를 해결합니다.

### 프레임워크 통합

- Fix native file dialogs from widgets QFileDialog: ** File dialogs opened with exec() and without parent were opened, but any user-interaction was blocked in a way that no file could be selected nor the dialog closed. ** File dialogs opened with open() or show() with parent were not opened at all.

<a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>Dot 기사</a>에서 릴리스에 대해 토론할 수 있습니다.
