---
aliases:
- ../announce-applications-18.12.1
changelog: true
date: 2019-01-10
description: KDE Ships Applications 18.12.1.
layout: application
major_version: '18.12'
title: KDE Ships KDE Applications 18.12.1
version: 18.12.1
---
{{% i18n_date %}}

Today KDE released the first stability update for <a href='../18.12.0'>KDE Applications 18.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Arredor de 20 correccións de erros inclúen melloras en, entre outros, Kontact, Cantor, Dolphin, JuK, Kdenlive, Konsole e Okular.

Entre as melloras están:

- Agora Akregator funciona con WebEngine a partir de Qt ≥ 5.11
- Corrixiuse o ordenamento das columnas no reprodutor de música JuK
- Konsole volve renderizar correctamente caracteres de debuxo de caixa
