---
aliases:
- ../announce-applications-16.04.0
changelog: true
date: 2016-04-20
description: KDE publica a versión 16.04.0 das aplicacións de KDE
layout: application
title: KDE publica a versión 16.04.0 das aplicacións de KDE
version: 16.04.0
---
20 de abril de 2016. KDE introduce hoxe a versión 16.04 das aplicacións de KDE con innumerábeis anovacións no que respecta a unha maior facilidade de acceso, a introdución de funcionalidades moi útiles e solucionar eses pequenos problemas que fan que as aplicacións de KDE estean un pouco máis preto de ofrecer a configuración perfecta para o seu dispositivo.

<a href='https://www.kde.org/applications/graphics/kcolorchooser/'>KColorChooser</a>, <a href='https://www.kde.org/applications/utilities/kfloppy/'>KFloppy</a>, <a href='https://www.kde.org/applications/games/kmahjongg/'>KMahjongg</a> and <a href='https://www.kde.org/applications/internet/krdc/'>KRDC</a> have now been ported to KDE Frameworks 5 and we look forward to your feedback and insight into the newest features introduced with this release. We would also highly encourage your support for <a href='https://www.kde.org/applications/education/minuet/'>Minuet</a> as we welcome it to our KDE Applications and your input on what more you'd like to see.

### Último engadido a KDE

{{<figure src="/announcements/applications/16.04.0/minuet1604.png" class="text-center" width="600px">}}

A new application has been added to the KDE Education suite. <a href='https://minuet.kde.org'>Minuet</a> is a Music Education Software featuring full MIDI support with tempo, pitch and volume control, which makes it suitable for both novice and experienced musicians.

Minuet inclúe 44 exercicios de adestramento de oído sobre escalas, acordes, intervalos e ritmos, permite visualizar contido musical no teclado do piano e permite unha integración transparente cos seus propios exercicios.

### Máis axuda para vostede

{{<figure src="/announcements/applications/16.04.0/khelpcenter1604.png" class="text-center" width="600px">}}

KHelpCenter, que adoitaba distribuírse xunto con Plasma, pasa a distribuírse como parte das aplicacións de KDE.

O equipo de KHelpCenter levou a cabo unha campaña masiva de revisión de fallos e limpeza que resultou na resolución de 49 fallos, moitos deles relacionados con mellorar e restaurar a funcionalidade de busca, que non funcionaba.

A busca de documento interna que usaba o software obsoleto ht::/dig substituíuse por un novo sistema de indexación e busca baseada en Xapian, o que leva a restaurar funcionalidades como buscar en páxinas de manual, en páxinas de información e documentación fornecida por software de KDE, engadindo a funcionalidade de marcadores para as páxinas de documentación.

Ao retirar a compatibilidade con KDE Libs 4, limpar código e solucionar algúns outros pequenos fallos, o mantemento do código mellorouse de maneira significativa para ofrecerlle un KHelpCenter completamente renovado.

### Exterminación de erros

A colección de Kontact recibiu 55 correccións de fallos; algunhas delas estaban relacionadas con problemas ao configurar alarmas, e na importación de correo de Thunderbird, reducindo o tamaño de iconas de Skype e Google Talk na vista de panel de contactos, apaños relacionados con KMail como a importación de cartafoles, importación de vCard, apertura de anexos de correo de ODF, insercións de URL de Chromium, as diferenzas dos menús de ferramentas coa aplicación iniciada como parte de Kontact ou de maneira independente, a falta do elemento de menú de «Enviar» e algúns outros. Engadíronse as fontes RSS de portugués do Brasil, e corrixíronse os enderezos das fontes de húngaro e castelán.

Entre as novas funcionalidades hai un cambio do deseño do editor de contactos de KAddressbook, un novo tema predeterminado de cabeceira de KMail, melloras no exportador da configuración e unha corrección da compatibilidade coas iconas de sitios web de Akregator. O editor de mensaxes de KMail limpouse ao tempo que se engadía un novo tema predeterminado de cabeceira de KMail co tema de Grantlee que se usa para a páxina de «Sobre» de KMail, Kontact e Akregator. Agora Akregator usa QtWebKit, un dos principais motores para renderizar páxinas web e executar código JavaScript como motor web de renderizado e xa comezou o proceso para permitir QtWebEngine en Akregator e outras aplicacións da colección Kontact. Ao tempo que moitas funcionalidades pasaron a fornecerse como complementos en «kdepim-addons», as bibliotecas de xestión de información persoal dividíronse en varios paquetes.

### Arquivar en profundidade

{{<figure src="/announcements/applications/16.04.0/ark1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, the archive manager, has had two major bugs fixed so that now Ark warns the user if an extraction fails due to lack of sufficient space in the destination folder and it does not fill up the RAM during the extraction of huge files via drag and drop.

Agora Ark tamén inclúe un diálogo de propiedades que mostra información como o tipo de arquivo, o tamaño comprimido e descomprimido, os resumos criptográficos de MD5, SHA-1 e SHA-256 sobre o arquivo aberto actualmente.

Agora Ark tamén pode abrir e extraer arquivos RAR sen utilizar as ferramentas non libres de RAR e pode abrir, extraer e crear arquivos TAR comprimidos cos formatos lzop, lzip e lrzip.

Puliuse a interface de usuario de Ark reorganizando a barra de menú e a barra de ferramentas para mellorar a facilidade de uso e para retirar ambigüidades, así como para aforrar espazo vertical grazas á barra de estado, que agora está agochada de maneira predeterminada.

### Máis precisión nas edicións de vídeo

{{<figure src="/announcements/applications/16.04.0/kdenlive1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a>, the non-linear video editor has numerous new features implemented. The titler now has a grid feature, gradients, the addition of text shadow and adjusting letter/line spacing.

Pantalla integrada de niveis de son permite monitorizar facilmente o son no proxecto xunto coas novas capas do monitor de vídeo que mostra a taxa de fotogramas da reprodución, as zonas seguras e as ondas de son e unha barra de ferramentas para ir a marcadores e un monitor de ampliación.

Tamén hai unha nova funcionalidade de biblioteca que permite copiar e pegar secuencias entre proxectos e unha vista dividida na liña de tempo para comparar e visualizar os efectos aplicados ao fragmento co fragmento sen eles.

O diálogo do renderizador escribiuse de novo engadindo unha opción para conseguir unha codificación máis rápida, producindo polo tanto ficheiros grandes, e o efecto de velocidade modificouse para que tamén funcione co son.

Introducíronse curvas nos fotogramas clave para algúns efectos e agora pode acceder rapidamente aos seus efectos máis habituais mediante o trebello de efectos favoritos. Ao usar a ferramenta de coitela, agora a liña vertical da liña de tempo mostrará o fotograma exacto no que se realizará o corte e os xeradores de fragmentos que se acaban de engadir permitiranlle crear fragmentos e contadores de barras de cores. Ademais disto, introduciuse de novo o número de usos de fragmentos no lixo do proxecto e as miniaturas de son reescribíronse para que sexan moito máis rápidas.

As correccións de fallos principais afectan a cousas como a quebra ao usar títulos (que require a última versión de MLT), corrixir corrupcións que se producen cando a taxa de fotogramas por segundo do proxecto é distinta de 25, as quebras ao eliminar pistas, o problemático modo de sobrescribir na liña de tempo e as corrupcións e os efectos perdidos ao usar unha configuración rexional con coma como separador. Ademais destas correccións, o equipo estivo traballando de maneira constante en facer grandes melloras de estabilidade, fluxo de traballo e pequenas melloras de facilidade de uso.

### E máis!

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, the document viewer brings in new features in the form of customizable inline annotation width border, allowance of directly opening embedded files instead of only saving them and also the display of a table of content markers when the child links are collapsed.

<a href='https://www.kde.org/applications/utilities/kleopatra'>Kleopatra</a> introduced improved support for GnuPG 2.1 with a fixing of selftest errors and the cumbersome key refreshes caused by the new GnuPG (GNU Privacy Guard) directory layout. ECC Key generation has been added with the display of Curve details for ECC certificates now. Kleopatra is now released as a separate package and support for .pfx and .crt files has been included. To resolve the difference in the behaviour of imported secret keys and generated keys, Kleopatra now allows you to set the owner trust to ultimate for imported secret keys. 
