---
aliases:
- ../../kde-frameworks-5.20.0
date: 2016-03-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Iconas de Breeze

- Moitas iconas novas
- Engadir iconas de tipo MIME de VirtualBox e outros tipos MIME que faltaban
- Engadir iconas para Synaptic e Octopi
- Corrixir a icona de cortar (fallo 354061)
- Corrixir o nome de audio-headphones.svg (+=d)
- Iconas de puntuación cunha marxe máis pequena (1px)

### Integración de infraestruturas

- Retirar posíbeis nomes de ficheiro de KDEPlatformFileDialog::setDirectory()
- Non filtrar por nome se tempos tipos MIME

### KActivities

- Retirar a dependencia de Qt5::Widgets
- Retirar a dependencia de KDBusAddons
- Retirar a dependencia de KI18n
- Retirar inclusións sen usar
- Mellorouse a saída dos scripts de intérprete de ordes
- Engadiuse o modelo de datos (ActivitiesModel) que mostra as actividades á biblioteca
- Construír unicamente a biblioteca de maneira predeterminada
- Retirar os compoñentes de servizo e espazo de traballo da construción
- Mover a biblioteca de src/lib/core a src/lib
- Corrixir un aviso de CMake
- Corrixir unha quebra no menú contextual das actividades (fallo 351485)

### KAuth

- Corrixir un bloqueo indefinido de kded5 cando se sae dun programa que usa KAuth

### KConfig

- KConfigIniBackend: Corrixir unha separación na busca que require moitos recursos

### KCoreAddons

- Corrixir a migración da configuración de Kdelibs4 en Windows
- Engadir unha API para obter a información de versión das infraestruturas en tempo de execución
- KRandom: Non usar ata 16 K de /dev/urandom como semente para rand() (fallo 359485)

### KDeclarative

- Non chamar a un punteiro nulo de obxecto (fallo 347962)

### KDED

- Permitir compilar como -DQT_NO_CAST_FROM_ASCII

### Compatibilidade coa versión 4 de KDELibs

- Corrixir a xestión de sesións en aplicacións baseadas en KApplication (fallo 354724)

### KDocTools

- Usar caracteres Unicode nas caixas de mensaxe

### KFileMetaData

- Agora KFileMetadata pode consultar e almacenar información sobre a mensaxe de correo electrónico orixinal da que un ficheiro gardado puido ser un anexo

### KHTML

- Corrixir a actualización de cursor na vista
- Limitar o uso de memoria das cadeas
- Visor de miniaplicativos de Java de KHTML: reparar unha chamada rota de D-Bus a kpasswdserver

### KI18n

- Usar un macro de nl_msg_cat_cntr compatíbel con varias plataformas
- Saltar a busca de . e .. para atopar as traducións dunha aplicación
- Restrinxir o uso de _nl_msg_cat_cntr a código de GNU gettext
- Engadir KLocalizedString::languages()
- Colocar as chamadas de Gettext só se se atopou o catálogo

### KIconThemes

- Asegurarse de que a variábel se preparar

### KInit

- kdeinit: Preferir cargar bibliotecas de RUNPATH
- Completar «Tarefa de Qt5: usar QUrl::fromStringList»

### KIO

- Corrixir que a conexión entre aplicación e escravo de KIO rompa se appName contén un «/» (fallo 357499)
- Intentar varios métodos de autenticación en caso de fallos
- axuda: corrixir mimeType() en get()
- KOpenWithDialog: mostrar o nome e o comentario de tipo MIME no texto da caixa para marcar de «Lembrar» (fallo 110146)
- Unha serie de cambios para evitar unha nova lista de directorios tras un cambio de nome de ficheiro en máis casos (fallo 359596)
- http: cambiar o nome de m_iError a m_kioError
- kio_http: ler e descartar o corpo tras un 404 con errorPage=false
- kio_http: corrixir a determinación de tipo MIME cando o URL remata en «/»
- FavIconRequestJob: engadir o accesor hostUrl() para que Konqueror poida descubrir para que era o traballo desde a rañura
- FavIconRequestJob: corrixir que os traballos se queden colgados ao interromperse por ter un favicon demasiado grande
- FavIconRequestJob: corrixir errorString(), só tiña o URL
- KIO::RenameDialog: restaurar a funcionalidade de vista previa, engadir etiquetas de data e tamaño (fallo 356278)
- KIO::RenameDialog: facer cambios internos en código duplicado
- Corrixir conversións incorrectas de ruta a QUrl
- Usar kf5.kio no nome de categoría para que coincida con outras categorías

### KItemModels

- KLinkItemSelectionModel: Engadir un novo construtor predeterminado
- KLinkItemSelectionModel: permitir definir o modelo da selección ligada
- KLinkItemSelectionModel: xestionar cambios do modelo selectionModel
- KLinkItemSelectionModel: non almacenar o modelo localmente
- KSelectionProxyModel: Corrixir un fallo de iteración
- Restabelecer o estado de KSelectionProxyModel cando se necesite
- Engadir unha propiedade que indique se os modelos forman unha cadea conectada
- KModelIndexProxyMapper: simplificar a lóxica da comprobación de conectado

### KJS

- Limitar o uso de memoria das cadeas

### KNewStuff

- Mostrar un aviso se hai un erro en Engine

### Infraestrutura de paquetes

- Permitir que KDocTools siga sendo opcional en KPackage

### KPeople

- Corrixir un uso obsoleto da API
- Engadir actionType ao complemento declarativo
- Inverter a lóxica de filtrado en PersonsSortFilterProxyModel
- Facer algo máis útil o exemplo de QML
- Engadir actionType a PersonActionsModel

### KService

- Simplificar o código, reducir as resolucións de punteiros, melloras relacionadas cos contedores
- Engadir o programa de probas kmimeassociations_dumper, inspirado no fallo 359850
- Corrixir que as aplicacións Chromium e Wine non cargasen nalgunhas distribucións (fallo 213972)

### KTextEditor

- Corrixir o salientado de todas as aparicións en ReadOnlyPart
- Non iterar por unha QString como se fose unha QStringList
- Preparar obxectos QMap estáticos correctamente
- Preferir toDisplayString(QUrl::PreferLocalFile)
- Permitir o envío de caracteres substitutos desde o método de entrada
- Non quebrar ao apagar cando a animación de texto continúa

### Infraestrutura de KWallet

- Asegurarse de que se busca KDocTools
- Non pasar un número negativo a D-Bus, asevérase en libdbus
- Limpar os ficheiros de CMake
- KWallet::openWallet(Synchronous): non usar 25 segundos como tempo máximo

### KWindowSystem

- permitir _NET_WM_BYPASS_COMPOSITOR (fallo 349910)

### KXMLGUI

- Usar o nome de idioma non nativo como reserva
- Corrixir a xestión de sesións que estaba rota desde KF5 e Qt5 (fallo 354724)
- Esquemas de atallos: permitir esquemas instalados globalmente
- Usar qHash(QKeySequence) de Qt ao construír con Qt ≥ 5.6
- Esquemas de atallos: corrixir un fallo polo que dous KXMLGUIClient co mesmo nome sobrescríbense o ficheiro de esquema do outro
- kxmlguiwindowtest: engadir o diálogo de atallos para probar o editor de esquemas de atallos
- Esquemas de atallos: mellorar a facilidade de uso cambiando os textos na interface gráfica de usuario
- Esquemas de atallos: mellorar o selector da lista de esquemas (tamaño automático, non baleirar en caso de esquema descoñecido)
- Esquemas de atallos: non prefixar o nome de ficheiro co nome do cliente de interface gráfica de usuario
- Esquemas de atallos: crear o directorio ates de intentar crear un novo esquema de atallos
- Esquemas de atallos: restaurar a marxe da disposición, senón ten un aspecto moi barulleiro
- Corrixir unha fuga de memoria no gancho de preparación de KXmlGui

### Infraestrutura de Plasma

- IconItem: non sobrescribir a fonte ao usar QIcon::name()
- ContainmentInterface: Corrixir o uso de right() e bottom() de QRect
- Retirar unha ruta de código esencialmente duplicada para xestionar QPixmaps
- Engadir documentación da API para IconItem
- Corrixir a folla de estilos (fallo 359345)
- Non borrar a máscara de xanela en cada cambio de xeometría cando os efectos de escritorio están activados e non se definiu ningunha máscara
- Applet: non quebrar ao retirar o panel (fallo 345723)
- Tema: Descartar a caché de mapas de píxeles ao cambiar de tema (fallo 359924)
- IconItemTest: Saltar cando grabToImage falla
- IconItem: Corrixir o cambio de cor das iconas SVG que se cargan dun tema de iconas
- Corrixir a resolución de iconPath de SVG en IconItem
- Se se pasa a ruta, coller a cola (fallo 359902)
- Engadir as propiedades configurationRequired e reason
- Mover contextualActionsAboutToShow a Applet
- ScrollViewStyle: non usar as marxes do elemento escintilante
- DataContainer: Corrixir as comprobacións de rañura antes de connect e disconnect
- ToolTip: evitar que se produzan varios cambios de xeometría mentres se cambia o contido
- SvgItem: Non usar Plasma::Theme desde o fío de renderización
- AppletQuickItem: Corrixir atopar a disposición anexada propia (fallo 358849)
- Expansor máis pequeno para a barra de tarefas
- Consello: deixar de mostrar o temporizador se se chama a hideTooltip (fallo 358894)
- Desactivar a animacións de iconas nos consellos de Plasma
- Retirar as animacións dos consellos
- O tema predeterminado respecta o esquema de cores
- Corrixir que IconItem non cargue iconas non de tema con nome (fallo 359388)
- Preferir contedores distintos de escritorios en containmentAt()
- WindowThumbnail: Descartar o mapa de píxeles de glx en stopRedirecting() (fallo 357895)
- Retirar o filtro vello de miniaplicativos
- ToolButtonStyle: Non depender dun identificador externo
- Non asumir que atopamos unha coroa (fallo 359026)
- Calendario: engadir botóns axeitados de atrás e adiante e un botón de «Hoxe» (fallos 336124, 348362, 358536)

### Sonnet

- Non desactivar a detección de idioma simplemente porque se definise un idioma
- Desactivar de maneira predeterminada a desactivación automática da corrección ortográfica automática
- Corrixir TextBreaks
- Corrixir que ás rutas de busca de dicionario de Hunspell lles faltase «/» (fallo 359866)
- Engadir &lt;directorio da aplicación&gt;/../share/hunspell á ruta de busca de dicionarios

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
