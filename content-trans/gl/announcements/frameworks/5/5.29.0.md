---
aliases:
- ../../kde-frameworks-5.29.0
date: 2016-12-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nova infraestrutura

This release includes Prison, a new framework for barcode generation (including QR codes).

### Xeral

FreeBSD was added to metainfo.yaml in all frameworks tested to work on FreeBSD.

### Baloo

- Melloras de rendemento ao escribir (×4 de velocidade ao escribir datos de saída)

### Iconas de Breeze

- Activar BINARY_ICONS_RESOURCE de maneira predeterminada
- engadir o MIME vnd.rar para shared-mime-info 1.7 (fallo 372461)
- engadir icona de claws (fallo 371914)
- engadir unha icona de gdrive en vez de unha icona xenérica de nube (fallo 372111)
- corrixir o fallo «list-remove-symbolic usa a imaxe incorrecta» (fallo 372119)
- outros engadidos e melloras

### Módulos adicionais de CMake

- Saltar a proba de API de Python se PyQt non está instalado
- Só engadir a proba se se atopa Python
- Reducir a versión mínima necesaria de CMake
- Engadir o módulo ecm_win_resolve_symlinks

### Integración de infraestruturas

- atopar QDBus, requirido polo xestor de KPackage de AppStream
- Permitir a KPackage depender de packagekit e appstream

### KActivitiesStats

- Enviar correctamente o evento ligado con recursos

### Ferramentas de Doxygen de KDE

- Adaptar ao cambio de quickgit a cgit
- Corrixir un fallo se o nome do grupo non está definido. Aínda pode romper se se dan malas condicións.

### KArchive

- Engadir o método errorString() para fornecer información de erro

### KAuth

- Engadir a propiedade «timeout» (fallo 363200)

### KConfig

- kconfig_compiler - xerar código con sobreposicións
- Analizar correctamente palabras clave de funcións (fallo 371562)

### KConfigWidgets

- Asegurarse de que as accións de menú obteñen o MenuRole correspondente

### KCoreAddons

- KTextToHtml: corrixir o fallo «[1] engádese ao final das ligazóns» (fallo 343275)
- KUser: só buscar un avatar se loginName non está baleiro

### KCrash

- Aliñarse con KInit e non usar DISPLAY en Mac
- Non pechar todos os descritores de ficheiro en OS X

### KDesignerPlugin

- src/kgendesignerplugin.cpp - engadir sobreposicións ao código xerado

### KDESU

- Retira a definición de XDG_RUNTIME_DIR en procesos executados con kdesu

### KFileMetaData

- Atopar de verdade o libpostproc de FFMpeg

### KHTML

- java: aplicar os nomes aos botóns correctos
- java: definir nomes no diálogo de permisos

### KI18n

- Comprobar correctamente a desigualdade de punteiros desde dngettext (fallo 372681)

### KIconThemes

- Permitir mostrar iconas de todas as categorías (fallo 216653)

### KInit

- Definir as variábeis de contorno desde KLaunchRequest ao iniciar un novo proceso

### KIO

- Migrouse a rexistros con categoría
- Corrixir a compilación co SDK de Windows XP
- Permitir sumas de comprobación en maiúsculas que coincidan co separador de sumas de comprobación (fallo 372518)
- Non estirar nunca a última columna (=data) no diálogo de ficheiro (fallo 312747)
- Importar e actualizar os docbook de KControl sobre o código de KIO da rama mestra de kde-runtime
- [OS X] facer que o lixo de KDE use o de OS X
- SlaveBase: engadir documentación sobre os bucles de eventos, as notificacións e os módulos de kded

### KNewStuff

- Engadir unha nova opción de xestión de arquivos (subdirectorio) a knsrc
- Consumir os novos sinais de erro (definir erros de traballo)
- Xestionar unha estrañeza sobre ficheiros que desaparecían xusto despois de crearse
- Instalar de verdade as cabeceiras principais, con MaiúsculasIniciais

### KNotification

- [KStatusNotifierItem] Gardar ou restaurar a posición do trebello durante a xanela de agochar ou restaurar (fallo 356523)
- [KNotification] Permitir anotar notificacións con URL

### Infraestrutura KPackage

- seguir instalando metadata.desktop (fallo 372594)
- cargar os metadatos manuales se se pasa unha ruta absoluta
- Corrixir un fallo potencial se un paquete non é compatíbel con appstream
- Informar a KPackage de X-Plasma-RootPath
- Corrixir a xeración do ficheiro metadata.json

### KPty

- Máis busca de ruta de utempter (incluíndo /usr/lib/utempter/)
- Engadir a ruta da biblioteca de xeito que se atope o binario de utempter en Ubuntu 16.10

### KTextEditor

- Evitar avisos de Qt sobre o modo de portapapeis non permitido en Mac
- Usar as definicións de sintaxe de KF5::SyntaxHighlighting

### KTextWidgets

- Non substituír as iconas de xanela co resultado dunha busca fallada

### KWayland

- [cliente] Corrixir unha resolución de punteiro nulo en ConfinedPointer e LockedPointer
- [client] Instalar pointerconstraints.h
- [servidor] Corrixir unha regresión en SeatInterface::end e cancelPointerPinchGesture
- Compatibilidade co protocolo PointerConstraints
- [servidor] Reducir os recursos que pointersForSurface necesita
- Devolver SurfaceInterface::size no espazo do compositor global
- [tools/generator] Xerar a enumeración FooInterfaceVersion do lado do servidor
- [tools/generator] Cubrir os argumentos das solicitudes de wl_fixed con wl_fixed_from_double
- [tools/generator] Xerar código de solicitudes do lado do cliente
- [tools/generator] Xerar fábricas de recursos do lado do cliente
- [tools/generator] Xerar retrochamadas e un oínte no lado do cliente
- [tools/generator] Pasar isto como punteiro q a Client::Resource::Private
- [tools/generator] Xerar Private::setup(wl_foo *arg) do lado do cliente
- Compatibilidade co protocolo PointerGestures

### KWidgetsAddons

- Evitar quebras en Mac
- Non substituír iconas co resultado dunha busca fallada
- KMessageWidget: corrixir a disposición cando se activa wordWrap sen accións
- KCollapsibleGroupBox: non agochar os trebellos, sobrepoñer a política de foco

### KWindowSystem

- [KWindowInfo] Engadir pid() e desktopFileName()

### Iconas de Oxygen

- Engadir unha icona de application-vnd.rar (fallo 372461)

### Infraestrutura de Plasma

- Comprobar a validez dos metadatos en settingsFileChanged (fallo 372651)
- Non xirar a disposición da barra de lapelas en vertical
- Retirar radialGradient4857 (fallo 372383)
- [AppletInterface] Non sacar nunca o foco de fullRepresentation (fallo 372476)
- Corrixir o prefixo do identificador dunha icona SVG (fallo 369622)

### Solid

- winutils_p.h: Restaurar a compatibilidade co SDK de Windows XP

### Sonnet

- Buscar tamén hunspell-1.5

### Realce da sintaxe

- Normalizar os valores dos atributos de licenza de XML
- Sincronizar as definicións de sintaxe de ktexteditor
- Corrixir a fusión da rexión de pregado

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
