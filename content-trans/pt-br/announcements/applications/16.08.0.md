---
aliases:
- ../announce-applications-16.08.0
changelog: true
date: 2016-08-18
description: O KDE disponibiliza o KDE Applications 16.08.0
layout: application
title: O KDE disponibiliza o KDE Applications 16.08.0
version: 16.08.0
---
18 de agosto de 2016. Hoje o KDE lançou o KDE Applications 16.08, com uma lista impressionante variedade de atualizações relacionadas a uma melhor facilidade de acesso. A introdução de funcionalidades realmente úteis, bem como a eliminação de alguns problemas menores, faz com que o KDE Applications fique um passo mais próximo de lhe oferecer a configuração perfeita para o seu dispositivo.

O <a href='https://www.kde.org/applications/graphics/kolourpaint/'>Kolourpaint</a>, o <a href='https://www.kde.org/applications/development/cervisia/'>Cervisia</a> e o KDiskFree foram agora migrados para as Plataformas do KDE 5 e estamos à espera da sua reacção e opiniões sobre as funcionalidades mais recentes que foram introduzidas com esta versão.

Diante do esforço contínuo em separar as bibliotecas da suíte Kontact para torná-las mais fáceis de serem usadas em outros programas, o tarball do kdepimlibs foi dividido em akonadi-contacts, akonadi-mime e akonadi-notes.

Nós descontinuamos os seguintes pacotes: kdegraphics-strigi-analyzer, kdenetwork-strigi-analyzers, kdesdk-strigi-analyzers, libkdeedu and mplayerthumbs. Isso irá nos ajudar a focar no resto do código.

### Mantendo-se em Kontato

O <a href='https://userbase.kde.org/Kontact'>Pacote Kontact</a> teve a sua ronda normal de limpezas, correcções de erros e optimizações nesta versão. De notar o uso do QtWebEngine em diversos componentes, o que permite o uso de um motor Web mais moderno. Também foi aperfeiçoado o suporte para VCard4, assim como a adição de novos 'plugins' de aviso se algumas condições são cumpridas ao enviar um e-mail, p.ex. verificar se deseja permitir o envio de e-mails com uma determinada identidade, verificar se está a enviar o e-mail como texto simples, etc.

### Nova versão do Marble

O <a href='https://marble.kde.org/'>Marble</a> 2.0 faz parte das Aplicações do KDE 16.08 e inclui mais de 450 alterações de código que incluem a navegação, a visualização e uma visualização vectorial experimental dos dados do OpenStreetMap.

### Mais Arquivamento

O <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> consegue agora extrair ficheiros de pacotes AppImage e .xar, assim como testar a integridade dos pacotes ZIP, 7z e RAR. Também consegue adicionar/editar os comentários nos pacotes RAR

### Melhorias no terminal

O <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a> recebeu algumas melhorias nas opções de desenho dos tipos de letra e teve um suporte de acessibilidade melhorado.

### E mais!

O <a href='https://kate-editor.org'>Kate</a> recebeu páginas com separadores amovíveis. <a href='https://kate-editor.org/2016/06/15/kates-tabbar-gets-movable-tabs/'>Mais informações...</a>.

O <a href='https://www.kde.org/applications/education/kgeography/'>KGeography</a> adicionou os mapas das Províncias e Regiões do Burkina Faso.

### Controle agressivo de problemas

Mais de 120 bugs foram resolvidos nos aplicativos KDE, incluindo a suíte Kontact, Ark, Cantor, Dolphin, KCalc, Kdenlive e mais!

### Registro completo das alterações
