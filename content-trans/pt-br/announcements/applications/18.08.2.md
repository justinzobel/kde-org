---
aliases:
- ../announce-applications-18.08.2
changelog: true
date: 2018-10-11
description: O KDE disponibiliza o KDE Applications 18.08.2
layout: application
title: O KDE disponibiliza o KDE Applications 18.08.2
version: 18.08.2
---
11 de Outubro de 2018. Hoje o KDE lançou a segunda actualização de estabilidade para as <a href='../18.08.0'>Aplicações do KDE 18.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Mais de uma dúzia de correções de erros registradas incluem melhorias no Kontact, Dolphin, Gwenview, KCalc, Umbrello, entre outros.

As melhorias incluem:

- Se arrastar um ficheiro no Dolphin já não activa por omissão a mudança de nome incorporada
- O KCalc permite de novo as teclas 'ponto' e 'vírgula' ao introduzir casas decimais
- Foi corrigido um defeito visual no baralho de cartas Paris nos jogos de cartas do KDE
