---
aliases:
- ../../kde-frameworks-5.62.0
date: 2019-09-14
layout: framework
libCount: 70
---
### Attica

- Correcção do ficheiro 'pkgconfig' do Attica

### Baloo

- Corrige um estoiro no Peruse originado pelo Baloo

### Ícones Breeze

- Adição de novos ícones de actividades e ecrãs virtuais
- Tornar os ícones pequenos dos documentos recentes mais parecidos com documentos e melhorias nos emblemas de relógios
- Criação de um novo ícone para as "Pastas recentes" (erro 411635)
- Adição do ícone 'preferences-desktop-navigation' (erro 402910)
- Adição de versão a 22px do 'dialog-scripts', mudança dos ícones de acções/locais dos programas para corresponder ao mesmo
- Melhoria do ícone "user-trash"
- Uso de estilo vazio/preenchido para o ícone monocromático de vazio/cheio do caixote do lixo
- Mudança dos ícones de notificações para usar o estilo de contorno
- Mudança dos ícones 'user-trash' para se parecerem com caixotes do lixo (erro 399613)
- Adição de ícones do Brisa para os ficheiros 'cern' ROOT
- Remoção do 'applets/22/computer' (erro 410854)
- Adição de ícones 'view-barcode-qr'
- O Krita separou-se do Calligra e agora usa o nome Krita em vez de 'calligrakrita' (erro 411163)
- Adição de ícones 'battery-ups' (erro 411051)
- Mudança do "monitor" para uma ligação ao ícone "computer"
- Adição de ícones do FictionBook 2
- Adição do ícone do 'kuiviewer', necessita de actualização -&gt; erro 407527
- Ligação simbólica do "port" para o "anchor", que representa uma iconografia mais apropriada
- Mudança do rádio para um ícone de dispositivo; adição de mais tamanhos
- Mudança do ícone <code>pin</code> para algo que se pareça mais com um pino, e não algo que não seja relacionado
- Correcção de dígito em falta e alinhamento ao pixel dos ícones de acções de profundidade (erro 406502)
- Mudança no 'folder-activities' a 16px para ficar parecido com os tamanhos maiores
- Adição do ícone do 'latte-dock' do repositório do mesmo em kde.org/applications
- remodelação do ícone do 'kdesrc-build', usado pelo kde.org/applications
- Mudança de nome do 'media-show-active-track-amarok' para 'media-track-show-active'

### Módulos extra do CMake

- ECMAddQtDesignerPlugin: passagem do exemplo de código indirectamente como argumento do nome da variável
- Manutenção da 'lib' como LIBDIR predefinida nos sistemas baseados em Arch Linux
- Activação do 'autorcc' por omissão
- Definição da localização de instalação dos ficheiros JAR/AAR no Android
- Adição do ECMAddQtDesignerPlugin

### KActivitiesStats

- Adição do Term::Type::files() e Term::Type::directories() para filtrar apenas as pastas ou para as excluir
- Adição do @since 5.62 para os novos métodos de modificação
- Adição de registos de actividade adequados usando a ECMQtDeclareLoggingCategory
- Adição de método de modificação para os campos de pesquisa Type, Activity, Agent e UrlFilter
- Uso das constantes de valores especiais no terms.cpp
- Possibilidade da filtragem por intervalos de datas dos eventos do recurso, usando o termo Date

### KActivities

- [kactivities] Uso de novo ícone de actividades

### KArchive

- Correcção da criação de pacotes em URL's content: do Android

### KCompletion

- Adição de opção para compilar um 'plugin' do Qt Designer (BUILD_DESIGNERPLUGIN, por omissão ON)

### KConfig

- Correcção de fuga de memória no KConfigWatcher
- Desactivação do KCONFIG_USE_DBUS no Android

### KConfigWidgets

- Adição de opção para compilar um 'plugin' do Qt Designer (BUILD_DESIGNERPLUGIN, por omissão ON)
- [KColorSchemeManager] Optimização da geração de antevisões

### KCoreAddons

- O KProcessInfo::name() agora devolve apenas o nome do executável. Para a linha de comandos completa, use o KProcessInfo::command()

### KCrash

- Evitar a activação do KCrash se só for incluído através de um 'plugin' (erro 401637)
- Desactivação do KCrash se estiver a executar no 'rr'

### KDBusAddons

- Correcção de problema de sincronização nos arranques automáticos do KCrash

### KDeclarative

- Aviso se o KPackage for inválido
- [GridDelegate] Não seleccionar um item não-seleccionado se carregar nalgum dos seus botões de acção (erro 404536)
- [ColorButton] Propagação do sinal 'accepted' do ColorDialog
- Uso de um sistema de coordenadas baseadas em zero no gráfico

### KDesignerPlugin

- Descontinuação do 'kgendesignerplugin', eliminação do 'plugin' 'bundle' de todos os elementos gráficos do KF5

### WebKit do KDE

- Uso do ECMAddQtDesignerPlugin em vez de uma cópia privada

### KDocTools

- KF5DocToolsMacros.cmake: Uso das variáveis não-obsoletas KDEInstallDirs (erro 410998)

### KFileMetaData

- implementação da gravação das imagens

### KHolidays

- Apresentação do nome do ficheiro onde é devolvido um erro

### KI18n

- Localização dos textos de números longos (erro 409077)
- Suporte da passagem do alvo à macro 'ki18n_wrap_ui'

### KIconThemes

- Adição de opção para compilar um 'plugin' do Qt Designer (BUILD_DESIGNERPLUGIN, por omissão ON)

### KIO

- A anulação do envio para o lixo de ficheiros do ecrã foi corrigida (erro 391606)
- kio_trash: divisão do 'copyOrMove' para um melhor erro que não seja "nunca deveria acontecer"
- FileUndoManager: validação mais simples quando se esquece de gravar
- Correcção da saída e estoiro no 'kio_file' quando o put() falha no 'readData'
- [CopyJob] Correcção de estoiro ao copiar uma pasta já existente e depois carregar em "Ignorar" (erro 408350)
- [KUrlNavigator] Adição dos tipos MIME suportados pelo 'krarc' no isCompressedPath (erro 386448)
- Adição de uma janela para atribuir a permissão de execução a um ficheiro executável quando o tentar executar
- [KPropertiesDialog] Verificação sempre se o ponto de montagem é nulo (erro 411517)
- [KRun] Verificação do tipo MIME no 'isExecutableFile' em primeiro lugar
- Adição de um ícone para o topo do lixo, bem como uma legenda adequada (erro 392882)
- Adição do suporte para o tratamento dos erros de SSL do QNAM no KSslErrorUiData
- Mudança para um comportamento mais consistente do FileJob
- [KFilePlacesView] Uso da chamada assíncrona KIO::FileSystemFreeSpaceJob
- mudança de nome do executável utilitário 'kioslave' interno para o 'kioslave5' (erro 386859)
- [KDirOperator] Reticências intermédias dos textos que sejam demasiado grandes para caber (erro 404955)
- [KDirOperator] Adição de opções para seguir as pastas novas
- KDirOperator: Só activar o menu "Criar um Novo" se o item seleccionado for uma pasta
- KIO FTP: Correcção da cópia de ficheiros que bloqueava quando se copiava para um ficheiro existente (erro 409954)
- KIO: migração para a chamada não-obsoleta KWindowSystem::setMainWindow
- Mudança para nomes de favoritos de ficheiros mais consistentes
- Adição de opção para compilar um 'plugin' do Qt Designer (BUILD_DESIGNERPLUGIN, por omissão ON)
- [KDirOperator] Uso de descrições do sentido de ordenação mais legíveis
- [Editor de permissões] Migração dos ícones para usar o QIcon::fromTheme() (erro 407662)

### Kirigami

- Substituição do botão de ultrapassagem personalizada com o PrivateActionToolButton no ActionToolBar
- Se uma acção de um submenu tiver um ícone atribuído, certificar-se que também é apresentado
- [Separador] Correspondência com a cor dos contornos do Brisa
- Adição do componente ListSectionHeader do Kirigami
- Correcção do botão do menu de contexto para as páginas que não aparecem
- Correcção do PrivateActionToolButton com um menu que não limpava o estado assinalado adequadamente
- possibilidade de definir um ícone personalizado para a pega esquerda da área
- Remodelação da lógica das 'visibleActions' no SwipeListItem
- Possibilidade de usar as acções do QQC2 nos componentes do Kirigami e mudança do K.Action para se basear no QQC2.Action
- Kirigami.Icon: Correcção do carregamento de imagens maiores quando a origem é um URL (erro 400312)
- Adição do ícone usado pelo Kirigami.AboutPage

### KItemModels

- Adição da interface Q_PROPERTIES ao KDescendantsProxyModel
- Migração de chamadas antigas no Qt

### KItemViews

- Adição de opção para compilar um 'plugin' do Qt Designer (BUILD_DESIGNERPLUGIN, por omissão ON)

### KNotification

- Impedimento de notificações duplicadas aparecerem no Windows e remoção de espaços em branco
- Uso de um ícone de aplicação 1024x1024 como ícone de contingência no Snore
- Adição do parâmetro <code>-pid</code> às chamadas da infra-estrutura do Snore
- Adição da infra-estrutura 'snoretoast' para o KNotifications no Windows

### KPeople

- Possibilidade de apagar os contactos nas infra-estruturas
- Possibilidade de modificação dos contactos

### KPlotting

- Adição de opção para compilar um 'plugin' do Qt Designer (BUILD_DESIGNERPLUGIN, por omissão ON)

### KRunner

- Certificação de que é verificada a finalização da desconstrução após terminar uma tarefa
- Adição de um sinal 'done' ao FindMatchesJob em vez de usar erradamente o QObjectDecorator

### KTextEditor

- Possibilidade de personalização dos atributos dos temas do KSyntaxHighlighting
- Correcção em todos os temas: possibilidade de desactivar os atributos nos ficheiros de realce em XML
- simplificação do 'isAcceptableInput' + permissão para todass as coisas nos métodos de entrada
- simplificação do 'typeChars', sem necessidade de retorno do código sem filtragem
- Imitação do QInputControl::isAcceptableInput() ao filtrar os caracteres escritos (erro 389796)
- tentativa de sanitização dos fins de linha na colagem (erro 410951)
- Correcção: possibilidade de desactivação dos atributos nos ficheiros de realce em XML
- melhoria da completação de palavras para usar o realce, de forma a detectar os limites das palavras (erro 360340)
- Mais migrações do QRegExp para o QRegularExpression
- verificação adequada se o comando 'diff' pode ser iniciado para a verificação de diferenças em ficheiros temporários (erro 389639)
- KTextEditor: Correcção da intermitência no contorno esquerdo ao mudar de documentos
- Migração de mais alguns QRegExp's para QRegularExpression's
- Possibilidade do 0 nos intervalos de linhas no modo do VIM
- Uso do 'find_dependency' do CMake em vez do 'find_package' no modelo de ficheiros de configuração do CMake

### KTextWidgets

- Adição de opção para compilar um 'plugin' do Qt Designer (BUILD_DESIGNERPLUGIN, por omissão ON)

### KUnitConversion

- Adição de unidades de potência em Decibel (dBW e múltiplos)

### KWallet Framework

- KWallet: correcção da inicialização do 'kwalletmanager', onde o nome do ficheiro .desktop tem um '5' no mesmo

### KWayland

- [servidor] Envolvência do 'proxyRemoveSurface' com um ponteiro inteligente
- [servidor] Maior uso do modo actualmente em 'cache' e garantias de validação
- [servidor] 'Cache' do modo actual
- Implementação do 'zwp_linux_dmabuf_v1'

### KWidgetsAddons

- [KMessageWidget] Passagem do 'widget' para o standardIcon()
- Adição de opção para compilar um 'plugin' do Qt Designer (BUILD_DESIGNERPLUGIN, por omissão ON)

### KWindowSystem

- KWindowSystem: adição da opção do CMake KWINDOWSYSTEM_NO_WIDGETS
- Descontinuação do slideWindow(QWidget *widget)
- Adição de versão substituta do KWindowSystem::setMainWindow(QWindow *)
- KWindowSystem: adição de versão substituta do setNewStartupId(QWindow *...)

### KXMLGUI

- Adição de opção para compilar um 'plugin' do Qt Designer (BUILD_DESIGNERPLUGIN, por omissão ON)

### NetworkManagerQt

- Mudança de nome do WirelessDevice::lastRequestScanTime para WirelessDevice::lastRequestScan
- Adição das propriedades 'lastScanTime' e 'lastRequestTime' no WirelessDevice

### Plasma Framework

- Mudança dos ícones de notificações para usar o estilo de contorno
- mudança para um tamanho mais consistente  dos botões de ferramentas
- Possibilidade de os elementos/contentores/papéis de parede adiarem o UIReadyConstraint
- Mudança dos ícones de notificações para se parecerem com campainhas (erro 384015)
- Correcção da posição inicial incorrecta das páginas nas barras de páginas verticais (erro 395390)

### Purpose

- Correcção do 'plugin' do Ecrã do Telegram no Fedora

### QQC2StyleBridge

- Impedimento de arrastar o conteúdo do ComboBox do QQC2 para fora do menu

### Solid

- Mudança da propriedade 'serial' da bateria para uma constante
- Exposição da propriedade 'technology' na interface da bateria

### Sonnet

- Adição de opção para compilar um 'plugin' do Qt Designer (BUILD_DESIGNERPLUGIN, por omissão ON)

### Realce de sintaxe

- C &amp; ISO C++: adição de dígrafos (dobragem &amp; pré-processador) (erro 411508)
- Markdown, TypeScript &amp; Logcat: algumas correcções
- Classe Format: adição de funções para saber se os ficheiros XML definem atributos de estilo
- combinação de coisas do test.m com o highlight.m existente
- Suporte para textos nativos do Matlab
- Gettext: Adição do estilo de "Texto Traduzido" e do atributo 'spellChecking' (erro 392612)
- Definição da indentação do OpenSCAD como sendo a do C, em vez de nenhuma
- Possibilidade de mudar os dados da Definition após o carregamento
- Indexação do realce: verificação do 'kateversion'
- Markdown: diversas melhorias e correcções (erro 390309)
- JSP: suporte do &lt;script&gt; e &lt;style&gt; ; uso do IncludeRule ##Java (erro 345003)
- LESS: importação das palavras-chave do CSS, novo realce e algumas melhorias
- JavaScript: remoção do contexto desnecessário de "Expressão Condicional"
- Nova sintaxe: SASS. Algumas correcções para o CSS e SCSS (erro 149313)
- Uso do 'find_dependency' do CMake no ficheiro de configuração do CMake em vez do 'find_package'
- SCSS: correcção da interpolação (#{...}) e adição da cor Interpolation
- correcção do atributo 'additionalDeliminator' (erro 399348)
- C++: os contratos não constam no C++20
- Gettext: correcção do "texto não-traduzido anterior" e outras melhorias/correcções
- Jam: Correcção do 'local' com uma variável sem inicialização e realce do SubRule
- passagem implícita caso exista um 'fallthroughContext'
- Adição das extensões de ficheiros comuns do GLSL (.vs, .gs, .fs)
- Latex: diversas correcções (modo matemático, 'verbatim' encadeado, ...) (erro 410477)
- Lua: correcção da cor do fim com diferentes níveis de encadeamento de condições e funções
- Indexação do realce: todos os avisos são fatais

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
