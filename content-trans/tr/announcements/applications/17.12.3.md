---
aliases:
- ../announce-applications-17.12.3
changelog: true
date: 2018-03-08
description: KDE, KDE Uygulamaları 17.12.3'ü Gönderdi
layout: application
title: KDE, KDE Uygulamaları 17.12.3'ü Gönderdi
version: 17.12.3
---
March 8, 2018. Today KDE released the third stability update for <a href='../17.12.0'>KDE Applications 17.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kaydedilen yaklaşık 25 hata düzeltmesi, diğerleri arasında Kontak, Dolphin, Gwenview, JuK, K İndir, Okular, Umbrello'da iyileştirmeler içerir.

İyileştirmeler şunları içerir:

- Akregatör, bir hatadan sonra artık feeds.opml besleme listesini silmez
- Gwenview'in tam ekran modu artık yeniden adlandırdıktan sonra doğru dosya adında çalışıyor
- Okular'daki birkaç nadir çökme tespit edildi ve düzeltildi
