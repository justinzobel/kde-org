---
aliases:
- ../announce-applications-18.04.2
changelog: true
date: 2018-06-07
description: KDE, KDE Uygulamaları 18.04.2'yi Gönderdi
layout: application
title: KDE, KDE Uygulamaları 18.04.2'yi Gönderdi
version: 18.04.2
---
June 7, 2018. Today KDE released the second stability update for <a href='../18.04.0'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kaydedilen yaklaşık 25 hata düzeltmesi, diğerleri arasında Kontak, Cantor, Dolphin, Gwenview, KGpg, Kig, Konsole, Lokalize, Okular için iyileştirmeler içerir.

İyileştirmeler şunları içerir:

- Gwenview'daki görüntü işlemleri artık geri alındıktan sonra yeniden yapılabilir
- KGpg artık sürüm başlığı olmayan mesajların şifresini çözemiyor
- Exporting of Cantor worksheets to LaTeX has been fixed for Maxima matrices
