---
aliases:
- ../announce-applications-15.04.1
changelog: true
date: '2015-05-12'
description: KDE veröffentlicht die Anwendungen 15.04.1.
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 15.04.1
version: 15.04.1
---
12. Mai 2014. Heute veröffentlicht KDE die erste Aktualisierung der <a href='../15.04.0'>KDE-Anwendungen 15.04</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 50 aufgezeichnete Fehlerkorrekturen enthalten Verbesserungen für kdelibs, kdepim, Kdenlive, Okular, Marble und Umbrello.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.19, KDE Development Platform 4.14.8 and the Kontact Suite 4.14.8.
