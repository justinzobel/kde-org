---
date: '2013-12-18'
hidden: true
title: KDE-sovellukset 4.12 tuo mukanaan suuria edistysaskelia henkilökohtaisten tietojen
  hallintaan ja parannuksia kaikkialle
---
KDE-yhteisö tiedottaa ylpeänä suurista KDE-sovellusten päivityksistä, jotka sisältävät uusia ominaisuuksia ja korjauksia. Tätä julkaisua leimaa runsaat parannukset KDE:n PIM-pinossa (henkilökohtaisten tietojen hallinnassa). Niiden ansiosta sen suorituskyky on nyt paljon parempi, ja siinä on paljon uusia ominaisuuksia. Katessa järkeistettiin Python-liitännäisten integraatiota, ja Katessa on nyt alustava tuki Vim-makroille. Peleissä ja opetusohjelmissa on monia uusia ominaisuuksia.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/kate.png" width="600px">}}

Linuxin kehittyneimmän graafisen tekstimuokkaimen Katen koodintäydennystä on jälleen parannettu: tällä kertaa siihen on lisätty <a href='http://scummos.blogspot.com/2013/10/advanced-code-completion-filtering-in.html'>kehittynyt hakukoodi, joka osaa käsitellä lyhenteet ja luokkien osittaiset osumat.</a> Uudella koodilla esimerkiksi ”LuokNim” löytää nimen ”LuokanNimi”. Kate saa myös <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-18th-august-2013'>alustavan Vim-makrotuen</a>. Parasta on, että kaikki parannukset valuvat myös KDevelopiin ja muihin Kate-teknologiaa käyttäviin sovelluksiin.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/okular.png" width="600px">}}

Asiakirjakatselin Okular <a href='http://tsdgeos.blogspot.com/2013/10/changes-in-okular-printing-for-412.html'>ottaa nyt tulostimen paperireunukset (marginaalit) huomioon</a>. Lisäksi siinä on nyt ääni- ja videotuki epub-tiedostoille ja parempi haku. Okular osaa nyt myös käsitellä isompaa osaa kuvien Exif-metatietojen kuvaamista muunnoksista. UML-mallinnusohjelma Umbrellossa yhteyksiä (assosiaatioita) voi nyt <a href='http://dot.kde.org/2013/09/20/kde-commit-digest-1st-september-2013'>piirtää käyttäen useita erilaisia asetteluita</a>, ja Umbrelloon on lisätty <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-25th-august-2013'>visuaalinen palaute siitä, onko objektia dokumentoitu</a>.

Yksityisyysvahti KGpg näyttää enemmän tietoja käyttäjille, ja KWalletManagerissa, salasanojen tallennusohjelmassa, <a href='http://www.rusu.info/wp/?p=248'>salasanat voi tallentaa GPG-muodossa</a>. Konsolessa tulosteen verkko-osoitteen Ctrl-napsautus avaa nyt osoitteen suoraan. Konsole osaa nyt myös <a href='http://martinsandsmark.wordpress.com/2013/11/02/mangonel-1-1-and-more/'>luetella käynnissä olevat prosessit varoittaessaan ohjelman sulkemisesta</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

KWebKit adds the ability to <a href='http://dot.kde.org/2013/08/09/kde-commit-digest-7th-july-2013'>automatically scale content to match desktop resolution</a>. File manager Dolphin introduced a number of performance improvements in sorting and showing files, reducing memory usage and speeding things up. KRDC introduced automatic reconnecting in VNC and KDialog now provides access to 'detailedsorry' and 'detailederror' message boxes for more informative console scripts. Kopete updated its OTR plugin and the Jabber protocol has support for XEP-0264: File Transfer Thumbnails. Besides these features the main focus was on cleaning code up and fixing compile warnings.

### Pelit ja opetusohjelmat

KDE-peleihin on tehty monenlaisia muutoksia. KReversi on <a href='http://tsdgeos.blogspot.ch/2013/10/kreversi-master-is-now-qt-quick-based.html'>nyt QML- ja Qt Quick -pohjainen</a>, mistä seuraa nätimpi ja jouhevampi pelauskokemus. Myös KNetWalk <a href='http://tsdgeos.blogspot.ch/2013/08/knetwalk-portedx-to-qtquick.html'>on siirretty</a> käyttämään Qt Quickia, ja se on saanut siten samat hyödyt. KNetWalkissa on nyt mahdollisuus asettaa ruudukolle haluamansa leveys ja korkeus. Konquestissa on uusi haastava tekoäly nimeltä ”Becai”.

In the Educational applications there have been some major changes. KTouch <a href='http://blog.sebasgo.net/blog/2013/11/12/what-is-new-for-ktouch-in-kde-sc-4-dot-12/'>introduces custom lesson support and several new courses</a>; KStars has a new, more accurate <a href='http://knro.blogspot.ch/2013/10/demo-of-ekos-alignment-module.html'>alignment module for telescopes</a>, find a <a href='http://www.youtube.com/watch?v=7Dcn5aFI-vA'>youtube video here</a> of the new features. Cantor, which offers an easy and powerful UI for a variety of mathematical backends, now has backends <a href='http://blog.filipesaraiva.info/?p=1171'>for Python2 and Scilab</a>. Read more about the powerful Scilab backend <a href='http://blog.filipesaraiva.info/?p=1159'>here</a>. Marble adds integration with ownCloud (settings are available in Preferences) and adds overlay rendering support. KAlgebra makes it possible to export 3D plots to PDF, giving a great way of sharing your work. Last but not least, many bugs have been fixed in the various KDE Education applications.

### Posti, kalenteri ja henkilökohtaiset tiedot

KDE PIMiin, KDE:n postin, kalenterin ja muiden henkilökohtaisten tietojen käsittelyn sovelluksiin, on tehty paljon työtä.

Starting with email client KMail, there is now <a href='http://dot.kde.org/2013/10/11/kde-commit-digest-29th-september-2013'>AdBlock support</a> (when HTML is enabled) and improved scam detection support by extending shortened URLs. A new Akonadi Agent named FolderArchiveAgent allows users to archive read emails in specific folders and the GUI of the Send Later functionality has been cleaned up. KMail also benefits from improved Sieve filter support. Sieve allows for server-side filtering of emails and you can now <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-script-parsing-22/'>create and modify the filters on the servers</a> and <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-12/'>convert existing KMail filters to server filters</a>. KMail's mbox support <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-mboximporter/'>has also been improved</a>.

In other applications, several changes make work easier and more enjoyable. A new tool is introduced, <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>the ContactThemeEditor</a>, which allows for creating KAddressBook Grantlee themes for displaying contacts. The addressbook can now also show previews before printing data. KNotes has seen some <a href='http://www.aegiap.eu/kdeblog/2013/11/what-news-in-kdepim-4-12-knotes/'>serious work on solving bugs</a>. Blogging tool Blogilo can now deal with translations and there are a wide variety of fixes and improvements all over the KDE PIM applications.

Benefiting all applications, the underlying KDE PIM data cache has <a href='http://ltinkl.blogspot.ch/2013/11/this-month-october-in-red-hat-kde.html'>seen much work on performance, stability and scalability</a>, fixing <a href='http://www.progdan.cz/2013/10/akonadi-1-10-3-with-postgresql-fix/'>support for PostgreSQL with the latest Qt 4.8.5</a>. And there is a new command line tool, the calendarjanitor which can scan all calendar data for buggy incidences and adds a debug dialog for search. Some very special hugs go to Laurent Montel for the work he is doing on KDE PIM features!

#### KDE-sovellusten asentaminen

KDE-ohjelmat, mukaan lukien kaikki KDE:n kirjastot ja sovellukset, ovat ilmaisesti saatavilla avoimen lähdekoodin lisenssien mukaisesti. KDE-ohjelmia voi käyttää useilla laitekokoonpanoilla ja suoritinarkkitehtuureilla kuten ARMilla ja x86:lla, useissa käyttöjärjestelmissä, ja ne toimivat kaikenlaisten ikkunointiohjelmien ja työpöytäympäristojen kanssa. Linux- ja UNIX-pohjaisten käyttöjärjestelmien lisäksi useimmista KDE-sovelluksista on saatavilla Microsoft Windows -versiot <a href='http://windows.kde.org'>KDE-ohjelmat Windowsissa</a> -sivustolta ja Apple Mac OS X -versiot <a href='http://mac.kde.org/'>KDE-ohjelmat Macillä</a> -sivustolta. Joistain KDE-sovelluksista on verkossa kokeellisia versioita, jotka toimivat useilla mobiilialustoilla kuten Meegolla, Microsoftin Windows Mobilella ja Symbianilla, mutta niitä ei tällä hetkellä tueta. <a href='http://plasma-active.org'>Plasma Active</a> on käyttökokemus laajemmalle laitekirjolle kuten esimerkiksi tablettitietokoneille ja muille mobiililaitteille.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.12.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Paketit

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.12.0 for some versions of their distribution, and in other cases community volunteers have done so.

##### Pakettien sijainnit

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.12.0'>Community Wiki</a>.

The complete source code for 4.12.0 may be <a href='/info/4/4.12.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.12.0 are available from the <a href='/info/4/4.12.0#binary'>4.12.0 Info Page</a>.

#### Järjestelmävaatimukset

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.

In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
