---
aliases:
- ../../kde-frameworks-5.59.0
date: 2019-06-08
layout: framework
libCount: 70
---
### Baloo

- Geen SQL-databasedumps proberen te indexeren
- .gcode en virtuele machine-bestanden uitsluiten van beschouwen voor indexering

### BluezQt

- Bluez API aan DBus XML parser/generator toevoegen

### Breeze pictogrammen

- ook gcompris-qt
- falkon-pictogram een echte SVG maken
- ontbrekende pictogrammen uit de toepassingen toevoegen om https://bugs.kde.org/show_bug.cgi?id=407527 opnieuw te doen
- pictogram voor kfourinline uit toepassing toevoegen, heeft ook bijwerken nodig
- kigo-pictogram https://bugs.kde.org/show_bug.cgi?id=407527 toevoegen
- kwave-pictogram uit kwave toevoegen, om opnieuw te worden gedaan in breeze-stijl
- Symbolische koppeling arrow-_-double naar go-_-skip, 24px go-*-skip
- Apparaatpictogramstijlen input-* wijzigen, 16px pictogrammen toevoegen
- Donkere versie van nieuw Knights-pictogram die ontsnapte uit mijn vorige commit
- Mieuw pictogram voor Knights gebaseerd op Anjuta's pictogram (bug 407527)
- pictogrammen toevoegen voor toepassingen die ontbreken in breeze, deze zouden bijgewerkt worden om meer breezy te zijn maar ze zijn voor nu nodig voor de nieuwe kde.org/applications
- kxstitch-pictogram uit kde:kxstitch, om te worden bijgewerkt
- niet alles glob maken en de keukengootsteen
- verzeker ook om ScaledDirectories toe te kennen

### Extra CMake-modules

- Specifieke map voor categorieënbestand van Qt-logging
- QT_STRICT_ITERATORS op Windows niet inschakelen

### Frameworkintegratie

- Zeker maken om ook in de oudere locatie te zoeken
- in de nieuwe locatie zoeken naar knsrc-bestanden

### KArchive

- Lezen en zoeken in KCompressionDevice testen
- KCompressionDevice: bIgnoreData verwijderen
- KAr: out-of-bounds lezen (bij ongeldige invoer) repareren door overzetten naar QByteArray
- KAr: ontleden van lange bestandsnamen met Qt-5.10 repareren
- KAr: de rechten zijn in octaal, niet decimaal
- KAr::openArchive: ook op ar_longnamesIndex is niet &lt; 0 controleren
- KAr::openArchive: ongeldige toegang tot geheugen op gebroken bestanden repareren
- KAr::openArchive: tegen Heap-buffer-overflow beschermen in gebroken bestanden
- KTar::KTarPrivate::readLonglink: crash repareren in misvormde bestanden

### KAuth

- dbus-policy installatiemap niet hard coderen

### KConfigWidgets

- Lokale valuta gebruiken voor donatiepictogram

### KCoreAddons

- Compilatie voor python bindings repareren (bug 407306)
- GetProcessList voor ophalen van de lijst met huidige actieve processen toevoegen

### KDeclarative

- qmldir-bestanden repareren

### Ondersteuning van KDELibs 4

- QApplication::setColorSpec verwijderen (lege methode)

### KFileMetaData

- Drie significante cijfers tonen bij tonen van paren (bug 343273)

### KIO

- Bytes manipuleren in plaats van tekens
- Uitvoerbare programma's kioslave die nooit eindigen repareren, bij instelling KDE_FORK_SLAVES
- Desktopkoppeling naar bestand of map repareren (bug 357171)
- Huidige filter testen alvorens een nieuwe in te stellen (bug 407642)
- [kioslave/file] een codec toevoegen voor oudere bestandsnamen (bug 165044)
- Op QSysInfo vertrouwen om de systeemdetails op te halen
- Documenten toevoegen aan de standaard lijst met plaatsen
- kioslave: argv[0] bewaren, om applicationDirPath() op niet-Linux te repareren
- Eén bestand of één map laten vallen toestaan op KDirOperator (bug 45154)
- Lange bestandsnaam afbreken alvorens een koppeling te maken (bug 342247)

### Kirigami

- [ActionTextField] QML-tekstballon consistent maken
- op hoogte baseren voor items die een opvulling bovenaan zouden moeten hebben (bug 405614)
- Prestatie: kleurwijzigingen comprimeren zonder een QTimer
- [FormLayout] boven en onder gelijke spatiëring gebruiken voor scheidingsteken (bug 405614)
- ScrollablePage: zeker maken dat de schuivende weergave de focus krijgt wanneer het wordt ingesteld (bug 389510)
- Gebruik van alleen toetsenbord van de werkbalk verbeteren (bug 403711)
- de recycler een FocusScope maken

### KNotification

- Behandel toepassingen die de eigenschap desktopFileName instellen met bestandsnaamachtervoegsel

### KService

- Ken soms toe (hash != 0) repareren wanneer een bestand is verwijderd door een ander proces
- Een andere toekenning repareren wanneer het bestand onder ons verdwijnt: ASSERT: "ctime != 0"

### KTextEditor

- Niet de gehele vorige regel verwijderen door backspace in positie 0 (bug 408016)
- Eigen dialoog gebruiken bij controle op overschrijven
- Actie om tekengrootte te resetten toevoegen
- altijd statische marker voor regelafbreking tonen indien gevraagd
- Nagaan van geaccentueerde reeks begin/eindmarkering na uitvouwen
- Reparatie: accentuering niet resetten wanneer enige bestanden worden opgeslagen (bug 407763)
- Automatisch inspringen: std::vector gebruiken in plaats van QList
- Reparatie: standaard inspringmodus voor nieuwe bestanden gebruiken (bug 375502)
- dubbele toekenning verwijderen
- auto-bracket instelling honoreren voor controle op balans
- controle op ongeldig teken verbeteren bij laden (bug 406571)
- Nieuw menu van syntaxisaccentuering in de statusbalk
- Oneindige loop vermijden in actie "Bevattende nodes omschakelen"

### KWayland

- Compositors toestaan discrete aswaarden te verzenden (bug 404152)
- set_window_geometry implementeren
- wl_surface::damage_buffer implementeren

### KWidgetsAddons

- KNewPasswordDialog: punten aan meldingwidgets toevoegen

### NetworkManagerQt

- Geen apparaatstatistieken ophalen bij constructie

### Plasma Framework

- Zorg dat Breeze licht/donker meer systeemkleuren gebruikt
- Soterrkolom van SortFilterModel exporteren naar QML
- plasmacore: qmldir, ToolTip.qml repareren die niet langer onderdeel zijn van module
- signaleer availableScreenRectChanged voor alle applets
- Eenvoudige configure_file gebruiken om de plasmacomponents3 bestanden te genereren
- *.qmltypes bijwerken naar huidige API van QML-modulen
- FrameSvg: maskcache ook wissen bij clearCache()
- FrameSvg: zorg hasElementPrefix() behandelt ook voorvoegsel met achtergevoegde -
- FrameSvgPrivate::generateBackground: achtergrond ook genereren als reqp != p
- FrameSvgItem: maskChanged ook uitsturen uit geometryChanged()
- FrameSvg: crash voorkomen bij aanroepen van mask() zonder nog gemaakt frame
- FrameSvgItem: maskChanged altijd uitsturen uit doUpdate()
- API dox: noteer voor FrameSvg::prefix()/actualPrefix() de achtergevoegde '-'
- API dox: naar Plasma5 versies wijzen op techbase indien beschikbaar
- FrameSvg: l- &amp; r- of t- &amp; b-randen  hebben niet noodzakelijk dezelfde hoogte resp. breedte

### Omschrijving

- [JobDialog] signaleer annuleren ook wanneer venster wordt gesloten door de gebruiker
- Rapporteer annuleren van configuratie als beëindigd met een fout (bug 407356)

### QQC2StyleBridge

- DefaultListItemBackground en MenuItem animatie verwijderen
- [QQC2 Slider Style] verkeerde handelpositionering repareren wanneer initiële waarde 1 is (bug 405471)
- ScrollBar: zorg dat het ook werkt als een horizontale schuifbalk (bug 390351)

### Solid

- Schoon de code op voor de manier apparaatbackends worden gebouwd en geregistreerd
- [Fstab] pictogram map-versleuteld gebruiken voor versleutelen van fuse-aankoppelingen

### Accentuering van syntaxis

- YAML: alleen commentaar na spaties en andere verbeteringen/reparaties (bug 407060)
- Markdown: includeAttrib in blokken code gebruiken
- accentuering van "0" in C modus repareren
- Tcsh: operators en sleutelwoorden repareren
- Syntaxisdefinitie voor de Common Intermediate Language toevoegen
- SyntaxHighlighter: voorgrondkleur voor tekst zonder speciale accentuering repareren (bug 406816)
- Voorbeeldtoepassing voor afdrukken van geaccentueerde tekst naar pdf toevoegen
- Markdown: kleur gebruiken met hoger contrast voor lijsten (bug 405824)
- .conf extensie van "INI Files" hl verwijderen, om de accentueerder te bepalen met gebruik van MIME-type (bug 400290)
- Perl: de // operator repareren (bug 407327)
- Hoofd- kleine letters repareren van  UInt* typen in Julia hl (bug 407611)

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
