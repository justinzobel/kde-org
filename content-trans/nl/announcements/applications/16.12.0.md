---
aliases:
- ../announce-applications-16.12.0
changelog: true
date: 2016-12-15
description: KDE stelt KDE Applicaties 16.12.0 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 16.12.0 beschikbaar
version: 16.12.0
---
15 december 2016. KDE introduceert vandaag KDE Applications 16.12 met een indrukwekkende hoeveelheid opwaarderingen wanneer het gaat om gemakkelijke toegang, de introductie van zeer nuttige functionaliteiten en reparatie van enige kleinere problemen die KDE Applications nu een stap dichter brengt bij het bieden van de perfecte setup voor uw apparaat.

<a href='https://okular.kde.org/'>Okular</a>, <a href='https://konqueror.org/'>Konqueror</a>, <a href='https://www.kde.org/applications/utilities/kgpg/'>KGpg</a>, <a href='https://www.kde.org/applications/education/ktouch/'>KTouch</a>, <a href='https://www.kde.org/applications/education/kalzium/'>Kalzium</a> en meer (<a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Uitgavenotities</a>) zijn nu overgezet naar KDE Frameworks 5. We kijken uit naar uw terugkoppeling en inzicht in de nieuwste mogelijkheden geïntroduceerd met deze uitgave.

In de voortgaande inspanning om toepassingen gemakkelijker alleenstaand te bouwen, hebben we de tarballs van kde-baseapps, kdepim en kdewebdev gesplitst. U kunt de nieuw gemaakte tarballs vinden in <a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_we_have_split'>het document Release Notes</a>

We gaan niet door met de volgende pakketten: kdgantt2, gpgmepp en kuser. Dit zal ons helpen te focussen op de rest van de code.

### Kwave geluidsbewerker doet mee met KDE Applications!

{{<figure src="https://www.kde.org/images/screenshots/kwave.png" width="600px" >}}

<a href='http://kwave.sourceforge.net/'>Kwave</a> is een geluidsbewerker, het kan vele soorten geluidsbestanden, inclusief multi-kanaal bestanden opnemen, afspelen, importeren en bewerken. Kwave bevat enige plug-ins om geluidsbestanden op verschillende manieren te transformeren en een grafische weergave te presenteren met een volledige zoom en schuifmogelijkheid.

### De wereld als uw achtergrond

{{<figure src="https://frinring.files.wordpress.com/2016/08/screenshot_20160804_171642.png" width="600px" >}}

Marble bevat nu zowel een achtergrondafbeelding en een widget voor Plasma die de tijd toont bovenop een satellietweergave van de aarde, met realtime dag/nacht weergave. Deze waren beschikbaar voor Plasma 4; ze zijn nu bijgewerkt om in Plasma 5 te werken.

U kunt meer informatie vinden op <a href='https://frinring.wordpress.com/2016/08/04/wip-plasma-world-map-wallpaper-world-clock-applet-powered-by-marble/'>de blog van Friedrich W.H. Kossebau</a>.

### Emoticons gloreren!

{{<figure src="/announcements/applications/16.12.0/kcharselect1612.png" width="600px" >}}

KCharSelect de mogelijkheid gekregen om het Unicode Emoticons blok te tonen (en andere SMP symboolblokken).

Het kreeg ook een menu Bladwijzers zodat u uw geliefde tekens als favoriet kunt markeren.

### Wiskunde is beter met Julia

{{<figure src="https://2.bp.blogspot.com/-BzJNpF5SXZQ/V7skrKcQttI/AAAAAAAAAA8/7KD8g356FfAd9-ipPcWYi6QX5_nCQJFKgCLcB/s640/promo.png" width="600px" >}}

Cantor heeft een nieuwe backend voor Julia, waarmee zijn gebruikers de mogelijkheid gegeven wordt om de laatste voortgang in wetenschappelijke rekenen te gebruiken.

U kunt meer informatie vinden op <a href='https://juliacantor.blogspot.com/2016/08/cantor-gets-support-of-julia-language.html'>de blog van Ivan Lakhtanov</a>.

### Geavanceerde archivering

{{<figure src="https://rthomsen6.files.wordpress.com/2016/11/blog-1612-comp-method.png" width="600px" >}}

Ark heeft verschillende nieuwe functies:

- Bestanden en mappen kunnen nu hernoemd, gekopieerd of verplaatst worden binnen het archief
- Het is nu mogelijke om algoritmen voor compressie en versleuteling te selecteren bij het aanmaken van archieven
- Ark kan nu AR-bestanden openen (bijv. Linux \*.a statische bibliotheken)

U kunt meer informatie vinden op <a href='https://rthomsen6.wordpress.com/2016/11/26/new-features-in-ark-16-12/'>de blog van Ragnar Thomsen</a>.

### En meer!

Kopete kreeg ondersteuning voor X-OAUTH2 SASL authenticatie in het jabber-protocol en repareerde enige problemen met de plug-in voor OTR-versleuteling.

Kdenlive heeft een nieuw Rotoscoping effect, ondersteuning voor te downloaden inhoud en een bijgewerkte bewegingsvolger. Het biedt ook <a href='https://kdenlive.org/download/'>Snap and AppImage</a> bestanden voor gemakkelijker installatie.

KMail en Akregator kunnen Google Safe Browsing gebruiken om te controleren of een koppeling waarop wordt geklikt onveilig is. Beiden hebben ook ondersteuning voor back printing (heeft Qt 5.8 nodig).

### Agressieve bestrijding van problemen

Meer dan 130 bugs zijn opgelost in toepassingen inclusief Dolphin, Akonadi, KAddressBook, KNotes, Akregator, Cantor, Ark, Kdenlive en meer!

### Volledige log met wijzigingen
