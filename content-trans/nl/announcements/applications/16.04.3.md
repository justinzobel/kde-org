---
aliases:
- ../announce-applications-16.04.3
changelog: true
date: 2016-07-12
description: KDE stelt KDE Applicaties 16.04.3 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 16.04.3 beschikbaar
version: 16.04.3
---
12 juli 2016. Vandaag heeft KDE de derde update voor stabiliteit vrijgegeven voor <a href='../16.04.0'>KDE Applicaties 16.04</a> Deze uitgave bevat alleen reparaties van bugs en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 20 aangegeven reparaties van bugs inclusief verbeteringen aan ark, cantor, kate, kdepim, umbrello, naast anderen.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.22.
