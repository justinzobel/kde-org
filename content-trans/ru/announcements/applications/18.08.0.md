---
aliases:
- ../announce-applications-18.08.0
changelog: true
date: 2018-08-16
description: KDE выпускает KDE Applications 18.08.0
layout: application
title: KDE выпускает KDE Applications 18.08.0
version: 18.08.0
---
16 августа 2018 года. Вышла новая версия KDE Applications 18.08.0.

Мы постоянно работаем над улучшением программного обеспечения, включённого в набор KDE Applications, надеясь, что сопутствующие улучшения и исправления ошибок будут вам полезны!

### What's new in KDE Applications 18.08

#### Базовые приложения

{{<figure src="/announcements/applications/18.08.0/dolphin1808-settings.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE's powerful file manager, has received various quality-of-life improvements:

- Диалоговое окно настройки стало интуитивнее.
- Были устранены утечки памяти, которые замедляли компьютер.
- Пункты меню «Создать» теперь недоступны при просмотре корзины.
- Теперь приложение лучше адаптируется к экранам с высоким разрешением.
- Контекстное меню теперь содержит пункты, позволяющие напрямую сортировать и изменять режим просмотра.
- Sorting by modification time is now 12 times faster. Also, you can now launch Dolphin again when logged in using the root user account. Support for modifying root-owned files when running Dolphin as a normal user is still work in progress.

{{<figure src="/announcements/applications/18.08.0/konsole1808-find.png" width="600px" >}}

Multiple enhancements for <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, KDE's terminal emulator application, are available:

- Строка поиска теперь появляется в верхней части окна, не отвлекая от рабочего процесса.
- Добавлена поддержка новых управляющих последовательностей (DECSCUSR и XTerm Alternate Scroll Mode).
- Теперь любой символ на клавиатуре можно использовать в составе комбинации клавиш.

#### Работа с графикой

{{<figure src="/announcements/applications/18.08.0/gwenview1808.png" width="600px" >}}

18.08 is a major release for <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE's image viewer and organizer. Over the last months contributors worked on a plethora of improvements. Highlights include:

- Теперь в строке состояния Gwenview имеется счетчик изображений, он показывает общее количество изображений.
- Появилась возможность сортировать изображения по оценке и в порядке убывания. Сортировка по дате теперь разделяет каталоги и архивы, а также исправлены некоторые ошибки, связанные с сортировкой по дате.
- Улучшена поддержка взаимодействия при помощи перетаскивания: можно просмотреть файлы и папки, перетащив их в Gwenview в режиме просмотра, а также можно перетаскивать просматриваемые объекты в другие приложения.
- Вставка скопированных изображений из Gwenview теперь работает для приложений, которые принимают только содержимое изображения, но путь к файлу. Поддерживается копирование изменённых изображений.
- Диалоговое окно изменения размера изображения было изменено для удобства взаимодействия. Также добавлена возможность изменения размеров изображения с указанием нового размера в процентах от старого.
- Исправлены ошибки, связанные с ползунком размера в инструменте для уменьшения эффекта красных глаз и курсором-перекрестием.
- Под полупрозрачным изображением теперь можно выбрать отсутствие фона. Такой параметр также доступен для изображений SVG.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-resize.png" width="600px" >}}

Функция масштабирования изображения стала удобнее:

- Теперь во время операций кадрирования и уменьшения эффекта красных глаз можно масштабировать изображение с помощью прокрутки или щелчков мышью и перемещать его.
- Повторное нажатие средней кнопкой мыши переключает между уровнем масштаба 100% и уровнем масштаба для вмещения всего изображения в окно Gwenview.
- Добавлены комбинации клавиш Shift+средняя кнопка мыши и Shift+F, которые применяют и отменяют масштабирование с заполнением.
- Масштабирование с помощью клавиши Ctrl и щелчка мышью происходит быстрее и надёжнее.
- Gwenview теперь использует текущее положение указателя мыши в качестве неподвижной точки при масштабировании при помощи комбинаций клавиш на клавиатуре и мыши.

Режим сравнения изображений получил несколько улучшений:

- Исправлен размер и выравнивание границы выбранной области изображения.
- Исправили ошибку на границе выбранной области изображения при работе с изображениями SVG.
- Для небольших SVG-изображений граница выделенной области равна размерам всего изображения.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-sort.png" width="600px" >}}

Был сделан ряд небольших улучшений, чтобы сделать ваш рабочий процессприятнее:

- Улучшены плавные переходы между изображениями с разными размерами и с прозрачностью.
- Исправлена видимость значков на некоторых плавающих кнопках, когда используется светлая цветовая схема.
- При сохранении изображения под новым именем просмотрщик теперь не будет открывать другое, несвязанное изображение.
- Если нажать кнопку «Опубликовать», но пакет kipi-plugins не установлен, то Gwenview предложит установить его. Сразу после установки этого пакета будут показаны способы публикации.
- Боковая панель теперь предотвращает случайное скрытие при изменении размера и помнит прежнюю ширину.

#### Офисные приложения

{{<figure src="/announcements/applications/18.08.0/kontact1808.png" width="600px" >}}

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, KDE's powerful email client, features some improvements in the travel data extraction engine. It now supports UIC 918.3 and SNCF train ticket barcodes and Wikidata-powered train station location lookup. Support for multi-traveler itineraries was added, and KMail now has integration with the KDE Itinerary app.

<a href='https://userbase.kde.org/Akonadi'>Akonadi</a>, the personal information management framework, is now faster thanks to notification payloads and features XOAUTH support for SMTP, allowing for native authentication with Gmail.

#### Образовательные приложения

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, KDE's frontend to mathematical software, now saves the status of panels (\"Variables\", \"Help\", etc.) for each session separately. Julia sessions have become much faster to create.

User experience in <a href='https://www.kde.org/applications/education/kalgebra/'>KAlgebra</a>, our graphing calculator, has been significantly improved for touch devices.

#### Утилиты

{{<figure src="/announcements/applications/18.08.0/spectacle1808.png" width="600px" >}}

Contributors to <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE's versatile screenshot tool, focused on improving the Rectangular Region mode:

- В режиме съёмки прямоугольной области теперь есть экранная лупа, которая помогает идеально точно установить границы прямоугольника.
- Теперь вы можете перемещать и изменять размер прямоугольной области с помощью клавиатуры.
- Пользовательский интерфейс будет использовать текущую цветовую схему. Улучшен показ справочной информации.

Чтобы упростить отправку снимков другим людям, ссылки на загруженные на сервер изображения теперь автоматически копируются в буфер обмена. Снимки экрана теперь могут автоматически сохраняться в пользовательских подкаталогах.

<a href='https://userbase.kde.org/Kamoso'>Kamoso</a>, our webcam recorder, was updated to avoid crashes with newer GStreamer versions.

### Сведения об ошибках

Зафиксировано более 120 исправлений ошибок в приложениях, в числе которых: Kontact, Ark, Cantor, Dolphin, Gwenview, Kate, Konsole, Okular, Spectacle, Umbrello и другие.

### Полный список изменений
