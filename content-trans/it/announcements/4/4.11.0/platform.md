---
date: 2013-08-14
hidden: true
title: KDE Platform 4.11 porta prestazioni migliori
---
KDE Platform 4 è stata in stato di «feature freeze» (blocco nello sviluppo di nuove funzionalità) a partire dalla versione 4.9. Questa versione include quindi esclusivamente correzioni di bug e miglioramenti nelle prestazioni.

Il gestore di archivi dati semantici e motore di ricerca Nepomuk ha ricevuto notevoli miglioramenti nelle prestazioni, come un insieme di ottimizzazioni nella lettura che rendono la lettura dei dati fino a sei volte più veloce. L'indicizzazione è diventata più intelligente, ed è stata spezzata in due fasi. Il primo passo recupera le informazioni generali (come il tipo e il nome del file) immediatamente; informazioni aggiuntive come i tag multimediali, informazioni sull'autore e altre sono estratte in un secondo, più lento, passo. La visualizzazione dei metadati degli elementi appena creati o scaricati è ora molto più veloce. Inoltre gli sviluppatori di Nepomuk hanno migliorato il sistema di salvataggio e ripristino delle copie di sicurezza. Infine, ma non meno importante, ora Nepomuk può anche indicizzare una varietà di formati di documenti come ODF e docx.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/dolphin.png" caption=`Funzioni semantiche in azione in Dolphin` width="600px">}}

Il formato di salvataggio dei dati ottimizzato e la riscrittura dell'indicizzatore dei messaggi di posta di Nepomuk richiedono una nuova indicizzazione di alcuni dei contenuti del disco rigido. Di conseguenza l'esecuzione di tale processo consumerà un quantitativo inusuale di prestazioni computazionali per un certo periodo – in base alla quantità dei contenuti che devono essere reindicizzati. La conversione automatica della banca dati di Nepomuk verrà eseguita al primo accesso.

Sono presenti anche altre correzioni minori che <a href='https://projects.kde.org/projects/kde/kdelibs/repository/revisions?rev=KDE%2F4.11'>possono essere consultate nei log di git</a>.

#### Installare la piattaforma di sviluppo di KDE

Il software KDE, incluse tutte le sue librerie e le sue applicazioni, è disponibile liberamente e gratuitamente sotto licenze Open Source. Il software KDE funziona su varie configurazioni hardware e architetture CPU come ARM e x86, su vari sistemi operativi e funziona con ogni tipo di gestore di finestre o ambiente desktop. Oltre a Linux e altri sistemi operativi basati su UNIX puoi trovare versioni per Microsoft Windows di buona parte delle applicazioni KDE sul sito del <a href='http://windows.kde.org'>software KDE per Windows</a> e versioni Apple Mac OS X sul sito del <a href='http://mac.kde.org/'>software KDE per Mac</a>. Versioni sperimentali di applicazioni KDE per varie piattaforme mobili come MeeGo, MS Windows Mobile e Symbian possono essere trovate sul web ma non sono al momento supportate. <a href='http://plasma-active.org'>Plasma Active</a> fornisce l'esperienza utente per una vasta gamma di dispositivi, come tablet e altro hardware mobile.

Il software KDE si può scaricare come sorgente e in vari formati binari da <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> e può essere anche ottenuto su <a href='/download'>CD-ROM</a> o con qualsiasi <a href='/distributions'>principale distribuzione GNU/Linux o sistema UNIX</a> attualmente disponibile.

##### Pacchetti

Alcuni fornitori di sistemi Linux/UNIX hanno gentilmente messo a disposizione pacchetti binari di 4.11.0 per alcune versioni delle rispettive distribuzioni, e in altri casi dei volontari della comunità hanno provveduto. <br />

##### Posizione dei pacchetti

Per l'elenco aggiornato dei pacchetti binari disponibili di cui la squadra di rilascio di KDE è stata informata, visita il <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>wiki Community</a>.

Il codice sorgente completo per 4.11.0 può essere <a href='/info/4/4.11.0'>scaricato liberamente</a>. Le istruzioni su come compilare e installare il software KDE 4.11.0 sono disponibili dalla <a href='/info/4/4.11.0#binary'>pagina di informazioni di 4.11.0</a>.

#### Requisiti di sistema

Per poter ottenere il massimo da questi rilasci, raccomandiamo l'uso di una versione recente di Qt, come 4.8.4. Questo è necessario per poter assicurare un'esperienza stabile e più efficiente, dato che alcuni miglioramenti del software KDE sono stati in realtà effettuati sul framework Qt sottostante.<br /> Per usare in pieno le funzionalità del software KDE raccomandiamo inoltre di usare i driver grafici più recenti disponibili per il tuo sistema, in quanto l'esperienza utente può migliorare notevolmente, sia nelle funzionalità opzionali, sia nelle prestazioni sia nella stabilità globale.

## Oggi sono stati annunciati anche:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 continua ad affinare l'esperienza utente</a>

In preparazione per il supporto a lungo termine, Plasma Workspaces porta ulteriori miglioramenti alle funzionalità di base con una barra delle applicazioni più fluida, un indicatore della batteria più intelligente ed un mixer audio migliorato. L'introduzione di KScreen porta in Workspaces una gestione intelligente di monitor multipli, mentre miglioramenti su larga scala delle prestazioni combinati con piccole correzioni all'usabilità producono un'esperienza d'uso complessiva più gradevole. 

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Application 4.11 introduce grandi passi avanti nella gestione delle informazioni personali (PIM) e miglioramenti globali</a>

Questo rilascio vede imponenti miglioramenti in KDE PIM che forniscono migliori prestazioni e molte nuove funzioni. Kate migliora la produttività degli sviluppatori Python e Javascript con nuove estensioni, Dolphin è diventato più veloce e le applicazioni didattiche hanno varie nuove funzionalità.
