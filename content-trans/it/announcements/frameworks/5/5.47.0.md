---
aliases:
- ../../kde-frameworks-5.47.0
date: 2018-06-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Terminate query execution early if subterm returns empty result set
- Avoid crash when reading corrupt data from document terms db (bug 392877)
- handle string lists as input
- Ignore more types of source files (bug 382117)

### Icone Brezza

- updated handles and overflow-menu

### Moduli CMake aggiuntivi

- Android toolchain: allow to specify extra libs manually
- Android: Don't define qml-import-paths if it's empty

### KArchive

- handle zip files embedded within zip files (bug 73821)

### KCMUtils

- [KCModuleQml] Ignore disabled controls when tabbing

### KConfig

- kcfg.xsd - do not require a kcfgfile element

### KConfigWidgets

- Fix the "Default" color scheme to match Breeze again

### KDeclarative

- Set kcm context property on the correct context
- [Plotter] Don't render if m_node is null (bug 394283)

### KDocTools

- Update the list of Ukrainian entities
- add entity OSD to general.entites
- Add entities CIFS, NFS, Samba, SMB to general.entities
- Add Falkon, Kirigami, macOS, Solid, USB, Wayland, X11, SDDM to general entities

### KFileMetaData

- check that ffmpeg is at least version 3.1 that introduce the API we require
- search for album artist and albumartist tags in taglibextractor
- popplerextractor: don't try to guess the title if there isn't one

### KGlobalAccel

- Make sure ungrab keyboard request is processed before emitting shortcut (bug 394689)

### KHolidays

- holiday_es_es - Fix day of the "Comunidad de Madrid"

### KIconThemes

- Check if group &lt; LastGroup, as KIconEffect doesn't handle UserGroup anyway

### KImageFormats

- Remove duplicated mime types from json files

### KIO

- Check if destination exists also when pasting binary data (bug 394318)
- Auth support: Return the actual length of socket buffer
- Auth support: Unify API for file descriptor sharing
- Auth support: Create socket file in user's runtime directory
- Auth support: Delete socket file after use
- Auth support: Move task of cleaning up socket file to FdReceiver
- Auth support: In linux don't use abstract socket to share file descriptor
- [kcoredirlister] Remove as many url.toString() as possible
- KFileItemActions: fallback to default mimetype when selecting only files (bug 393710)
- Introduce KFileItemListProperties::isFile()
- KPropertiesDialogPlugin can now specify multiple supported protocols using X-KDE-Protocols
- Preserve fragment when redirecting from http to https
- [KUrlNavigator] Emit tabRequested when path in path selector menu is middle-clicked
- Performance: use the new uds implementation
- Don't redirect smb:/ to smb:// and then to smb:///
- Allow accepting by double-click in save dialog (bug 267749)
- Enable preview by default in the filepicker dialog
- Hide file preview when icon is too small
- i18n: use plural form again for plugin message
- Use a regular dialog rather than a list dialog when trashing or deleting a single file
- Make the warning text for deletion operations emphasize its permanency and irreversibility
- Revert "Show view mode buttons in the open/save dialog's toolbar"

### Kirigami

- Show action.main more prominently on the ToolBarApplicationHeader
- Allow Kirigami build without KWin tablet mode dependency
- correct swipefilter on RTL
- correct resizing of contentItem
- fix --reverse behavior
- share contextobject to always access i18n
- make sure tooltip is hidden
- make sure to not assign invalid variants to the tracked properties
- handle not a MouseArea, dropped() signal
- no hover effects on mobile
- proper icons overflow-menu-left and right
- Drag handle to reorder items in a ListView
- Use Mnemonics on the toolbar buttons
- Added missing files in QMake's .pri
- [API dox] Fix Kirigami.InlineMessageType -&gt; Kirigami.MessageType
- fix applicationheaders in applicationitem
- Don't allow showing/hiding the drawer when there's no handle (bug 393776)

### KItemModels

- KConcatenateRowsProxyModel: properly sanitize input

### KNotification

- Fix crashes in NotifyByAudio when closing applications

### KPackage Framework

- kpackage*install**package: fix missing dep between .desktop and .json
- make sure paths in rcc are never derived from absolute paths

### KRunner

- Process DBus replies in the ::match thread (bug 394272)

### KTextEditor

- Don't use title case for the "show word count" checkbox
- Make the word/char count a global preference

### KWayland

- Increase org_kde_plasma_shell interface version
- Add "SkipSwitcher" to API
- Add XDG Output Protocol

### KWidgetsAddons

- [KCharSelect] Fix table cell size with Qt 5.11
- [API dox] Remove usage of overload, resulting in broken docs
- [API dox] Tell doxygen "e.g." does not end the sentence, use ". "
- [API dox] Remove unneeded HTML escaping
- Don't automatically set the default icons for each style
- Make KMessageWidget match Kirigami inlineMessage's style (bug 381255)

### NetworkManagerQt

- Make information about unhandled property just debug messages
- WirelessSetting: implement assignedMacAddress property

### Plasma Framework

- Templates: consistent naming, fix translation catalog names &amp; more
- [Breeze Plasma Theme] Fix kleopatra icon to use color stylesheet (bug 394400)
- [Dialog] Handle dialog being minimized gracefully (bug 381242)

### Scopo

- Improve Telegram integration
- Treat inner arrays as OR constraints rather than AND
- Make it possible to constrain plugins by a desktop file presence
- Make it possible to filter plugins by executable
- Highlight the selected device in the KDE Connect plugin
- fix i18n issues in frameworks/purpose/plugins
- Add Telegram plugin
- kdeconnect: Notify when the process fails to start (bug 389765)

### QQC2StyleBridge

- Use pallet property only when using qtquickcontrols 2.4
- Work with Qt&lt;5.10
- Fix height of tabbar
- Use Control.palette
- [RadioButton] Rename "control" to "controlRoot"
- Don't set explicit spacing on RadioButton/CheckBox
- [FocusRect] Use manual placement instead of anchors
- It turns out the flickable in a scrollview is the contentItem
- Show focus rect when CheckBox or RadioButton are focused
- hacky fix to scrollview detection
- Don't reparent the flickable to the mousearea
- [TabBar] Switch tabs with mouse wheel
- Control must not have children (bug 394134)
- Constrain scroll (bug 393992)

### Evidenziazione della sintassi

- Perl6: Add support for extensions .pl6, .p6, or .pm6 (bug 392468)
- DoxygenLua: fix closing comment blocks (bug 394184)
- Add pgf to the latex-ish file formats (same format as tikz)
- Add postgresql keywords
- Highlighting for OpenSCAD
- debchangelog: add Cosmic Cuttlefish
- cmake: Fix DetectChar warning about escaped backslash
- Pony: fix identifier and keyword
- Lua: updated for Lua5.3

### Informazioni di sicurezza

Il codice rilasciato è stato firmato con GPG utilizzando la chiave seguente: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impronta della chiave primaria: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Puoi discutere e condividere idee su questo rilascio nella sezione dei commenti nell'<a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>articolo sul Dot</a>.
