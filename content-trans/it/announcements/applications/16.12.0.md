---
aliases:
- ../announce-applications-16.12.0
changelog: true
date: 2016-12-15
description: KDE rilascia KDE Applications 16.12.0
layout: application
title: KDE rilascia KDE Applications 16.12.0
version: 16.12.0
---
15 dicembre 2016. Oggi KDE introduce KDE Applications 16.12, con una serie impressionante di aggiornamenti in termini di una maggiore semplicità di accesso, l'introduzione di funzionalità molto utili ed eliminando alcuni problemi minori, portando KDE Applications un passo più vicino a offrirti la configurazione perfetta per il tuo dispositivo.

<a href='https://okular.kde.org/'>Okular</a>, <a href='https://konqueror.org/'>Konqueror</a>, <a href='https://www.kde.org/applications/utilities/kgpg/'>KGpg</a>, <a href='https://www.kde.org/applications/education/ktouch/'>KTouch</a>, <a href='https://www.kde.org/applications/education/kalzium/'>Kalzium</a> e altro (<a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Note di rilascio</a>) ora sono stati portati su KDE Frameworks 5. Attendiamo con impazienza il tuo riscontro e idee sulle nuove funzionalità introdotte con questa versione.

Nel continuo sforzo di rendere le applicazioni più facili da compilare  in modo autonomo, abbiamo diviso i tarball kde-baseapps, kdepim and kdewebdev . Puoi trovare i tarball appena creati sul <a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_we_have_split'>documento Note di rilascio</a>

Abbiamo abbandonato i seguenti pacchetti: kdgantt2, gpgmepp e kuser. Questo ci aiuterà a concentrarci sul resto del codice.

### L'editor dei suoni Kwave si unisce a KDE Applications!

{{<figure src="https://www.kde.org/images/screenshots/kwave.png" width="600px" >}}

<a href='http://kwave.sourceforge.net/'>Kwave</a> è un editor audio, può registrare, riprodurre, importare e modificare molti tipi di file audio, inclusi i file con più canali. Kwave include alcune estensioni per trasformare i file audio in diversi modi e presenta una vista grafica con una capacità completa di ingrandimento e scorrimento.

### Il mondo come sfondo

{{<figure src="https://frinring.files.wordpress.com/2016/08/screenshot_20160804_171642.png" width="600px" >}}

Marble ora include sia uno sfondo che un oggetto per Plasma che mostrano l'orario in cima a una vista satellitare della Terra, con visualizzazione giorno/notte in tempo reale. Questi erano disponibili per Plasma 4; ora sono stati aggiornati per funzionare su Plasma 5.

Puoi trovare maggiori informazioni nel <a href='https://frinring.wordpress.com/2016/08/04/wip-plasma-world-map-wallpaper-world-clock-applet-powered-by-marble/'>blog di Friedrich W. H. Kossebau</a>.

### Emoticon a fiumi!

{{<figure src="/announcements/applications/16.12.0/kcharselect1612.png" width="600px" >}}

KCharSelect ha acquisito la capacità di mostrare il blocco di emoticon Unicode (e altri blocchi di simboli SMP).

Ha anche ottenuto un menu Segnalibri in modo da poter aggiungere ai preferiti tutti i tuoi personaggi amati.

### La matematica è meglio con Julia

{{<figure src="https://2.bp.blogspot.com/-BzJNpF5SXZQ/V7skrKcQttI/AAAAAAAAAA8/7KD8g356FfAd9-ipPcWYi6QX5_nCQJFKgCLcB/s640/promo.png" width="600px" >}}

Cantor ha un nuovo motore per Julia, offrendo ai suoi utenti la possibilità di utilizzare gli ultimi progressi nel calcolo scientifico.

Puoi trovare maggiori informazioni nel <a href='https://juliacantor.blogspot.com/2016/08/cantor-gets-support-of-julia-language.html'>blog di Ivan Lakhtanov</a>.

### Archiviazione avanzata

{{<figure src="https://rthomsen6.files.wordpress.com/2016/11/blog-1612-comp-method.png" width="600px" >}}

Ark ha diverse novità:

- I file e le cartelle possono ora essere rinominati, copiati o spostati all'interno dell'archivio
- È ora possibile selezionare algoritmi di compressione e cifratura durante la creazione di archivi
- Ark ora può aprire i file AR (ad es. librerie statiche di Linux \*.a)

Puoi trovare maggiori informazioni nel <a href='https://rthomsen6.wordpress.com/2016/11/26/new-features-in-ark-16-12/'>blog di Ragnar Thomsen</a>.

### E altro!

Kopete ha ottenuto il supporto per l'autenticazione X-OAUTH2 SASL nel protocollo jabber e ha risolto alcuni problemi con l'estensione di cifratura OTR.

Kdenlive ha un nuovo effetto Rotoscoping, supporto per contenuti scaricabili e un sistema di tracciamento del movimento aggiornato. Fornisce inoltre file <a href='https://kdenlive.org/download/'>Snap e AppImage</a> per un'installazione più semplice.

KMail e Akregator possono utilizzare Google Safe Browsing per verificare se un collegamento sul quale si è fatto clic è dannoso. Entrambi hanno anche aggiunto il supporto per la stampa (necessita di Qt 5.8).

### Controllo aggressivo dei parassiti

Oltre 130 errori corretti in applicazioni tra cui Dolphin, Akonadi, KAddressBook, KNotes, Akregator, Cantor, Ark, Kdenlive e altre.

### Elenco completo dei cambiamenti
