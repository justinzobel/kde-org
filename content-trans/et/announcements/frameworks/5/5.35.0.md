---
aliases:
- ../../kde-frameworks-5.35.0
date: 2017-06-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Vigade märguannete täiustus

### BluezQt

- Vahetu argumentide loendi edastamine. See takistab QProcess'il üritamast käidelda meie ruumi, mis sisaldab asukohta shelli kaudu
- Parandus: omaduse muutused jäid arvesse võtmata kohe pärast objekti lisamist (veateade 377405)

### Breeze'i ikoonid

- awk'i MIME uuendamine, sest see on skriptikeel (veateade 376211)

### CMake'i lisamoodulid

- hidden-visibility testimise taastamine Xcode 6.2-ga
- ecm_qt_declare_logging_category(): veel unikaalsem kaasatute kaitsmine päises
- Sõnumite "Genereeritud. Ärge muutke" lisamine või täiustamine ja ühtlustamine
- Uue mooduli FindGperf lisamine
- pkgconfig'i vaikimisi paigalduse asukoha muutmine FreeBSD peal

### KActivitiesStats

- kactivities-stats'i parandus 3. kihis

### KDE Doxygeni tööriistad

- Võtmesõna Q_REQUIRED_RESULT ei kaaluta

### KAuth

- Kontroll, et kes iganes meid välja kutsub, on tõepoolest see, kellena esineb

### KCodecs

- gperf'i väljundi genereerimine ehitamisajal

### KCoreAddons

- Kohase lõimekaupa seemnete jagamise tagamine KRandom'is
- QRC asukohti ei jälgita (veateade 374075)

### KDBusAddons

- pid'i ei kaasata dbus'i asukohta flatpak'i peal

### KDeclarative

- signaali MouseEventListener::pressed ühtlustatud väljastamine
- MimeData objekti ei lekitata (veateade 380270)

### KDELibs 4 toetus

- CMAKE_SOURCE_DIR'i asukohas tühimärkide käitlemine

### KEmoticons

- Parandus: Qt5::DBus on ainult privaatselt kasutatav

### KFileMetaData

- /usr/bin/env kasutamine python2 leidmiseks

### KHTML

- kentities'i gperf'i väljundi genereerimine ehitamisajal
- doctypes'i gperf'i väljundi genereerimine ehitamisajal

### KI18n

- Programmer's Guide'i täiendamine märkustega setlocale() mõju kohta

### KIO

- Probleemi käitlemine, mille puhul rakenduste teatavad elemendid (näiteks Dolphini failivaade)muutusid kättesaamatuks high-dpi mitmeekraaniseadistuse korral (veateade 365548)
- [RenameDialog] jõuga lihtteksti vorming
- PIE binaarfailide (application/x-sharedlib) määratlemine täitmisfailidena (veateade 350018)
- core: GETMNTINFO_USES_STATVFS avalikustamine seadistuse päises
- PreviewJob: võrgukataloogide vahelejätmine. Eelvaatlus on liiga kulukas (veateade 208625)
- PreviewJob: tühja ajutise faili puhastamine, kui get() nurjub (veateade 208625)
- Üksikasjaliku puuvaate näitamise kiirendamine liiga paljusid veeru suuruse muutmisi vältides

### KNewStuff

- Ühe QNAM-i (ja kettapuhvri) kasutamine HTTP töödes
- Sisemine puhver pakkuja andmetele initsialiseerimisel

### KNotification

- Parandus: KSNIs ei suutnud registreerida teenust flatpakis
- Rakenduse nime kasutamine pid'i asemel SNI dbus-teenuse loomisel

### KPeople

- Privaatsete teekide sümboleid ei ekspordita
- KF5PeopleWidgets'i ja KF5PeopleBackend'i sümbolite eksportimise parandus
- #warning'i piiritlemine GCC-ga

### KWalleti raamistik

- kwalletd4 asendamine pärast migreerimise lõpetamist
- Migreerimisagendi lõpetamise puhul antakse signaal
- Migreerimisagendi taimeri käivitamine ainult vajaduse korral
- Unikaalse rakenduseisendi kontrollimine võimalikult vara

### KWayland

- requestToggleKeepAbove/below lisamine
- QIcon::fromTheme'i hoidmine põhilõimes
- pid'i changedSignal'i eemaldamine Client::PlasmaWindow's
- pid'i lisamine plasma aknahalduse protokolli

### KWidgetsAddons

- KViewStateSerializer: krahhi vältimine, kui vaade hävitatakse enne oleku serialiseerimist (veateade 353380)

### KWindowSystem

- Parem parandus NetRootInfoTestWM'ile tühimärkidega asukoha puhul

### KXMLGUI

- Peaakna määramine autonoomsete hüpikmenüüde eellaseks
- Menüühierarhia ehitamisel kuulutatakse menüüde eellaseks nende konteinerid

### Plasma raamistik

- VLC süsteemisalve ikooni lisamine
- Plasmoidimallid: pildi kasutamine, mis kuulub paketti (taas)
- QML-laiendusega Plasma QML-apleti malli lisamine
- plasmashellsurf'i taasloomine avalikustamisel, hävitamine peitmisel

### Süntaksi esiletõstmine

- Haskell:  "julius" kvaasitsiteerija esiletõstmine Normal##Javascript'i reeglite järgi
- Haskell: hamlet'i esiletõstmise lubamine ka "shamlet'i" kvaasitsiteerija korral

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
