---
aliases:
- ../../kde-frameworks-5.15.0
date: 2015-10-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Piirangu/nihke käitlemise parandus SearchStore::exec'is
- Baloo indeksi taasloomine
- balooctl config: võtmete lisamine määrata/näha onlyBasicIndexing
- balooctl kontrolli portimine töötamiseks uue arhitektuuri peale (veateade 353011)
- FileContentIndexer: filePath'i kahekordse edastamise parandus
- UnindexedFileIterator: mtime on quint32, mitte quint64
- Transaction: veel ühe Dbi kirjavea parandus
- Transaction: documentMTime() ja documentCTime() vale Dbi kasutamise parandus.
- Transaction::checkPostingDbInTermsDb: koodi optimeerimine
- dbus'i hoiatuste parandus
- Balooctl: käsu checkDb lisamine
- balooctl config: välistamisfiltri lisamine
- KF5Baloo: tagamine, et D-Bus'i liidesed oleksid enne kasutamist genereeritud. (veateade 353308)
- QByteArray::fromRawData kasutamise vältimine
- Baloo jälgija eemaldamine Baloost
- TagListJob: vea edastamine andmebaasi avamise nurjumisel
- Alamtermineid ei eirata ka siis, kui neid ei leita
- Puhtam kood Baloo::File::load() nurjumise korral andmebaasi avamise nurjumise järel.
- balooctl kasutab IndexerConfig'i, mitte ei käitle otseselt baloofilerc'd
- balooshow i18n täiustamine
- balooshow nurjub viisakalt, kui andmebaasi ei õnnestu avada.
- Baloo::File::load() nurjub, kui andmebaas ei ole avatud. (veateade 353049)
- IndexerConfig: meetodi refresh() lisamine
- inotify: Do not simulate a closedWrite event after move without cookie
- ExtractorProcess: Remove the extra n at the end of the filePath
- baloo_file_extractor: QProcess::close väljakutsumine enne QProcess'i hävitamist
- baloomonitorplugin/balooctl: indekseerija oleku muutmine tõlgitavaks.
- BalooCtl: võtme 'config' lisamine
- baloosearchi'i muutmine esinduslikumaks
- Tühjade EventMonitor'i failide eemaldamine
- BalooShow: näidatakse rohkem teavet, kui ID-d ei sobi kokku
- BalooShow: millal kutsuda välja ID kontroll, kui ID on korrektne
- Klassi FileInfo lisamine
- Veakontrollide lisamine mitmele poole, et Balood ei tabaks krahh, kui see on keelatakse (veateade 352454).
- Baloo arvestab nüüd "ainult põhiindekseerimise" seadistusvalikut
- Monitor: jäänud aja hankimine käivitamisel
- Tegelike meetodite väljakutsete kasutamine MainAdaptor'is QMetaObject::invokeMethod asemel
- Tagasiühilduvuse huvides org.kde.baloo liidese lisamine juurobjektile
- Aadressiribal näidatava kuupäevastringi parandus (viga oli tingitud portimisest QDate peale)
- Viivituse lisamine iga faili, mitte iga hulgitoimingu järele
- Qt::Widgets'i sõltuvuse eemaldamine baloo_file'ilt
- Kasutamata koodi eemaldamine baloo_file_extractor'ist
- Baloo jälgija või eksperimentaalse QML-plugina lisamine
- "jäänud aja pärimise" muutmine lõimekindlaks
- kioslaves: puuduvate virtuaalsete funktsioonide tühistamiste lisamine
- Extractor: applicationData määramine pärast rakenduse loomist
- Query: 'offset' toetuse teostamine
- Balooctl: --version ja --help lisamine (veateade 351645)
- KAuth'i toetuse eemaldamine inotify jälgijate maksimumi tõstmiseks, kui arv on liiga väike (veateade 351602)

### BluezQt

- fakebluez'i krahhi kõrvaldamine obexmanagertest'is ASAN-i kasutamisel
- Kõigi eksporditud klasside eeldeklareerimine päisefailis types.h
- ObexTransfer: veast teatamine, kui ülekandeseanss eemaldatakse
- Utils: Hold pointers to managers instances
- ObexTransfer: veast teatamine, kui rg.bluez.obex'it tabab krahh

### CMake'i lisamoodulid

- GTK ikoonipuhvri uuendamine ikoonide paigaldamisel.
- Hädaabinõu eemaldamine täitmisega viivitamiseks Androidil
- ECMEnableSanitizers: gcc 4.9 toetab defineerimata saniteerijaid
- X11, XCB jms tuvastamise keelamine OS X peal
- Failide otsimine paigaldatud prefiksi, mitte prefiksi asukoha järgi
- Qt5 kasutamine Qt5 paigaldamisprefiksi määramiseks
- Definitsiooni ANDROID lisamine, mida vajab qsystemdetection.h.

### Raamistike lõimimine

- Failidialoogi juhusliku mittenäitamise probleemi lahendamine (veateade 350758).

### KActivities

- Kohandatud vastendamise funktsiooni kasutamine sqlite'i globaali asemel (veateade 352574).
- Probleemi lahendamine uue ressursi lisamisel mudelisse

### KCodecs

- Krahhi likvideerimine UnicodeGroupProber::HandleData's lühikeste stringide korral

### KConfig

- kconfig-compiler'i märgistamine graafilise kasutajaliideseta tööriistana

### KCoreAddons

- KShell::splitArgs: eraldaja on ainult ASCII tühimärk, mitte Unicode tühimärl U+3000 (veateade 345140)
- KDirWatch: krahhi kõrvaldamine, kui globaalne staatiline destruktor kasutab KDirWatch::self() (veateade 353080)
- Krahhi kõrvaldamine, kui Q_GLOBAL_STATIC kasutab KDirWatch'i.
- KDirWatch: lõimetuvalisuse parandus
- Selgitamine, kuidas määrata KAboutData konstruktori argumente.

### KCrash

- KCrash: cwd edastamine kdeinit'ile rakenduse automaatsel taaskäivitamisel kdeinit'i kaudu (veateade 337760)
- KCrash::initialize() lisamine, et rakendused ja platvormi pluginad saaksid otseselt KCrash'i lubada.
- ASAN-i keelamine,  kui see on lubatud

### KDeclarative

- Pisitäiustused ColumnProxyModel'is
- Võimaldamine rakendustel teada kodukataloogi asukohta
- EventForge eemaldamine töölauakonteinerist
- QIconItemi omaduse lubamine.

### KDED

- kded: sycoca loogika lihtsustamine lihtsalt ensureCacheValid'it välja kutsudes

### KDELibs 4 toetus

- newInstance'i väljakutsumine järglasest esimesel väljakutsel
- kdewin'i definitsioonide kasutamine.
- Enam ei püüta leida X11 WIN32 peal
- cmake: taglib'i versioonikontrolli parandus failis FindTaglib.cmake.

### KDesignerPlugin

- Qt moc ei suuda käidelda makrosid (QT_VERSION_CHECK)

### KDESU

- kWarning -&gt; qWarning

### KFileMetaData

- windowsi kasutajate metaandmete teostus

### KDE GUI Addons

- X11/XCB otsimata jätmine ka WIN32 peal

### KHTML

- std::auto_ptr asendamine std::unique_ptr'iga
- khtml-filter: reeglite hülgamine, mis sisaldavad adblocki eriomadusi, mida me veel ei toeta.
- khtml-filter: koodi ümberkorraldamine funktsionaalsete muutusteta.
- khtml-filter: valikutega regulaaravaldiste eiramine, sest me ei toeta neid.
- khtml-filter: adblocki valikute eristaja tuvastamise parandus.
- khtml-filter: lõpetavate tühimärkide puhastamine.
- khtml-filter: ei hüljata ridu, mille alguses seisab '&amp;', sest see ei ole adblocki erimärk.

### KI18n

- rangete iteraatorite eemaldamine msvc puhul k18n ehitamise tagamiseks

### KIO

- KFileWidget: eellasargument peab olema vaikimisi 0 nagu kõigis vidinates.
- Make sure the size of the byte array we just dumped into the struct is big enough before calculating the targetInfo, otherwise we're accessing memory that doesn't belong to us
- Qurl'i kasutuse parandus QFileDialog::getExistingDirectory() väljakutsumisel
- Solidi seadmeloendi värskendamine enne kio_trash'i päringut
- trash: lubamine trash:/ kõrval URL-ina listDir'is (kutsub välja listRoot'i) (veateade 353181)
- KProtocolManager: tupikseisu parandus EnvVarProxy kasutamisel (veateade 350890).
- Enam ei püüta leida X11 WIN32 peal
- KBuildSycocaProgressDialog: Qt sisseehitatud hõivatuse indikaatori kasutamine (veateade 158672).
- KBuildSycocaProgressDialog: kbuildsycoca5 käivitamine QProcess'iga.
- KPropertiesDialog: paranuds, kui ~/.local on nimeviit, kanoonilisi asukohti võrreldes
- Võrguressursside toetuse lisamine kio_trash'is (veateade 177023)
- Ühendumine QDialogButtonBox'i, mitte QDialog'i signaalidega (veateade 352770)
- Küpsiste juhtimismoodul: D-Busi nimede uuendamine kded5 tarbeks
- JSON-failide vahetu kasutamine kcoreaddons_desktop_to_json() asemel

### KNotification

- Märguande uuendamise signaali ei saadeta kaks korda
- Märguannete seadistuse parsimine ainult muutmise korral
- Enam ei püüta leida X11 WIN32 peal

### KNotifyConfig

- Vaikeväärtuste laadimise meetodi muutmine
- appname, mille seadistust on uuendatud, saatmine koos DBusi signaaliga
- Meetodi lisamine kconfigwidget'i taastamiseks vaikeväärtustele
- Seadistust ei sünkroonita enam salvestamisel n korda

### KService

- Alamkataloogi suurima ajatempli kasutamine ressursikataloogi ajatemplina.
- KSycoca: kõigi lähtekataloogide mtime'i salvestamine muudatuste tuvastamiseks (veateade 353036).
- KServiceTypeProfile: tarbetu põhibaasi loomise eemaldamine (veateade 353360).
- KServiceTest::initTestCase'i lihtsustamine ja kiirendamine.
- faili applications.menu paigaldamine puhverdatud cmake'i muutujasse
- KSycoca: ensureCacheValid() peab looma andmebaasi, kui seda veel ei ole
- KSycoca: globaalse andmebaasi töötamise tagamine pärast viimast ajatempli kontrollkoodi
- KSycoca: andmebaasi failinime muutmine, et see sisaldaks nende kataloogide keelt ja sha1, mille põhjal see ehitati.
- KSycoca: tagamine, et ensureCacheValid() oleks avalikus API-s.
- KSycoca: pointeri lisamine singletoni kasutamise veel enamaks eemaldamiseks
- KSycoca: kõigi põhibaaside meetodite self() eemaldamine ja nende salvestamine selle asemel KSycoca'sse.
- KBuildSycoca: faili ksycoca5stamp kirjutamise eemaldamine.
- KBuildSycoca: qCWarning'i kasutamine fprintf(stderr, ...) või qWarning'i asemel
- KSycoca: ksycoca taasehitamine protsessis kbuildsycoca5 täitmise asemel
- KSycoca: kogu kbuildsycoca koodi liigutamine teeki (lib), välja arvatud main().
- KSycoca optimeerimine: faili jälgimine ainult siis, kui rakendus loob ühenduse databaseChanged()-ga
- Mälulekete parandused KBuildSycoca klassis
- KSycoca: DBusi märguannete asendamine faili jälgimisega KDirWatch'i abil.
- kbuildsycoca: võtme --nosignal märkimine iganenuks.
- KBuildSycoca: dbus-põhise lukustamise asendamine lukustufailiga.
- Krahhi vältimine vigase pluginateabe korral.
- Päistele _p.h nime andmine, valmistamaks ette üleminekut kservice'i teegile.
- checkGlobalHeader() liigutamine KBuildSycoca::recreate()-sse.
- --checkstamps ja --nocheckfiles koodi eemaldamine.

### KTextEditor

- rohkemate regulaaravaldiste valideerimine
- regulaaravaldiste parandus esiletõstmise failides (veateade 352662)
- ocaml'i esiletõstmise sünkroonimine saidiga https://code.google.com/p/vincent-hugot-projects/, enne kui Google'i kood kaob, ühes väikeste veaparandustega
- reamurdmise lubamine (veateade 352258)
- rea valideerimine enne kokku-lahtikerimise alustamist (veateade 339894)
- Kate sõnade arvu näitamise probleemide parandus DocumentPrivate'i jälgimisega Document'i asemel (veateade 353258)
- Kconfig'i süntaksi esiletõstmise uuendamine: kahe uue Linux 4.2 operandi lisamine
- sünkroonimine Kate haruga KDE/4.14
- minikaart: kerimisriba pidemete mittenäitamisse parandus juhtudel, kui kerimismärke ei näidatud (veateade 352641).
- süntaks: võtme git user lisamine kdesrc-buildrc'le

### KWalleti raamistik

- Viimast kasutust enam automaatselt ei kloonita

### KWidgetsAddons

- Hoiatuse C4138 (MSVC): '*/' found outside of comment parandus

### KWindowSystem

- QByteArray get_stringlist_reply süvakoopia tegemine
- Mitme X-serveriga interaktiivse suhtlemise lubamine NETWM klassides.
- [xcb] KeyServeri moodide hindamine initsialiseerimisel platvormil != x11
- KKeyserver (x11) viimine kategoriseeritud logimise peale

### KXMLGUI

- Võimaldamine importida/eksportida kiirklahviskeeme sümmeetriliselt

### NetworkManagerQt

- Introspektsiooni parandus: LastSeen peab olema AccessPoint'is, mitte ActiveConnection'is

### Plasma raamistik

- Kohtspikri dialoogi varjamine, kui kursor jõuab mitteaktiivse ToolTipArea kohale
- Kui töölauafailis on Icon=/midagi.svgz, kasutatakse paketi faili
- failitüübi "screenshot" lisamine pakettidesse
- devicepixelration'i kaalumine autonoomsetel kerimisribadel
- Hiirekursori alust efekti ei rakendata puute- ja mobiiliekraanide puhul
- lineedit'i svg veeriste kasutamine sizeHint'i arvutamisel
- Animeeritud ikooni Plasma kohtspikrites ei panda hääbuma
- Nuputeksti kärpimise parandus
- Aplettide kontekstimenüüd paneelil ei kata enam apletti kinni
- Seonduvate rakenduste loendi hankimise lihtsustamine AssociatedApplicationManager'is

### Sonnet

- hunspelli plugina ID parandus selle korrektseks laadimiseks
- staatilise kompileerimise toetamine windowsis, windowsi libreoffice'i hunspelli sõnastiku asukoha lisamine
- Ei eeldata, et Hunspelli sõnastikud on UTF-8 kodeeringus (veateade 353133).
- Highlighter::setCurrentLanguage() parandus juhtudel, mil eelmine keel oli vigane (veateade 349151)
- asukoha /usr/share/hunspell toetus sõnastiku asukohana
- NSSpellChecker'i põhine plugin

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
