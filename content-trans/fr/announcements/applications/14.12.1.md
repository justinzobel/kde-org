---
aliases:
- ../announce-applications-14.12.1
changelog: true
date: '2015-01-08'
description: KDE publie la version 14.12.1. des applications.
layout: application
title: KDE publie KDE Applications 14.12.1
version: 14.12.1
---
13 Janvier 2015. Aujourd'hui, KDE a publié la première mise à jour de consolidation pour les <a href='../14.12.0'>applications 14.12 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 50 corrections de bogues référencées contiennent des améliorations pour l'outil d'archivage Ark, l'outil de modélisation « UML » Umbrello, l'afficheur de documents Okular, l'application d'apprentissage de la prononciation Artikulate et le client de bureau distant. 

Cette version inclut également les versions maintenues à long terme des espaces de travail de Plasma 4.11.15, de la plate-forme de développement 4.14.4 et de la suite Kontact4.14.4.
