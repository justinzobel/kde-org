---
aliases:
- ../announce-applications-14.12.3
changelog: true
date: '2015-03-03'
description: KDE publie la version 14.12.3 des applications.
layout: application
title: KDE publie KDE Applications 14.12.3
version: 14.12.3
---
03 Mars 2015. Aujourd'hui, KDE publie la troisième mise à jour de stabilisation de <a href='../14.12.0'>des applications 14.12de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction. Il s'agit d'une mise à jour sûre et bénéfique pour tout le monde.

Avec 19 corrections de bogues répertoriées, cette livraison comprend également des amélioration du jeu d'anagrammes Kanagram, du modeleur UML Umbrello, de l'afficheur Okular et de l'application de géométrie Kig.

Cette version inclut également les versions maintenues à long terme des espaces de travail de Plasma 4.11.17, de la plate-forme de développement 4.14.6 et de la suite Kontact4.14.6.
