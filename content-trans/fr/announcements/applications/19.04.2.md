---
aliases:
- ../announce-applications-19.04.2
changelog: true
date: 2019-06-06
description: KDE publie la version 19.04.2 des applications.
layout: application
major_version: '19.04'
release: applications-19.04.2
title: KDE publie les applications KDE 19.04.2
version: 19.04.2
---
{{% i18n_date %}}

Aujourd'hui, KDE a publié la seconde mise à jour de consolidation pour les <a href='../19.04.0'>applications de KDE %[2 ]s</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traductions, permettant une mise à jour sûre et appréciable pour tout le monde.

Environ 50 corrections de bogues identifiés, portant sur des améliorations pour Kontact, Ark, Dolphin, JuK, Kdenlive, KmPlot, Okular, Spectacle et bien d'autres.

Les améliorations incluses sont :

- Un plantage lors de l'affichage de certains documents « ePub » dans Okular a été corrigé.
- Les clés secrètes peuvent de nouveau être exportées à partir du gestionnaire de cryptographie Kleopatra.
- Le rappel d'évènements de KAlarm ne se plante plus lors du démarrage avec les bibliothèques les plus récentes « PIM »
