---
aliases:
- ../announce-applications-15.12-beta
date: 2015-11-20
description: KDE publie la version 15.12 « bêta » des applications de KDE.
layout: application
release: applications-15.11.80
title: KDE publie la version « bêta » pour les applications KDE 15.12.
---
20 Novembre 2015. Aujourd'hui, KDE publie la version « bêta » des nouvelles versions des applications KDE. Les dépendances et les fonctionnalités sont stabilisées et l'équipe KDE se concentre à présent sur la correction des bogues et sur les dernières finitions.

Avec de nombreuses applications reposant sur l'environnement de développement version 5, les mises à jour des applications 15.12 ont besoin de tests intensifs pour maintenir et améliorer la qualité et l'interface utilisateur. Les utilisateurs actuels sont très importants pour maintenir la grande qualité de KDE. En effet, les développeurs ne peuvent tester toutes les configurations possibles. Votre aide est nécessaire pour aider à trouver les bogues suffisamment tôt pour qu'ils puissent être corrigés avant la version finale. Veuillez contribuer à l'équipe en installant la version « bêta » et <a href='https://bugs.kde.org/'>en signalant tout bogue</a>.
