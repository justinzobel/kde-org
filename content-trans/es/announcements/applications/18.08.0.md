---
aliases:
- ../announce-applications-18.08.0
changelog: true
date: 2018-08-16
description: KDE lanza las Aplicaciones de KDE 18.08.0
layout: application
title: KDE lanza las Aplicaciones de KDE 18.08.0
version: 18.08.0
---
16 de agosto de 2018. Publicadas las Aplicaciones de KDE 18.08.0.

Trabajamos continuamente en la mejora del software incluido en nuestras series de Aplicaciones de KDE y esperamos que encuentre útiles todas las nuevas mejoras y correcciones de errores.

### Novedades de las Aplicaciones de KDE 18.08

#### Sistema

{{<figure src="/announcements/applications/18.08.0/dolphin1808-settings.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, el potente gestor de archivos de KDE, contiene numerosas mejoras de calidad:

- El diálogo de «Preferencias» se ha modernizado para adaptarse mejor a nuestras directrices de diseño y para ser más intuitivo.
- Se han eliminado varias fugas de memoria que podían ocasionar que su equipo funcionase más lentamente.
- Los elementos del menú «Crear nuevo» ya no están disponibles al ver el contenido de la papelera.
- La aplicación se adapta mejor ahora a las pantallas de alta resolución.
- El menú de contexto incluye ahora opciones más útiles que le permiten ordenar y modificar el modo de vista directamente.
- La ordenación por la hora de modificación es ahora 12 veces más rápida. Además, ahora puede lanzar Dolphin nuevamente cuando inicie sesión con la cuenta de usuario «root». La posibilidad de modificar archivos propiedad de «root» cuando se ejecuta Dolphin como un usuario normal todavía está en desarrollo.

{{<figure src="/announcements/applications/18.08.0/konsole1808-find.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, la aplicación de emulación de terminal de KDE, contiene múltiples mejoras:

- El complemento «Buscar» aparece ahora encima de la ventana sin interrumpir el trabajo.
- Se permite el uso de más secuencias de escape (DECSCUSR y modo de desplazamiento alternativo de XTerm).
- Ahora también es posible asignar cualquier carácter como tecla para los accesos rápidos.

#### Gráficos

{{<figure src="/announcements/applications/18.08.0/gwenview1808.png" width="600px" >}}

18.08 es un lanzamiento importante para <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, el visor y organizador de imágenes de KDE. Durante los últimos meses, los colaboradores han trabajado en una gran cantidad de mejoras. Los puntos destacados incluyen:

- La barra de estado de Gwenview muestra ahora un contador de imágenes y el número total de imágenes.
- Ahora es posible ordenar por puntuación y en orden descendente. Al ordenar por fecha ahora se separan directorios y archivos, y se ha corregido en algunas situaciones.
- Se ha mejorado la compatibilidad con arrastrar y soltar para permitir arrastrar archivos y carpetas en el modo de Vista para mostrarlos, así como arrastrar los elementos visualizados a aplicaciones externas.
- Pegar imágenes copiadas de Gwenview también funciona ahora para aplicaciones que solo aceptan datos de imágenes sin procesar, pero no rutas de archivo. Ahora también se permite la copia de imágenes modificadas.
- El diálogo de cambio de tamaño de la imagen se ha revisado para dotarlo de mayor usabilidad y se le ha añadido una opción para cambiar el tamaño de la imagen basado en porcentaje.
- Se han corregido el control deslizante de tamaño de la herramienta de reducción de ojos rojos y el cursor en forma de cruz.
- La selección de fondo transparente tiene ahora una opción para 'Ninguno' y también se puede configurar para SVG.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-resize.png" width="600px" >}}

La ampliación de imágenes se ha vuelto más conveniente:

- Se activado la ampliación por desplazamiento o haciendo clic, así como también la panorámica cuando están activas las herramientas de recorte o de reducción de ojos rojos.
- El clic central vuelve a cambiar de nuevo entre ampliar para ajustar y ampliar al 100%.
- Se han añadido los accesos rápidos de teclado «Mayúsculas+clic central» y «Mayúsculas+F» para conmutar la ampliación de relleno.
- Ctrl-clic amplía ahora más rápido y de forma más fiable.
- Gwenview amplía ahora en la posición actual del cursor para las operaciones de Ampliar/Reducir, Rellenar y hacer zoom al 100% al usar el ratón y los accesos rápidos de teclado.

El modo de comparación de imágenes contiene varias mejoras:

- Se ha corregido el tamaño y la alineación de la selección resaltada.
- Se ha corregido el solapamiento de SVG con la selección resaltada.
- Para imágenes SVG pequeñas, el resaltado de la selección coincide con el tamaño de la imagen.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-sort.png" width="600px" >}}

Se han introducido varias mejoras menores para hacer que el flujo de trabajo sea aún más agradable:

- Se han mejorado las transiciones de desvanecimiento entre imágenes de diferentes tamaños y transparencias.
- Se ha corregido la visibilidad de los iconos en algunos botones flotantes cuando se usa una combinación de colores claros.
- Al guardar una imagen con un nuevo nombre, el visor no salta a una imagen no relacionada después.
- Cuando se pulsa el botón de compartir y los complementos «kipi» no están instalados, Gwenview le pedirá al usuario que los instale. Tras su instalación, se muestran inmediatamente.
- La barra lateral impide ahora que se oculte accidentalmente al cambiar su tamaño y recuerda su anchura.

#### Oficina

{{<figure src="/announcements/applications/18.08.0/kontact1808.png" width="600px" >}}

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, el potente cliente de correo electrónico de KDE, presenta algunas mejoras en el motor de extracción de datos de viajes. Ahora admite códigos de barras de billetes de tren UIC 918.3 y SNCF, así como búsqueda de ubicación de estaciones de tren basada en Wikidata. Se ha añadido soporte para itinerarios de múltiples viajeros, y KMail dispone ahora de integración con la aplicación KDE Itinerary.

<a href='https://userbase.kde.org/Akonadi'>Akonadi</a>, la infraestructura de gestión de información personal, es ahora más rápido gracias a las cargas útiles de notificación y al uso de XOAUTH para SMTP, lo que permite la autenticación nativa con Gmail.

#### Educativos

<a href='%[1]'>Cantor</a> la interfaz de KDE para software matemático, guarda ahora el estado de los paneles («Variables», «Ayuda», etc.) para cada sesión se forma independiente. Las sesiones de «Julia» son ahora más rápidas de crear.

La experiencia de usuario en <a href='https://www.kde.org/applications/education/kalgebra/'>KAlgebra</a>, nuestra calculadora gráfica, se ha visto significativamente mejorada en los dispositivos táctiles.

#### Utilidades

{{<figure src="/announcements/applications/18.08.0/spectacle1808.png" width="600px" >}}

Los colaboradores de <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, la versátil herramienta de capturas de pantalla de KDE, se han centrado en mejorar el modo de región rectangular:

- En el modo de región rectangular existe ahora una lupa para ayudarle a dibujar un rectángulo de selección perfecto ajustado al píxel.
- Ahora es posible mover y cambiar el tamaño del rectángulo de selección mediante el teclado.
- La interfaz de usuario sigue el esquema de color del usuario y la presentación de texto de ayuda se ha mejorado

Para hacer que sea más fácil compartir las capturas de pantalla con otras personas, los enlaces de las imágenes compartidas se copian ahora de forma automática en el portapapeles. Las capturas de pantalla se pueden guardar automáticamente en los subdirectorios indicados por el usuario.

<a href='https://userbase.kde.org/Kamoso'>Kamoso</a>, nuestro grabador de webcam recorder, se ha actualizado para evitar cuelgues con las versiones recientes de GStreamer.

### A la caza de errores

Se han resuelto más de 120 errores en aplicaciones, que incluyen la suite Kontact, Ark, Cantor, Dolphin, Gwenview, Kate, Konsole, Okular, Spectacle y Umbrello, entre otras.

### Registro de cambios completo
