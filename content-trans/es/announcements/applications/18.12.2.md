---
aliases:
- ../announce-applications-18.12.2
changelog: true
date: 2019-02-07
description: KDE lanza las Aplicaciones 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.2
title: KDE lanza las Aplicaciones de KDE 18.12.2
version: 18.12.2
---
{{% i18n_date %}}

KDE ha lanzado hoy la segunda actualización de estabilización para las <a href='../18.12.0'>Aplicaciones 18.12</a>. Esta versión solo contiene correcciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 12 correcciones de errores registradas, se incluyen mejoras en Kontact, Ark, Konsole, Lokalize y Umbrello, entre otras aplicaciones.

Las mejoras incluyen:

- Ark ya no borra archivos guardados desde dentro del visor integrado.</li>
- La libreta de direcciones recuerda ahora los cumpleaños cuando se fusionan contactos.</li>
- En Umbrello se han corregido varias actualizaciones de visualización de diagramas que faltaban.</li>
