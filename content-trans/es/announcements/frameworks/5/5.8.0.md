---
aliases:
- ../../kde-frameworks-5.8.0
date: '2015-03-13'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nuevas infraestructuras:

- KPeople, proporciona acceso a todos los contactos y a las personas que contienen.
- KXmlRpcClient, interacción con los servicios «XMLRPC».

### General

- Numerosas soluciones de compilación para la compilación del próximo Qt 5.5

### KActivities

- El servicio de puntuación de recursos ya está finalizado.

### KArchive

- Impedir fallos en archivos ZIP con descriptores de datos redundantes.

### KCMUtils

- Restaurar «KCModule::setAuthAction».

### KCoreAddons

- KPluginMetadata: Permitir el uso de la clave «Hidden».

### KDeclarative

- Preferir la exposición de listas a QML con «QJsonArray».
- Contemplar el uso de «devicePixelRatios» no predeterminados en las imágenes.
- Exponer «hasUrls» en «DeclarativeMimeData».
- Permitir que el usuario configure el número de líneas horizontales a dibujar.

### KDocTools

- Se ha corregido la compilación en MacOSX cuando se usa Homebrew.
- Mejor estilo de objetos multimedia (imágenes, etc.) en la documentación.
- Codificar caracteres no válidos en las rutas que se usan en las DTD de XML, evitando errores.

### KGlobalAccel

- Marca de tiempo de activación definida como propiedad dinámica cuando se activa una QAction.

### KIconThemes

- Se ha corregido que «QIcon::fromTheme(xxx, algúnFallback)» no devuelva el «fallback».

### KImageFormats

- Hacer que el lector de imágenes PSD sea agnóstico en cuanto al orde de bytes.

### KIO

- Marcar como obsoleto «UDSEntry::listFields» y añadir el método «UDSEntry::fields», que devuelve un «QVector» sin realizar una conversión costosa.
- Sincronizar el gestor de marcadores solo si este proceso es el que realiza los cambios (error 343735).
- Se ha corregido el inicio del servicio DBus de «kssld5».
- Implementar los bytes de cuota usados y disponibles de RFC 4331 para activar la información de espacio libreen el esclavo de E/S http.

### KNotifications

- Demorar la inicialización del sonido hasta que sea realmente necesario.
- Se ha corregido que la configuración de las notificaciones no se aplique instantáneamente.
- Se ha corregido la detención de las notificaciones de sonido tras reproducir el primer archivo.

### KNotifyConfig

- Se ha añadido una dependencia opcional a «QtSpeech» para volver a activar las notificaciones del habla.

### KService

- KPluginInfo: permitir el uso de listas de cadenas de texto como propiedades.

### KTextEditor

- Se han añadido estadísticas de conteo de palabras en la barra de estado.
- vimode: Se ha corregido un fallo al eliminar la última línea en el modo de línea visual.

### KWidgetsAddons

- Hacer que «KRatingWidget» se las arregle con «devicePixelRatio».

### KWindowSystem

- «KSelectionWatcher» y «KSelectionOwner» se pueden usar sin depender de «QX11Info».
- «KXMessages» se puede usar sin depender de «QX11Info».

### NetworkManagerQt

- Se han añadido nuevas propiedades y métodos de «NetworkManager 1.0.0».

#### Plasma framework

- Se ha corregido «plasmapkg2» para los sistemas traducidos.
- Se ha mejorado el diseño de las ayudas emergentes.
- Hacer posible que los plasmoides puedan cargar guiones externos al paquete de Plasma...

### Cambios en el sistema de construcción (módulos extra de cmake).

- Se ha extendido la macro «ecm_generate_headers» para que también permita las cabeceras «CamelCase.h».

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
