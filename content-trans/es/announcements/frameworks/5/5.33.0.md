---
aliases:
- ../../kde-frameworks-5.33.0
date: 2017-04-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Se han añadido descripciones a las órdenes (balooctl)
- También se busca en directorios enlazados (fallo 333678)

### BluezQt

- Proporcionar tipo de dispositivo para los dispositivos de bajo consumo

### Módulos CMake adicionales

- Especificar «qml-root-path» como el directorio compartido en el prefijo
- Corregir la compatibilidad de ecm_generate_pkgconfig_file con el nuevo cmake
- Registrar las opciones APPLE_* solo «if(APPLE)»

### KActivitiesStats

- Se han añadido preajustes a la aplicación de prueba
- Mover elementos correctamente a la posición deseada
- Sincronizar los cambios de orden a otras instancias del modelo
- Si el orden no está definido, ordenar las entradas por su ID

### Herramientas KDE Doxygen

- [Meta] Cambiar los responsables en setup.py

### KAuth

- Motor para Mac
- Añadir la posibilidad de matar un KAuth::ExecuteJob

### KConfig

- Sanear la lista de accesos rápidos al leer y escribir «kdeglobals»
- Evitar reasignaciones de memoria inútiles eliminando llamadas de compresión en memorias intermedias temporales

### KDBusAddons

- KDBusService: Añadir un accesor para el nombre de servicio DBus que se ha registrado

### KDeclarative

- Con Qt &gt;= 5.8, usar la nueva API para definir el motor de escena gráfica
- No definir «acceptHoverEvents» en «DragArea», ya que no se usan

### KDocTools

- meinproc5: enlace a los archivos, no a la biblioteca (fallo 377406).

### KFileMetaData

- Hacer que «PlainTextExtractor» vuelva a coincidir con «text/plain»

### KHTML

- Página de error: cargar correctamente la imagen (con un URL real)

### KIO

- Hacer que vuelva a funcionar la redirección de URL de archivo remoto «file://» a «smb://»
- Mantener la codificación de la consulta cuando se usa el proxy HTTP
- Se han actualizado agentes de usuario (Firefox 52 ESR, Chromium 57)
- Manejar/truncar la cadena mostrada de URL asignada a la descripción de un trabajo. Previene que se incluyan en notificaciones de la interfaz gráfica grandes URL de datos
- Añadir KFileWidget::setSelectedUrl() (fallo 376365)
- Corregir el modo de guardar «KUrlRequester» añadiendo «setAcceptMode»

### KItemModels

- Mencionar el nuevo «QSFPM::setRecursiveFiltering(true)» que hace obsoleto a «KRecursiveFilterProxyModel»

### KNotification

- No eliminar notificaciones encoladas cuando se inicia el servicio «fd.o»
- Adaptaciones de la plataforma Mac

### KParts

- API dox: corregir la falta de la nota al llamar «setXMLFile» con «KParts::MainWindow»

### KService

- Corregir los mensajes de la terminal «No se ha encontrado: ""»

### KTextEditor

- Exponer funcionalidades internas adicionales de las vistas a la API pública
- Ahorrar numerosas asignaciones de memoria para «setPen»
- Corregir «ConfigInterface» de «KTextEditor::Document»
- Se han añadido opciones de tipos de letra y comprobación ortográfica al vuelo en «ConfigInterface»

### KWayland

- Se ha añadido la implementación de «wl_shell_surface::set_popup» y «popup_done»

### KWidgetsAddons

- Posibilidad de compilar con Qt sin «a11y» activado
- Corregir la sugerencia de tamaño incorrecto cuando se llama «animatedShow» con un padre oculto (fallo 377676)
- Corregir que se puedan editar caracteres en «KCharSelectTable»
- Activar todos los planos en el diálogo de prueba de «kcharselect»

### NetworkManagerQt

- WiredSetting: devolver negociación automática incluso cuando está desactivada
- Impedir que Qt defina señales de «glib2»
- WiredSetting: la velocidad y el modo dúplex solo se pueden definir cuando la negociación automática está desconectada (fallo 376018)
- El valor de la negociación automática para el ajuste de red cableada debe ser falso

### Framework de Plasma

- [ModelContextMenu] Usar un instanciador en lugar repetir código una y otra vez
- [Calendario] Encoger y elidir los nombres de las semanas como se hace con los días (fallo 378020)
- [Elemento de icono] Hacer que la propiedad «smooth» haga realmente lo que dice
- Definir el tamaño implícito a partir del tamaño original de las imágenes SVG para las URL de origen
- Añadir una nueva propiedad a los contenedores para el modo de edición
- Corregir «maskRequestedPrefix» cuando no se usa ningún prefijo (fallo 377893)
- [Menú] Armonizar la posición de «openRelative»
- La mayoría de los menús (de contexto) poseen ahora aceleradores (accesos rápidos de tipo Alt+letra) (fallo 361915)
- Controles de Plasma basados en QtQuickControls2
- Manejar «applyPreFixes» con una cadena vacía (fallo 377441)
- Borrar realmente las cachés de temas antiguos
- [Interfaz de contenedores] Disparar menús de contexto al pulsar la tecla «Menú»
- [Tema Brisa de Plasma] Mejorar los iconos de capas de acciones (fallo 376321)

### Resaltado de sintaxis

- TOML: Corregir el resaltado de las secuencias de escape de cadenas
- Actualizar el resaltado de sintaxis para Clojure
- Unas cuantas actualizaciones para la sintaxis de OCaml
- Resaltar los archivos *.sbt como código de «scala»
- Usar también el resaltado de código QML para los archivos .qmltypes

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
