---
aliases:
- ../../kde-frameworks-5.30.0
date: 2017-01-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Iconos Brisa

- Añadir los iconos de Haguichi de Stephen Brandt (¡gracias!).
- Terminar la implementación de iconos de Calligra
- Se ha añadido el icono para CUPS, gracias a Colin (fallo 373126)
- Actualizar el icono de kalarm (fallo 362631)
- Añadir el icono de krfb (fallo 373362)
- Añadir implementación de tipos MIME para R (fallo 371811)
- Y mucho más

### Módulos CMake adicionales

- appstreamtest: Manejar los programas no instalados.
- Activar las advertencias coloreadas en la salida de «ninja».
- Corregir un «::» que faltaba en la documentación de la API para desencadenar el estilo del código.
- Ignorar los archivos «libs/includes/cmakeconfig» del «host» en la «toolchain» de Android.
- Documentar el uso de «gnustl_shared» con la «toolchain» de Android.
- No usar nunca «-Wl,--no-undefined» en Mac (APPLE).

### Integración con Frameworks

- Mejorar «KNSHandler» de «KPackage».
- Buscar la versión requerida más precisa de «AppstreamQt».
- Añadir uso de KNewStuff a KPackage

### KActivitiesStats

- Permitir compilación con -fno-operator-names
- No obtener más recursos vinculados cuando uno de ellos desaparezca.
- Se ha añadido un rol de modelo para recuperar la lista de actividades vinculadas a un recurso.

### Herramientas KDE Doxygen

- Añadir Android a la lista de plataformas disponibles.
- Incluir archivos de código fuente en ObjC++.

### KConfig

- Generar una instancia con «KSharedConfig::Ptr» para «singleton» y «arg».
- kconfig_compiler: Usar «nullptr» en el código generado.

### KConfigWidgets

- Ocultar la acción «Mostrar la barra de menú» si todas las barras de menú son nativas.
- KConfigDialogManager: Descartar las clases de «kdelibs3».

### KCoreAddons

- Devolver tipos stringlist/booleanos en «KPluginMetaData::value».
- DesktopFileParser: Respetar el campo «ServiceTypes».

### KDBusAddons

- Añadir vínculos en Python para «KDBusAddons».

### KDeclarative

- Introducir la importación QML «org.kde.kconfig» con «KAuthorized».

### KDocTools

- kdoctools_install: coincidir con la ruta completa del programa (error 374435).

### KGlobalAccel

- [runtime] Introducir una variable de entorno «KGLOBALACCEL_TEST_MODE».

### Complementos KDE GUI

- Añadir vínculos Python para «KGuiAddons».

### KHTML

- Definir los datos del complemento para que funcione el visor de imágenes integrable.

### KIconThemes

- Informar también a «QIconLoader» cuando cambia el tema de iconos del escritorio (error 365363).

### KInit

- Tener en cuenta la propiedad «X-KDE-RunOnDiscreteGpu» cuando se inicia una aplicación usando «klauncher».

### KIO

- «KIO::iconNameForUrl» devolverá ahora iconos especiales para las ubicaciones de «xdg», como la carpeta «Imágenes» del usuario.
- ForwardingSlaveBase: Se ha corregido el paso del indicador «Overwrite» a «kio_desktop» (error 360487).
- Mejorar y exportar la clase «KPasswdServerClient», la API cliente para «kpasswdserver».
- [KPropertiesDialog] Eliminar la opción «Situar en la bandeja del sistema».
- [KPropertiesDialog] No cambiar el «Name» de «Link» en los archivos «.desktop» si el nombre es de solo lectura.
- [KFileWidget] Usar «urlFromString» en lugar de «QUrl(QString)» en «setSelection» (error 369216).
- Añadir una opción a «KPropertiesDialog» para ejecutar una aplicación en una tarjeta gráfica discreta.
- Terminar correctamente «DropJob» tras soltar un ejecutable.
- Se ha corregido el uso de las categorías de registro en Windows.
- DropJob: Emitir «started» del trabajo de copia tras su creación.
- Permitir que se pueda llamar «suspend()» en un «CopyJob» antes de que se inicie.
- Permitir que se puedan suspender trabajos inmediatamente, al menos para «SimpleJob» y «FileCopyJob».

### KItemModels

- Actualizar los proxys para la clase de errores detectados recientemente (manejo de «layoutChanged»).
- Hacer que KConcatenateRowsProxyModel funcione en QML
- KExtraColumnsProxyModel: Hacer persistentes los índices del modelo tras emitir «layoutChange», no antes.
- Se ha corregido una aserción (en «beginRemoveRows») cuando se quita la selección de un elemento hijo vacío de un elemento hijo seleccionado en «korganizer».

### KConfigWidgets

- No ceder el foco a las ventanas de avance (error 333934).

### KNewStuff

- [Botón GHNS] Ocultar cuando se aplica la restricción de KIOSK.
- Se ha corregido la configuración de «::Engine» cuando se crea con una ruta absoluta de archivo de configuración.

### KNotification

- Añadir una prueba manual para los lanzadores de Unity.
- [KNotificationRestrictions] Permitir que el usuario pueda especificar la cadena del motivo de la restricción.

### Framework KPackage

- [PackageLoader] No acceder a «KPluginMetadata» no válido (error 374541).
- Se ha corregido la descripción de la opción «-t» en la página de manual.
- Mejorar mensajes de error
- Corregir el mensaje de ayuda para --type
- Mejorar el proceso de instalación de los paquetes de «KPackage».
- Instalar un archivo «kpackage-generic.desktop».
- Excluir los archivos «qmlc» de la instalación.
- No listar los plasmoides de forma separada en «metadata.desktop» y «.json».
- Corrección adicional para los paquetes con tipos diferentes y los mismos identificadores.
- Corregir un fallo de cmake cuando dos paquetes con diferente tipo tienen el mismo identificador.

### KParts

- Llamar el nuevo «checkAmbiguousShortcuts()» desde «MainWindow::createShellGUI».

### KService

- KSycoca: No seguir los enlaces simbólicos a directorios, ya que crea un riesgo de recursividad.

### KTextEditor

- Corrección: El texto arrastrado hacia adelante da como resultado una selección incorrecta (error 374163).

### Framework KWallet

- Especificar el mínimo requerido a GpgME++ versión 1.7.0.
- Revertir «Si "Gpgmepp" no se encuentra, intentar usar "KF5Gpgmepp"».

### KWidgetsAddons

- Añadir enlaces de Python.
- Añadir «KToolTipWidget», una ayuda emergente que contiene otro widget.
- Corregir las comprobaciones de validez de las fechas introducidas en «KDateComboBox».
- KMessageWidget: Usar un color rojo más oscuro cuando el tipo es «Error» (error 357210).

### KXMLGUI

- Advertir durante el inicio sobre accesos rápidos ambiguos (con la excepción de Mayúsculas+Suprimir).
- MSWin y Mac tienen un comportamiento similar para guardar de forma automática el tamaño de la ventana.
- Mostrar la versión de la aplicación en la cabecera del diálogo «acerca de» (error 372367).

### Iconos de Oxígeno

- Sincronizar con los iconos de Brisa.

### Framework de Plasma

- Se han corregido las propiedades «renderType» de varios componentes.
- [ToolTipDialog] Usar «KWindowSystem::isPlatformX11()» que está en caché.
- [Elemento de icono] Corregir la actualización del tamaño implícito cuando cambia el tamaño del icono.
- [Diálogo] Usar «setPosition/setSize» en lugar de definir cada cosa individualmente.
- [Unidades] Hacer constante la propiedad «iconSizes».
- Ahora existe una instancia global de «Units», lo que reduce el consumo de memoria y el tiempo de creación de elementos SVG.
- [Elemento de icono] Permitir iconos que no sean cuadrados (error 355592).
- Buscar/sustituir antiguos tipos codificados a mano de «plasmapkg2» (error 374463).
- Se han corregido los tipos «X-Plasma-Drop*» (error 374418).
- [ScrollViewStyle de Plasma] Mostrar el fondo de la barra de desplazamiento solo al situar el puntero del ratón sobre ella.
- Corregir #374127: Colocación errónea de las ventanas emergentes en las ventanas del panel.
- Marcar como obsoleta la API de «Plasma::Package» en «PluginLoader».
- Volver a comprobar la representación que se debe usar en «setPreferredRepresentation».
- [declarativeimports] Usar «QtRendering» en dispositivos de tipo teléfono.
- Considerar siempre un panel vacío en «miniaplicaciones cargadas» (error 373836).
- Emitir «toolTipMainTextChanged» si cambia en respuesta a un cambio de título.
- [TextField] Permitir la desactivación del botón para revelar la contraseña en la restricción de KIOSK.
- [AppletQuickItem] Permitir mensaje de error de lanzamiento.
- Corregir la lógica del manejo de flejas en las configuraciones locales RTL (error 373749).
- TextFieldStyle: Corregir el valor «implicitHeight» para que el cursor de texto esté centrado.

### Sonnet

- cmake: Buscar también «hunspell-1.6».

### Resaltado de sintaxis

- Corregir el resaltado sintáctico de «Makefile.inc» añadiendo prioridad a «makefile.xml».
- Resaltado sintáctico de los archivos SCXML como XML.
- makefile.xml: Muchas mejoras, demasiadas para listarlas aquí.
- Sintaxis de Python: Se han añadido «f-literals» y se ha mejorado el manejo de cadenas.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
